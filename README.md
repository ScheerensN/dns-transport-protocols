.
├── benchmark					        Folder of the python benchmark script. Run with main.py. Example: python3.8 main.py --log ./baseline/event.log --experiment-path ./baseline/ -p ./profile_baseline.yaml
│   │																							  python3.8 main.py --help
│   └── Measure                         Folder containing the measurement taken and displayed in the written work
│       ├── baseline50 merged           Synthetic setup - baseline measure - 50 samples
│       ├── lag200-50 merged            Synthetic setup - lag 200 ms - 50 samples
│       ├── lag400-50 merged            Synthetic setup - lag 400 ms - 50 samples
│       ├── loss.5-Nagle-50 merged      Synthetic setup - 0.5% packet loss - 50 samples
│       ├── loss1-Nagle-50 merged       Synthetic setup - 1% packet loss - 50 samples
│       ├── google-baseline50 merged    Live setup - dns.google - baseline measure - 50 samples
│       ├── google-lag200-50 merged     Live setup - dns.google - lag 200 ms - 50 samples
│       ├── google-lag400-50 merged     Live setup - dns.google - lag 400 ms - 50 samples
│       ├── google-loss.5-Nagle-50 merged Live setup - dns.google - 0.5% packet loss - 50 samples
│       └── google-loss1-Nagle-50 merged  Live setup - dns.google - 1% packet loss - 50 samples
├── dnsconfig                           Configuration files used for unbound and dnsdist
├── httparchive							Script and charts related to the httparchive data
│   └── chart
├── input								Folders containting the three type of input (query) accepted by benchmark
│   ├── dependencytree
│   ├── timings
│   └── wprof
├── phantomJS							Script running PhantomJS to generate dns query timings based on website page load
├── script								Various scripts
└── writing								Written work
    ├── benchmark
    ├── build
    ├── Chapters
    └── image