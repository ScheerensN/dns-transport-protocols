#! /usr/bin/python
import array
import mysql.connector as connector
import pygal
from numpy import percentile, arange

outputPath = "output.txt"

q_limit = 1000000000

mysql_user = 'root'
mysql_password = 'secret'  # ;)
mysql_host = '127.0.0.1'
mysql_database = 'httparchive'

debugF = False
verbose = False

queryMin = ("SELECT min(numDomains) as minDomains FROM pages WHERE rank > -1")
queryMax = ("SELECT max(numDomains) as maxDomains FROM pages WHERE rank > -1")
queryRankMin = ("SELECT min(rank) as minRank FROM pages WHERE rank > -1")
queryRankMax = ("SELECT max(rank) as maxRank FROM pages WHERE rank > -1")
queryData = ("SELECT rank, url, numDomains FROM pages WHERE rank > -1 ORDER BY rank")

### FUNCTIONS ###


def debug(string):
    if debugF:
        print string


def queryValue(connexion, query):
    cursor = connexion.cursor()
    cursor.execute(query)
    value = cursor.fetchone()[0]
    cursor.close()
    return value

def renderToFile(ch, path):
    ch.render_to_file(path + ".svg", force_uri_protocol='https')  # https because there is a bug with http
    ch.render_to_png(path + ".png")


def barChart(title, x_title, y_title, data, x_labels=None):
    ch = pygal.Bar(width=640, height=360, x_title=x_title, y_title=y_title, show_legend=False)
    #ch.title = title
    if x_labels:
        ch.x_labels = x_labels
    for (t, d) in data:
        ch.add(t, d)
    return ch


def xyChart(title, x_title, y_title, data, stroke=True, human_readable=False):
    ch = pygal.XY(width=640, height=360, x_title=x_title, y_title=y_title, human_readable=human_readable, show_legend=False)
    #ch.title = title

    for (t, d) in data:
        if stroke:
            ch.add(t, d)
        else:
            ch.add(t, d, stroke=False)
    return ch

def boxGraph(title, y_title, data):
    ch = pygal.Box(box_mode="tukey", y_title=y_title, show_legend=False)
    #ch.title = title

    for (name, points) in data:
        ch.add(name, points)
    return ch

def percentileStep(raw, step=0.1):
    array = []
    for i in arange(0, 100 + step, step):
        # percentile as always a value that can be mapped to int because it uses the higher interpolation method.
        per = int(percentile(raw, i, interpolation='higher'))
        array.append((i, per))

    return array

def percentileStepString(raw, step=0.1):
    s = ''
    for i in arange(0, 100 + step, step):
        # percentile as always a value that can be mapped to int because it uses the higher interpolation method.
        per = int(percentile(raw, i, interpolation='higher'))
        s += "{}th percentile: {}\n".format(i, per)

    return s


### MYSQL DB CONNECTION AND QUERIES ###

connexion = connector.connect(user=mysql_user, password=mysql_password, host=mysql_host, database=mysql_database)

minDomains = queryValue(connexion, queryMin)
print "The minimum number of domain names in the data set is {}".format(minDomains)

maxDomains = queryValue(connexion, queryMax)
print "The maximum number of domain names in the data set is {}".format(maxDomains)

minRank = queryValue(connexion, queryRankMin)
print "The minimum rank in the data set is {}".format(minRank)

maxRank = queryValue(connexion, queryRankMax)
print "The maximum number of domain names in the data set is {}".format(maxRank)

# Array to aggregate numDomains
debug('Array initialization')
histArray = [0 for i in range(minDomains, maxDomains + 1)]
rankArray = [None for i in range(minRank, maxRank+1)]
rawArray = []
binSize = 10000
binRankArray = [[] for i in range(0, (maxRank//binSize)+1 )]

cursor = connexion.cursor()
cursor.execute(queryData)

debug('Interpret data')
for (rank, url, numDomains) in cursor:
    if verbose:
        print "Rank: {}, Number of domains: {}, URL: {}".format( '%06d' % rank, '%03d' % numDomains, url)
    try:
        histArray[numDomains - 1] += 1
        rankArray[rank - minRank] = numDomains
        rawArray.append(numDomains)
        binRankArray[rank//binSize].append(numDomains)
    except IndexError as err:
        print 'min: {}, max: {}, numDomains: {}'.format(minDomains, maxDomains, numDomains)
        raise err


cursor.close()

connexion.close()

### PLOTTING CHARTS ###
output = open(outputPath, 'w')
output.write('{} element in the data set\n'.format(len(rawArray)))

output.write("The minimum number of domain names in the data set is {}\n".format(minDomains))
output.write("The maximum number of domain names in the data set is {}\n".format(maxDomains))

output.write("The minimum rank in the data set is {}\n".format(minRank))
output.write("The maximum rank in the data set is {}\n".format(maxRank))

print '{} element in the data set'.format(len(rawArray))
x_labels = map(str, range(minDomains, maxDomains+1))
debug('Bar graph')
chart = barChart("Number of website with n domain names", "Number of domain names", "Number of websites",
         [('httparchive.org ranked data', histArray)], x_labels=x_labels)
renderToFile(chart, 'chart/numDomains-bar')

debug('XY graph')

XYArray = []
curr = minDomains
for i in histArray:
    if i == 0:
        i = None
    XYArray.append((curr, i))
    curr += 1

chart = xyChart("Number of website with n domain names", "Number of domain names", "Number of websites",
                [('httparchive.org ranked data', XYArray)])
renderToFile(chart, 'chart/numDomains-XY')

''' # Not interresting
debug('Box Graph')
chart = boxGraph("Number of domains per website", 'Number of domains per website',
                 [('httparchive.org ranked data', rawArray)])
renderToFile(chart, 'chart/numDomains-Box')
'''

debug('Percentile')
output.write('\nPercentile:\n')
perBarArray = []
perDetailedXYArray = []
for i in arange(0, 100.1, 0.1):
    # percentile as always a value that can be mapped to int because it uses the higher interpolation method.
    per = int(percentile(rawArray, i, interpolation='higher'))
    print "{}th percentile: {}".format(i, per)
    output.write("{}th percentile: {}\n".format(i, per))
    perBarArray.append(per)
    perDetailedXYArray.append((i, per))


''' # Not interresting
debug('Percentile Graph - Bar')
#x_labels = map(str, range(0, 105, 5))
chart = barChart("Number of domains per percentile", "th percentile", "Number of domains",
                 [('httparchive.org ranked data', perBarArray)])
renderToFile(chart, 'chart/numDomains-Percentile-Bar')
'''

debug('Detailed Percentile Graph - XY')
chart = xyChart("Number of domain names per percentile", "th percentile", "Number of domain names",
        [('httparchive.org ranked data', perDetailedXYArray)], stroke=False, human_readable=True)
renderToFile(chart, 'chart/numDomains-Percentile-Detailed-XY')


debug('Percentile Rank')
for subset in range(0, (maxRank//binSize)+1):
    l = len(binRankArray[subset])
    if l > 50:
        s = 'Rank between {} and {} ({})'.format((subset*binSize)+1, (subset*binSize)+binSize-1,
                                l)
        print s
        output.write(s)
        s = percentileStepString(binRankArray[subset], 5)
        print s
        print ''
        output.write(s)
        output.write('\n\n')
    else:
        s = 'Not enough data for ranks between {} and {} ({})'.format((subset*binSize)+1, (subset*binSize)+binSize-1,
                                l)


''' # nothing added by rank
perBinRankArray = [[] for i in range(0, (maxRank//binSize)+1)]
for subset in range(0, (maxRank//binSize)+1):
    perBinRankArray[subset] = ('Rank between {} and {} ({})'.format((subset*binSize)+1, (subset*binSize)+binSize-1,
                                len(binRankArray[subset])), percentileStep(binRankArray[subset]))
for i in range(0, (len(perBinRankArray)//10)+1):
    chart = xyChart("Number of domains per percentile", "th percentile", "Number of domains",
                perBinRankArray[i*10:(i*10)+9], stroke=False, human_readable=True)
    renderToFile(chart, 'chart/numDomains-Percentile-Ranked-XY-{}'.format(i))
'''

debug('CDF GRAPH - XY')

histCDFArray = []
total = float(len(rawArray))
current = 0.0
currentIndex = 0
for i in histArray:
    currentIndex += 1
    if i:
        current += float(i)
        histCDFArray.append((currentIndex, (current/total) * 100.0))

chart = xyChart("CDF of the number of domain names", "Number of domain names", "Percentage [%]",
                [('CDF of the number of domain names per website', histCDFArray)])
renderToFile(chart, 'chart/numDomains-CDF-XY')

output.close()
