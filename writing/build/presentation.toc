\beamer@endinputifotherversion {3.36pt}
\select@language {english}
\beamer@sectionintoc {1}{Erratum}{2}{0}{1}
\beamer@sectionintoc {2}{Introduction}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{DNS}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{DNSSEC}{5}{0}{2}
\beamer@subsectionintoc {2}{3}{DNS at the transport layer}{6}{0}{2}
\beamer@subsectionintoc {2}{4}{Motivation}{8}{0}{2}
\beamer@subsectionintoc {2}{5}{TFO}{9}{0}{2}
\beamer@subsectionintoc {2}{6}{Nodelay, Nagle \& Cork}{10}{0}{2}
\beamer@subsectionintoc {2}{7}{Early Retransmit and Tail Loss Probe}{12}{0}{2}
\beamer@subsectionintoc {2}{8}{Benchmark}{13}{0}{2}
\beamer@subsectionintoc {2}{9}{PhantomJS}{14}{0}{2}
\beamer@sectionintoc {3}{Results}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{TFO}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{Lag \& Jitter}{20}{0}{3}
\beamer@subsectionintoc {3}{3}{Reordering}{26}{0}{3}
\beamer@subsectionintoc {3}{4}{TLS behavior}{27}{0}{3}
\beamer@subsectionintoc {3}{5}{Conclusion}{28}{0}{3}
\beamer@sectionintoc {4}{Questions}{29}{0}{4}
\beamer@subsectionintoc {4}{1}{Queries}{34}{0}{4}
