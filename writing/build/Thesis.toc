\select@language {english}
\contentsline {chapter}{Abstract}{2}{chapter*.3}
\contentsline {chapter}{Introduction}{3}{chapter*.4}
\contentsline {chapter}{\numberline {1}The Domain Name System (DNS) Infrastructure}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Name Servers}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}Tree name structure}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Authority and delegation}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Implementation and structure}{5}{section.1.4}
\contentsline {section}{\numberline {1.5}Queries and Answers}{6}{section.1.5}
\contentsline {section}{\numberline {1.6}Resource Record RR}{8}{section.1.6}
\contentsline {subsection}{\numberline {1.6.1}\gls {SOA} \gls {RR} \hfill {\texttt {6} \hspace *{1.75cm} 1035}}{9}{subsection.1.6.1}
\contentsline {subsection}{\numberline {1.6.2}A \gls {RR} \hfill {\texttt {1} \hspace *{1.75cm} 1035}}{9}{subsection.1.6.2}
\contentsline {subsection}{\numberline {1.6.3}AAAA \gls {RR} \hfill {\texttt {28} \hspace *{1.75cm} 3596}}{9}{subsection.1.6.3}
\contentsline {subsection}{\numberline {1.6.4}\gls {NS} \gls {RR} \hfill {\texttt {2} \hspace *{1.75cm} 1035}}{9}{subsection.1.6.4}
\contentsline {subsection}{\numberline {1.6.5}TXT \gls {RR} \hfill {\texttt {16} \hspace *{1.75cm} 1035}}{10}{subsection.1.6.5}
\contentsline {subsection}{\numberline {1.6.6}NSEC, NSEC3, NSEC3PARAM, RRSIG, DS and DNSKEY \glspl {RR} }{10}{subsection.1.6.6}
\contentsline {subsection}{\numberline {1.6.7}\gls {TSIG} \gls {RR} \hfill {\texttt {250} \hspace *{1.75cm} 2845}}{10}{subsection.1.6.7}
\contentsline {subsection}{\numberline {1.6.8}\gls {TKEY} \gls {RR} \hfill {\texttt {249} \hspace *{1.75cm} 2930}}{10}{subsection.1.6.8}
\contentsline {subsection}{\numberline {1.6.9}SIG(0) \gls {RR} \hfill {\texttt {24} \hspace *{0.25cm} 2931 \& 2535}}{10}{subsection.1.6.9}
\contentsline {subsection}{\numberline {1.6.10}OPT \gls {RR} \hfill {\texttt {41} \hspace *{0.25cm} 6891}}{10}{subsection.1.6.10}
\contentsline {section}{\numberline {1.7}Zones}{10}{section.1.7}
\contentsline {section}{\numberline {1.8}Query types}{11}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Iterative Queries}{11}{subsection.1.8.1}
\contentsline {subsection}{\numberline {1.8.2}Recursive Queries}{12}{subsection.1.8.2}
\contentsline {section}{\numberline {1.9}Zone update and propagation process}{12}{section.1.9}
\contentsline {section}{\numberline {1.10}DNS server configurations}{13}{section.1.10}
\contentsline {subsection}{\numberline {1.10.1}Master Name Server}{14}{subsection.1.10.1}
\contentsline {subsection}{\numberline {1.10.2}Slave Name Server}{14}{subsection.1.10.2}
\contentsline {subsection}{\numberline {1.10.3}Caching Name Server}{14}{subsection.1.10.3}
\contentsline {subsection}{\numberline {1.10.4}Forwarding Name Server}{15}{subsection.1.10.4}
\contentsline {subsection}{\numberline {1.10.5}Authoritative-only Name Server}{15}{subsection.1.10.5}
\contentsline {chapter}{\numberline {2}DNS security}{16}{chapter.2}
\contentsline {section}{\numberline {2.1}Transaction Level Authentication}{16}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}TSIG \hfill {\texttt {250} \hspace *{1.75cm} 2845}}{16}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}TKEY \hfill {\texttt {249} \hspace *{1.75cm} 2930}}{16}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}SIG(0) \hfill {\texttt {24} \hspace *{0,25cm} 2535 \& 2931}}{16}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}Data origin authentication and data integrity}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Islands of Security}{17}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Chains of trust}{17}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Confidentiality}{18}{section.2.3}
\contentsline {chapter}{\numberline {3}Network aspects}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}DNS over UDP}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}Transmission Control Protocol (TCP)}{19}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1} \gls {TCP} Connection Establishment and Termination}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2} \gls {TCP} Options}{20}{subsection.3.2.2}
\contentsline {subsubsection}{\acrfull {MSS}}{20}{section*.12}
\contentsline {subsubsection}{\acrfull {TFO}}{21}{section*.13}
\contentsline {subsection}{\numberline {3.2.3} \gls {TCP} Fast Retransmit}{22}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4} \gls {TCP} Tail Loss Probe}{22}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Nagle}{22}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Cork}{22}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}No delay}{23}{subsection.3.2.7}
\contentsline {section}{\numberline {3.3}DNS over TCP}{23}{section.3.3}
\contentsline {section}{\numberline {3.4}TLS Handshake and Session Resumption}{24}{section.3.4}
\contentsline {section}{\numberline {3.5}DNS over TLS}{25}{section.3.5}
\contentsline {section}{\numberline {3.6}DNS over HTTPS}{25}{section.3.6}
\contentsline {section}{\numberline {3.7}Motivation}{26}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}TCP}{26}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}DNS over TLS}{26}{subsection.3.7.2}
\contentsline {chapter}{\numberline {4}Methodology}{28}{chapter.4}
\contentsline {section}{\numberline {4.1}Topology}{28}{section.4.1}
\contentsline {section}{\numberline {4.2}NSD, Unbound \& the T-DNS proxy}{29}{section.4.2}
\contentsline {section}{\numberline {4.3}Benchmark}{30}{section.4.3}
\contentsline {section}{\numberline {4.4}PhantomJS}{31}{section.4.4}
\contentsline {chapter}{\numberline {5}Results}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}Number of unique domain names per website}{33}{section.5.1}
\contentsline {section}{\numberline {5.2}Results for a burst of 10 queries}{34}{section.5.2}
\contentsline {chapter}{\numberline {6}Conclusion}{38}{chapter.6}
\contentsline {chapter}{Bibliography}{38}{chapter.6}
\contentsline {chapter}{Acronyms}{43}{section*.24}
\contentsline {chapter}{\numberline {A}Git Repository}{45}{appendix.A}
\contentsline {chapter}{\numberline {B}Ressource Records}{46}{appendix.B}
\contentsline {section}{\numberline {B.1}Range for RR types}{46}{section.B.1}
\contentsline {chapter}{\numberline {C}DNSSEC in more details}{47}{appendix.C}
\contentsline {section}{\numberline {C.1}Signing the zone}{47}{section.C.1}
\contentsline {section}{\numberline {C.2}Secure Zone Maintenance}{48}{section.C.2}
\contentsline {section}{\numberline {C.3}\acrfull {NSEC3}}{49}{section.C.3}
