\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\providecommand{\bibAnnoteFile}[1]{%
  \IfFileExists{#1}{\begin{quotation}\noindent\textsc{Key:} #1\\
  \textsc{Annotation:}\ \input{#1}\end{quotation}}{}}
\providecommand{\bibAnnote}[2]{%
  \begin{quotation}\noindent\textsc{Key:} #1\\
  \textsc{Annotation:}\ #2\end{quotation}}
\providecommand{\bibinfo}[2]{#2}
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem{serverSurvey}
\textsc{\bibinfo{author}{Netcraft.com}}, \bibinfo{title}{February 2017 web
  server survey},
  \bibinfo{howpublished}{https://news.netcraft.com/archives/category/web-server-survey},
  \bibinfo{year}{accessed March 01, 2017}.
\bibAnnoteFile{serverSurvey}

\bibitem{rfc1034}
\textsc{\bibinfo{author}{Mockapetris, P.}}, \bibinfo{title}{Rfc 1034: Domain
  names: concepts and facilities (november 1987)}, \bibinfo{journal}{Status:
  Standard}, vol.~\bibinfo{volume}{6} (\bibinfo{year}{2003}).
\bibAnnoteFile{rfc1034}

\bibitem{rfc1035}
\textsc{\bibinfo{author}{Mockapetris, P.}}, \bibinfo{title}{Rfc 1035: Domain
  names implementation and specification, november 1987}, \bibinfo{journal}{URL
  http://www. ietf. org/rfc/rfc1035. txt},  (\bibinfo{year}{2004}).
\bibAnnoteFile{rfc1035}

\bibitem{dnsAndBind10}
\textsc{\bibinfo{author}{Aitchison, R.}}, \bibinfo{title}{Pro DNS and Bind 10},
  \bibinfo{publisher}{Berkeley, CA Apress}, \bibinfo{year}{2011}.
\bibAnnoteFile{dnsAndBind10}

\bibitem{DNSroot}
\textsc{\bibinfo{author}{\gls{IANA}}}, \bibinfo{title}{Iana --- root servers},
  \bibinfo{howpublished}{https://www.iana.org/domains/root/servers},
  \bibinfo{year}{accessed April 08, 2017}.
\bibAnnoteFile{DNSroot}

\bibitem{RootServersOrg}
\textsc{\bibinfo{author}{root servers.org}}, \bibinfo{title}{Root servers
  technical operations assn},
  \bibinfo{howpublished}{http://www.root-servers.org/}, \bibinfo{year}{accessed
  April 14, 2017}.
\bibAnnoteFile{RootServersOrg}

\bibitem{RootServersIPv6}
\textsc{\bibinfo{author}{root servers.org}},
  \bibinfo{title}{announcement-of-ipv6-addresses},
  \bibinfo{howpublished}{http://www.root-servers.org/news/announcement-of-ipv6-addresses.txt},
  \bibinfo{year}{accessed April 14, 2017}.
\bibAnnoteFile{RootServersIPv6}

\bibitem{rfc5625}
\textsc{\bibinfo{author}{Bellis, R.}}, \bibinfo{title}{Rfc 5625: Dns proxy
  implementation guidelines},  (\bibinfo{year}{2009}).
\bibAnnoteFile{rfc5625}

\bibitem{rfc6895}
\textsc{\bibinfo{author}{Eastlake~3rd, D.}}, \bibinfo{title}{Rfc 6895: Domain
  name system (dns) iana considerations},  (\bibinfo{year}{2013}).
\bibAnnoteFile{rfc6895}

\bibitem{tcpBook}
\textsc{\bibinfo{author}{Fall, K.~R.} \& \bibinfo{author}{Stevens, W.~R.}},
  \bibinfo{title}{TCP/IP illustrated, volume 1: The protocols},
  \bibinfo{edition}{second} ed., \bibinfo{publisher}{addison-Wesley},
  \bibinfo{year}{2011}.
\bibAnnoteFile{tcpBook}

\bibitem{rfc2136}
\textsc{\bibinfo{author}{Bound, J.} \& \bibinfo{author}{Rekhter, Y.}},
  \bibinfo{title}{Rfc 2136: Dynamic updates in the domain name system (dns
  update)},  (\bibinfo{year}{1997}).
\bibAnnoteFile{rfc2136}

\bibitem{rfc2142}
\textsc{\bibinfo{author}{Crocker, D.}}, \bibinfo{title}{Rfc 2142: Mailbox names
  for common services, roles and functions}, \bibinfo{journal}{Network Working
  Group, May},  (\bibinfo{year}{1997}).
\bibAnnoteFile{rfc2142}

\bibitem{rfc3596}
\textsc{\bibinfo{author}{Thomson, S.}, \bibinfo{author}{Huitema, C.},
  \bibinfo{author}{Ksinant, V.}, \& \bibinfo{author}{Souissi, M.}},
  \bibinfo{title}{Rfc 3596: Dns extensions to support ip version 6},
  \bibinfo{type}{Tech. Rep.}, \bibinfo{year}{2003}.
\bibAnnoteFile{rfc3596}

\bibitem{rfc2845}
\textsc{\bibinfo{author}{Vixie, P.}, \bibinfo{author}{Gudmundsson, O.},
  \bibinfo{author}{Eastlake~3rd, D.}, \& \bibinfo{author}{Wellington, B.}},
  \bibinfo{title}{Rfc 2845: Secret key transaction authentication for dns
  (tsig)}, \bibinfo{type}{Tech. Rep.}, \bibinfo{year}{2000}.
\bibAnnoteFile{rfc2845}

\bibitem{rfc2931}
\textsc{\bibinfo{author}{Eastlake, D.~E.} et~al.}, \bibinfo{title}{Rfc 2931:
  Dns request and transaction signatures ( sig(0)s )},  (\bibinfo{year}{2000}).
\bibAnnoteFile{rfc2931}

\bibitem{rfc2535}
\textsc{\bibinfo{author}{Eastlake, D.~E.} et~al.}, \bibinfo{title}{Rfc2535:
  Domain name system security extensions},  (\bibinfo{year}{1999}).
\bibAnnoteFile{rfc2535}

\bibitem{rfc6891}
\textsc{\bibinfo{author}{Damas, J.}, \bibinfo{author}{Graff, M.}, \&
  \bibinfo{author}{Vixie, P.}}, \bibinfo{title}{Rfc 6891: Extension mechanisms
  for dns (edns (0))},  (\bibinfo{year}{2013}).
\bibAnnoteFile{rfc6891}

\bibitem{rfc3425}
\textsc{\bibinfo{author}{Lawrence, D.~C.}}, \bibinfo{title}{Rfc 3425:
  Obsoleting iquery},  (\bibinfo{year}{2002}).
\bibAnnoteFile{rfc3425}

\bibitem{rfc1995}
\textsc{\bibinfo{author}{Ohta, M.}}, \bibinfo{title}{Rfc 1995: Incremental zone
  transfer in dns},  (\bibinfo{year}{1996}).
\bibAnnoteFile{rfc1995}

\bibitem{rfc1912}
\textsc{\bibinfo{author}{Barr, D.}}, \bibinfo{title}{Rfc 1912: Common dns
  operational and configuration errors}, \bibinfo{journal}{International
  Engineering Task Force, Status: Standard. Availabl e at http://www. ietf.
  org/rfc/rfc1912. txt},  (\bibinfo{year}{1996}).
\bibAnnoteFile{rfc1912}

\bibitem{rfc1996}
\textsc{\bibinfo{author}{Vixie, P.}}, \bibinfo{title}{Rfc 1996: A mechanism for
  prompt notification of zone changes (dns notify)}, \bibinfo{type}{Tech.
  Rep.}, \bibinfo{year}{1996}.
\bibAnnoteFile{rfc1996}

\bibitem{rfc2930}
\textsc{\bibinfo{author}{Eastlake, D.}}, \bibinfo{title}{Rfc 2930: Secret key
  establishment for dns (tkey rr)}, \bibinfo{journal}{IETF, September},
  (\bibinfo{year}{2000}).
\bibAnnoteFile{rfc2930}

\bibitem{rfc4033}
\textsc{\bibinfo{author}{Arends, R.}, \bibinfo{author}{Austein, R.},
  \bibinfo{author}{Larson, M.}, \bibinfo{author}{Massey, D.}, \&
  \bibinfo{author}{Rose, S.~W.}}, \bibinfo{title}{Rfc 4033: Dns security
  introduction and requirements}, \bibinfo{journal}{Internet Engineering Task
  Force (IETF)},  (\bibinfo{year}{2005}).
\bibAnnoteFile{rfc4033}

\bibitem{rfc4034}
\textsc{\bibinfo{author}{Arends, R.}, \bibinfo{author}{Austein, R.},
  \bibinfo{author}{Larson, M.}, \bibinfo{author}{Massey, D.}, \&
  \bibinfo{author}{Rose, S.}}, \bibinfo{title}{Rfc 4034: Resource records for
  the dns security extensions (2005)}, \bibinfo{journal}{Internet Engineering
  Task Force (IETF)}.
\bibAnnoteFile{rfc4034}

\bibitem{rfc4035}
\textsc{\bibinfo{author}{Arends, R.}, \bibinfo{author}{Austein, R.},
  \bibinfo{author}{Larson, M.}, \bibinfo{author}{Massey, D.}, \&
  \bibinfo{author}{Rose, S.}}, \bibinfo{title}{Rfc 4035: Protocol modifications
  for the dns security extensions}, \bibinfo{journal}{Internet Engineering Task
  Force},  (\bibinfo{year}{2005}).
\bibAnnoteFile{rfc4035}

\bibitem{rfc4470}
\textsc{\bibinfo{author}{Weiler, S.} \& \bibinfo{author}{Ihren, J.}},
  \bibinfo{title}{Rfc 4470: Minimally covering nsec records and dnssec on-line
  signing}, \bibinfo{year}{2006}.
\bibAnnoteFile{rfc4470}

\bibitem{rfc4509}
\textsc{\bibinfo{author}{Hardaker, W.}}, \bibinfo{title}{Rfc 4509: Use of
  sha-256 in dnssec delegation signer (ds) resource records (rrs)},
  (\bibinfo{year}{2006}).
\bibAnnoteFile{rfc4509}

\bibitem{rfc5011}
\textsc{\bibinfo{author}{StJohns, M.}}, \bibinfo{title}{Rfc 5011: Automated
  updates of dns security (dnssec) trust anchors},  (\bibinfo{year}{2007}).
\bibAnnoteFile{rfc5011}

\bibitem{rfc5155}
\textsc{\bibinfo{author}{Laurie, B.}, \bibinfo{author}{Sisson, G.},
  \bibinfo{author}{Arends, R.}, \bibinfo{author}{Blacka, D.}, et~al.},
  \bibinfo{title}{Rfc 5155: Dns security (dnssec) hashed authenticated denial
  of existence}, \bibinfo{journal}{Request for Comments}, vol.
  \bibinfo{volume}{5155} (\bibinfo{year}{2008}).
\bibAnnoteFile{rfc5155}

\bibitem{rfc6944}
\textsc{\bibinfo{author}{Rose, S.}}, \bibinfo{title}{Rfc 6944: Applicability
  statement: Dns security (dnssec) dnskey algorithm implementation status},
  (\bibinfo{year}{2013}).
\bibAnnoteFile{rfc6944}

\bibitem{RootDNSSEC}
\textsc{\bibinfo{author}{ICANN} \& \bibinfo{author}{VeriSign}},
  \bibinfo{title}{Root dnssec},
  \bibinfo{howpublished}{http://www.root-dnssec.org}, \bibinfo{year}{accessed
  July 19, 2017}.
\bibAnnoteFile{RootDNSSEC}

\bibitem{RootTrustAnchors}
\textsc{\bibinfo{author}{IANA}}, \bibinfo{title}{Index of /root-anchors},
  \bibinfo{howpublished}{https://data.iana.org/root-anchors},
  \bibinfo{year}{accessed July 19, 2017}.
\bibAnnoteFile{RootTrustAnchors}

\bibitem{TldDNSSEC}
\textsc{\bibinfo{author}{ICANN}}, \bibinfo{title}{Icann research - tld dnssec
  report},
  \bibinfo{howpublished}{http://stats.research.icann.org/dns/tld\_report/index.html},
  \bibinfo{year}{accessed July 19, 2017 and accessed August, 2019}.
\bibAnnoteFile{TldDNSSEC}

\bibitem{DNSSECRegistrars}
\textsc{\bibinfo{author}{ICANN}}, \bibinfo{title}{Deploying dnssec - icann},
  \bibinfo{howpublished}{https://www.icann.org/resources/pages/deployment-2012-02-25-en},
  \bibinfo{year}{accessed July 19, 2017}.
\bibAnnoteFile{DNSSECRegistrars}

\bibitem{rfc7766}
\textsc{\bibinfo{author}{Dickinson, J.}, \bibinfo{author}{Dickinson, S.},
  \bibinfo{author}{Bellis, R.}, \& \bibinfo{author}{Mankin, A.}},
  \bibinfo{title}{Rfc 7766: Dns transport over tcp - implementation
  requirements}, \bibinfo{type}{Tech. Rep.}, \bibinfo{year}{March 2016}.
\bibAnnoteFile{rfc7766}

\bibitem{rfc7858}
\textsc{\bibinfo{author}{Hu, Z.}, \bibinfo{author}{Zhu, L.},
  \bibinfo{author}{Heidemann, J.}, \bibinfo{author}{Mankin, A.}, \&
  \bibinfo{author}{Wessels, D.}}, \bibinfo{title}{Rfc 7858: Specification for
  dns over transport layer security (tls)}, \bibinfo{type}{Tech. Rep.},
  \bibinfo{year}{May 2016}.
\bibAnnoteFile{rfc7858}

\bibitem{tfoArticle}
\textsc{\bibinfo{author}{Radhakrishnan, S.}, \bibinfo{author}{Cheng, Y.},
  \bibinfo{author}{Chu, J.}, \bibinfo{author}{Jain, A.}, \&
  \bibinfo{author}{Raghavan, B.}}, \bibinfo{title}{Tcp fast open}, in
  \bibinfo{booktitle}{Proceedings of the Seventh COnference on emerging
  Networking EXperiments and Technologies}, p.~\bibinfo{pages}{21},
  \bibinfo{organization}{ACM}, \bibinfo{year}{2011}.
\bibAnnoteFile{tfoArticle}

\bibitem{rfc5827}
\textsc{\bibinfo{author}{Allman, M.}, \bibinfo{author}{Avrachenkov, K.},
  \bibinfo{author}{Ayesta, U.}, \bibinfo{author}{Blanton, J.}, \&
  \bibinfo{author}{Hurtig, P.}}, \bibinfo{title}{Rfc 5827: Early retransmit for
  tcp and stream control transmission protocol (sctp)}, \bibinfo{type}{Tech.
  Rep.}, \bibinfo{year}{2010}.
\bibAnnoteFile{rfc5827}

\bibitem{RACKTLP}
\textsc{\bibinfo{author}{Y.~Cheng, N.} \& \bibinfo{author}{Dukkipati, N.}},
  \bibinfo{title}{draft-ietf-tcpm-rack-02: Rack: a time-based fast loss
  detection algorithm for tcp}, \bibinfo{type}{Tech. Rep.},
  \bibinfo{year}{March 2017}.
\bibAnnoteFile{RACKTLP}

\bibitem{rfc2246}
\textsc{\bibinfo{author}{T.~Dierks, C.~A.}}, \bibinfo{title}{Rfc 2246: The tls
  protocol version 1.0},  (\bibinfo{year}{1999}).
\bibAnnoteFile{rfc2246}

\bibitem{rfc4346}
\textsc{\bibinfo{author}{Dierks, T.} \& \bibinfo{author}{Rescorla, E.}},
  \bibinfo{title}{Rfc 4346: The transport layer security (tls) protocol version
  1.1},  (\bibinfo{year}{2006}).
\bibAnnoteFile{rfc4346}

\bibitem{rfc5246}
\textsc{\bibinfo{author}{Dierks, T.} \& \bibinfo{author}{Rescorla, E.}},
  \bibinfo{title}{Rfc 5246: The transport layer security (tls) protocol version
  1.2 (2008)}.
\bibAnnoteFile{rfc5246}

\bibitem{rfc8446}
\textsc{\bibinfo{author}{Rescorla, E.}}, \bibinfo{title}{The transport layer
  security (tls) protocol version 1.3 (august 2018)}.
\bibAnnoteFile{rfc8446}

\bibitem{rfc5077}
\textsc{\bibinfo{author}{Salowey, J.}, \bibinfo{author}{Zhou, H.},
  \bibinfo{author}{Eronen, P.}, \& \bibinfo{author}{Tschofenig, H.}},
  \bibinfo{title}{Rfc 5077: Transport layer security (tls) session resumption
  without server-side state. 2008}.
\bibAnnoteFile{rfc5077}

\bibitem{rfc8484}
\textsc{\bibinfo{author}{McManus, P.} \& \bibinfo{author}{Hoffman, P.}},
  \bibinfo{title}{Dns queries over https (doh)},  (\bibinfo{year}{2018}).
\bibAnnoteFile{rfc8484}

\bibitem{ChrHttpPipe}
\textsc{\bibinfo{author}{chromium.org}}, \bibinfo{title}{Http pipelining},
  \bibinfo{howpublished}{https://www.chromium.org/developers/design-documents/network-stack/http-pipelining}.
\bibAnnoteFile{ChrHttpPipe}

\bibitem{MozHttpPipe}
\textsc{\bibinfo{author}{mozilla.org}}, \bibinfo{title}{Bug 264354 enable http
  pipelining by default},
  \bibinfo{howpublished}{https://bugzilla.mozilla.org/show_bug.cgi?id=264354}.
\bibAnnoteFile{MozHttpPipe}

\bibitem{connectionOrientedDNS}
\textsc{\bibinfo{author}{Zhu, L.}, \bibinfo{author}{Hu, Z.},
  \bibinfo{author}{Heidemann, J.}, \bibinfo{author}{Wessels, D.},
  \bibinfo{author}{Mankin, A.}, \& \bibinfo{author}{Somaiya, N.}},
  \bibinfo{title}{Connection-oriented dns to improve privacy and security}, in
  \bibinfo{booktitle}{Security and Privacy (SP), 2015 IEEE Symposium on}, pp.
  \bibinfo{pages}{171--186}, \bibinfo{organization}{IEEE},
  \bibinfo{year}{2015}.
\bibAnnoteFile{connectionOrientedDNS}

\bibitem{netalyzr}
\textsc{\bibinfo{author}{Weaver, N.}, \bibinfo{author}{Kreibich, C.},
  \bibinfo{author}{Nechaev, B.}, \& \bibinfo{author}{Paxson, V.}},
  \bibinfo{title}{Implications of netalyzr’s dns measurements}, in
  \bibinfo{booktitle}{Proceedings of the First Workshop on Securing and
  Trusting Internet Names (SATIN), Teddington, United Kingdom},
  \bibinfo{year}{2011}.
\bibAnnoteFile{netalyzr}

\bibitem{burstyDNS}
\textsc{\bibinfo{author}{Schomp, K.}, \bibinfo{author}{Rabinovich, M.}, \&
  \bibinfo{author}{Allman, M.}}, \bibinfo{title}{Towards a model of dns client
  behavior}, in \bibinfo{booktitle}{International Conference on Passive and
  Active Network Measurement}, pp. \bibinfo{pages}{263--275},
  \bibinfo{organization}{Springer}, \bibinfo{year}{2016}.
\bibAnnoteFile{burstyDNS}

\bibitem{ipSpoofing}
\textsc{\bibinfo{author}{Beverly, R.}, \bibinfo{author}{Koga, R.}, \&
  \bibinfo{author}{Claffy, K.}}, \bibinfo{title}{Initial longitudinal analysis
  of ip source spoofing capability on the internet},  (\bibinfo{year}{2013}).
\bibAnnoteFile{ipSpoofing}

\bibitem{rfc7258}
\textsc{\bibinfo{author}{Farrell, S.} \& \bibinfo{author}{Tschofenig, H.}},
  \bibinfo{title}{Rfc 7258: pervasive monitoring is an attack},
  \bibinfo{journal}{Internet Engineering Task Force: Best Current Practice},
  (\bibinfo{year}{2014}).
\bibAnnoteFile{rfc7258}

\bibitem{tdnsGit}
\textsc{\bibinfo{author}{Sinodun.com}}, \bibinfo{title}{Sinodun it git server},
  \bibinfo{howpublished}{https://portal.sinodun.com/stash/projects/TDNS}.
\bibAnnoteFile{tdnsGit}

\bibitem{tdnsWeb}
\textsc{\bibinfo{author}{Lab, A.}}, \bibinfo{title}{Ant software},
  \bibinfo{howpublished}{https://ant.isi.edu/software/index.html}.
\bibAnnoteFile{tdnsWeb}

\bibitem{dnsPrivacy}
\textsc{\bibinfo{author}{dnsprivacy.org}}, \bibinfo{title}{Using a tls proxy},
  \bibinfo{howpublished}{https://dnsprivacy.org/wiki/display/DP/Using+a+TLS+proxy}.
\bibAnnoteFile{dnsPrivacy}

\bibitem{RFC2397}
\textsc{\bibinfo{author}{Masinter, L.}}, \bibinfo{title}{The" data" url
  scheme},  (\bibinfo{year}{1998}).
\bibAnnoteFile{RFC2397}

\bibitem{httpArchive}
\textsc{\bibinfo{author}{httparchive.org}}, \bibinfo{title}{Homepage},
  \bibinfo{howpublished}{http://httparchive.org/}.
\bibAnnoteFile{httpArchive}

\bibitem{rfc5702}
\textsc{\bibinfo{author}{Jansen, J.}}, \bibinfo{title}{Rfc 5702: Use of sha-2
  algorithms with rsa in dnskey and rrsig resource records for dnssec},
  (\bibinfo{year}{2009}).
\bibAnnoteFile{rfc5702}

\bibitem{rfc5933}
\textsc{\bibinfo{author}{Dolmatov, V.}}, \bibinfo{title}{Rfc 5933: Use of gost
  signature algorithms in dnskey and rrsig resource records for dnssec},
  (\bibinfo{year}{2010}).
\bibAnnoteFile{rfc5933}

\bibitem{rfc6605}
\textsc{\bibinfo{author}{Hoffman, P.}}, \bibinfo{title}{Rfc 6605: Elliptic
  curve digital signature algorithm (dsa) for dnssec},  (\bibinfo{year}{2012}).
\bibAnnoteFile{rfc6605}

\bibitem{rfc6781}
\textsc{\bibinfo{author}{Kolkman, O.}, \bibinfo{author}{Mekking, W.}, \&
  \bibinfo{author}{Gieben, R.}}, \bibinfo{title}{Rfc 6781: Dnssec operational
  practices, version 2. internet engineering task force (ietf), 2012}.
\bibAnnoteFile{rfc6781}

\end{thebibliography}
