\label{ch:results}
We will present our results and their interpretation in this chapter. We followed a few steps to obtain these results. First we researched how many DNS queries are commonly triggered when loading a webpage when nothing is cached based on the HTTP Archive web crawler's results. We then choose a representative website which has a number of domains close to the median. We then measure the comparative performance of UDP, TCP and TLS in different scenario. First through synthetic measurement then through measurement on a live service. In this case the the live service is the google public DNS.


\section{Number of unique domain names per website}
\label{sec:number of domains per website}
We started to wonder about the number of unique domain names involved in a website after reading in \emph{Towards a Model of DNS Client Behaviour}\cite{burstyDNS} that, according to data from HTTP Archive\cite{httpArchive}, the average webpage involve 16 different hostnames. While an average is nice, we weren't satisfied and wanted to find a better characterization of the number of domain names per website.

To do so, we used the publicly available mysql dumps of HTTP Archive. More specifically, \emph{Apr 15 2016 IE: pages} which contains information about 479 243 websites homepage from the Alexa Top 1 000 000 of the 15th of April 2016.

To investigate this data set, we created a script in python who generates statistics and charts from the data loaded in a mysql database. The script calculate the percentiles of the number of domains per websites with a step of a tenth of a percent. We consider this step big enough to be relevant considering the size of the data set. The number of domains for a given percentile is shown in FIG. \ref{fig:domains_percentile}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{image/numDomains-Percentile-Detailed-XY.png}
\caption{Number of domain names per percentile of the data set.}\label{fig:domains_percentile}
\end{figure}

As can be seen on Fig. \ref{fig:domains_percentile}, which plots the number of domains for each percentile (one point each), we observe a succession of plateaus at the start and higher increase in the number of domains per percentile steps above the 75th percentile. This shows that most website have a small number of domains with the 28.9th percentile at 6 domains, the 45.5th at 10 domains and the 74.3th at 21 domains.

A similar inquiry was conducted in \emph{Connection-oriented DNS to improve privacy and security}\cite{connectionOrientedDNS} from a different data set. Their conclusion was that 62\% of web pages have 4 or more unique domain names and 32\% have 10 or more. \\

Based on this data we chose to use \textit{amazon.com} as example to generate queries in our tests. This website generate ten queries to unique domains which is at the 45.5th percentile based on our dataset. 

Since this is close to the median value of our dataset, we consider that it is representative

A more detailed version of this section is available in Appendix \ref{ax:number of domains per website}

\section{Wprof data amazon.com scenario: Synthetic results}
Synthetic results generated using the Scenario A described in Section \ref{sec:topology}. The following notation will be used in the charts shown in this Section. \\ UDP 1 is always standard UDP. The reference table for the following two subsections is shown in Table \ref{tab:ref_baseline}. The reference table for Subsection \ref{sub:synth_lag} is shown in Table \ref{tab:ref_loss}.

\begin{table}[!ht]
\parbox{.45\linewidth}{
\centering
\tiny
\begin{tabular}{llllll}
Name    & Nagle & Cork & Nodelay & TFO & TLS Resumption \\
TCP 1   & y     &      &         &     &                \\
TCP 2   & y     &      &         & y   &                \\
TCP 3   &       & y    &         &     &                \\
TCP 4   &       & y    &         & y   &                \\
TCP 5   &       &      & y       &     &                \\
TCP 6   &       &      & y       & y   &                \\
TLS 1   & y     &      &         &     &                \\
TLS 2   & y     &      &         &     & y              \\
TLS 3   &       & y    &         &     &                \\
TLS 4   &       & y    &         &     & y              \\
TLS 5   &       &      & y       &     &                \\
TLS 6   &       &      & y       &     & y              \\
HTTPS 1 & y     &      &         &     &                \\
HTTPS 2 &       & y    &         &     &                \\
HTTPS 3 &       &      & y       &     &               
\end{tabular}
\caption{\tiny Reference table for graphs in subsections Baseline and Latency. All TCP, TLS and HTTPS use ER\_AND\_TLP\_DELAYED}\label{tab:ref_baseline}
}
%\hfill
\parbox{.45\linewidth}{
\flushright
\tiny
\begin{tabular}{llll}
Name    & Retransmission type                                                                        & TFO & TLS Resumption \\
TCP 1   & No Early Retransmit                                                                        &     &                \\
TCP 2   & Early Retransmit                                                                           &     &                \\
TCP 3   & Early Retransmit Delayed                                                                   &     &                \\
TCP 4   & Early Retransmit and Tail Loss Probe Delayed                                               &     &                \\
TCP 5   & Tail Loss Probe                                                                            &     &                \\
TLS 1   & No Early Retransmit                                                                        &     & y              \\
TLS 2   & Early Retransmit                                                                           &     & y              \\
TLS 3   & Early Retransmit Delayed                                                                   &     & y              \\
TLS 4   & Early Retransmit and Tail Loss Probe Delayed                                               &     & y              \\
TLS 5   & Tail Loss Probe                                                                            &     & y              \\
HTTPS 1 & No Early Retransmit                                                                        &     &                \\
HTTPS 2 & Early Retransmit                                                                           &     &                \\
HTTPS 3 & Early Retransmit Delayed                                                                   &     &                \\
HTTPS 4 & Early Retransmit and ail Loss Probe Delayed                                                &     &                \\
HTTPS 5 & Tail Loss Probe                                                                            &     &               
\end{tabular}
\caption{\tiny Reference table for graphs in subsection Loss.}\label{tab:ref_loss}
}
\end{table}

\subsection{Baseline}

For this baseline, the expected results are that UDP outperforms TCP which outperforms TLS which outperforms HTTPS, this is because from left to right the protocols gain in complexity and thus add an additional overhead. We expect Nodelay to outperform both Nagle and Cork. It is less clear who between Nagle and Cork will outperform each other because Cork has a longer timeout than Nagle but we tried to uncork at appropriate times within the benchmark to improve Cork's performance. We expect that TFO will improve the performance of the first TCP queries and should thus improve the performance of the following ones because the time they are sent at is dependent on the reception of the answer of the first query. We use TLS resumption with TLS which we expect will speed up the TLS handshake and thus like with TFO for TCP reduce the time at which the first answer is received compared to TLS without resumption.\\
Unfortunately, we are unable to test TFO with TLS and HTTPS because the first packet to be sent is part of the TLS handshake and is hidden within the ssl and pycurl module respectively. Similarly we are also unable to set TLS resumption for HTTPS because the change from a TCP socket to a TLS socket is hidden within the pycurl module.\\
\\
\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/baseline50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2050 YM2350".png}
\caption{Synthetic Baseline: Boxplot of the connection duration per scenario. Y axis range limited between 2050 and 2350 ms}\label{fig:synth_basleline}
\end{figure}

The first thing that is obvious when looking at FIG. \ref{fig:synth_basleline} is that the HTTPS Cork values are not visible. They are above the maximum value for Y on the chart more specifically they vary between 3200 ms and 3300 ms which is much worse than the others. This is due to the inability of easily uncork socket within pycurl for HTTPS because the handshakes are hidden within the module and are unable to uncork after them which introduces additional delay to conclude the handshake.\\
More generally Cork is always outperformed by Nagle and No Delay however we can saw than for TCP. The exception here is Cork using TFO which is 200 ms slower. This is due to an implementation mistake as the socket is not uncorked after establishing the connection which lead to a 200 ms timeout. \\
Another surprise is that while TFO seems to have no discernible effect when using NO Delay, when using Nagle and TFO, while the median to get the first answer stays the same. The range over which the answer is received is much greater than without TFO as shown in FIG. \ref{fig:synth_basleline_tfo}. Our result also do not show a significant difference between Nagle and No Delay when TFO isn't used. \\
\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{"image2/baseline50 merged/timeline/60/TCP NAGLE TFO Comparison".png}
\caption{Synthetic Baseline: Boxplot for TCP 1 \& 2 showing up to the first DNS answer.}\label{fig:synth_basleline_tfo}
\end{figure}

In FIG. \ref{fig:synth_basleline}, the values between TLS with or without resumption don't seem to vary much. In FIG. \ref{fig:synth_basleline_resumption}, we can indeed see that for TLS with No Delay there doesn't to any gain from using TLS Resumption. After additional research, we found that Resumption is not supported for TLS1.3 in python3.8. Unfortunately, TLS1.3 is also the only version of TLS we can use to extract the TLS connections secrets and are not able to switch to another one as a result. which means we won't be able to investigate the effect of TLS resumption.\\
\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{"image2/baseline50 merged/timeline/10/TLS Resumption Comparison".png}
\caption{Synthetic Baseline: Boxplot for TLS 5 \& 6 showing up to the first DNS answer.}\label{fig:synth_basleline_resumption}
\end{figure}

UDP and TCP No Delay are also very close with a delta in connection length of about 5 ms second in favour of UDP. TLS No Delay is also only between 5 and 7 ms behind TCP No Delay.

\subsection{Latency}
With the introduction of latency, HTTPS Cork continue to perform badly with connection duration of 15.3 seconds and 8.9 seconds for respectively 400 ms and 200 ms of latency whereas every other case is under 6.1 seconds and 4.1 seconds. \\
However, it is the time to shine for UDP. This is due to the fact that UDP doesn't use additional packets like TCP, TLS and HTTPS.\\
\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/lag200-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2400 YM4500".png}
\caption{Synthetic 200ms latency: Boxplot of the connection duration per scenario. Y axis range limited between 2400 and 4500 ms}\label{fig:synth_lag200}
\end{figure}
\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/lag400-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2800 YM7000".png}
\caption{Synthetic 400ms latency: Boxplot of the connection duration per scenario. Y axis range limited between 2800 and 7000 ms}\label{fig:synth_lag400}
\end{figure}
TCP Nagle Surprisingly outperforms TCP No Delay at 200 ms Latency but the situation is reversed at 400 ms latency where even TCP Cork No TFO has better performance. The fact that TCP Nagle has better performance at 200 ms latency than No Delay is very unexpected.\\
The respective performance of the different measure for TLS are as expected. For HTTPS Nagle and No Delay, their performance is close to TLS No delay and at 200 ms outperforms TLS Nagle.

\subsection{Packet Loss}
\label{sub:synth_loss}
The chosen methodology to evaluate packet loss is the same than for the baseline or lag. However as visible in FIG.  \ref{fig:synth_loss1} \&  \ref{fig:synth_loss.5} this is not a good approach to evaluate the different retransmission mechanism available in TCP. A good path forward would be to instead compare timeline type graph for all the packet similar to the ones shown in \ref{fig:synth_basleline_resumption} where the loss happen on the same packet every time to make it consistent between the different mechanism. This would help in evaluating the impact of those mechanism. Different type of packet could also be dropped. Query and answers are the obvious choice but packet part of the different handshakes could also be interesting to investigate.

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/loss.5-Nagle-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2000 YM2500".png}
\caption{Synthetic 0.5\% Packet Loss: Boxplot of the connection duration per scenario. Y axis range limited between 2000 and 2500 ms}\label{fig:synth_loss.5}
\end{figure}


\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/loss1-Nagle-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2050 YM2350".png}
\caption{Synthetic 1\% Packet Loss: Boxplot of the connection duration per scenario. Y axis range limited between 2050 and 2350 ms}\label{fig:synth_loss1}
\end{figure}
\clearpage
\section{Wprof data amazon.com scenario: Live services results}
Results generated using google public DNS and the scenario B described in Section \ref{sec:topology}.
Like in the previous Section, Table \ref{tab:ref_baseline} \& Table \ref{tab:ref_loss} are used respectively for the first two section and the third section.

\subsection{Baseline}
The result expected for this baseline are the same than the one for the synthetic baseline. However, we expect that some results that were indistinguishable could now be differentiated due to the increased RTT which becomes 16.7 ms on average compared to the previous RTT of 0.9 ms.\\
\\
\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/google-baseline50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2050 YM2400".png}
\caption{Google DNS Baseline: Boxplot of the connection duration per scenario. Y axis range limited between 2050 and 2400 ms}\label{fig:google_basleline}
\end{figure}

In  accordance with our observations in the precedent section, now that we use a service with a higher latency, the difference between UDP and TCP is more obvious and UDP performs better than the other protocols as expected.
A difference that can be seen in \ref{fig:google_basleline} however is that TCP CORK does better than TCP Nagle and about the same than TCP No Delay. The performance of Cork for TLS and HTTPS stay consistent with the synthetic results for the baseline. While the use of TFO doesn't seem to affect the minimum at which the first answers happens, when analysing the results you can see that they are more likely to happen sooner.

\subsection{Latency}
The most surprising result we get is that at 400 ms latency TCP Cork with or without TFO outperforms UDP. This was caused by the DNS server closing the connection early for every samples related to TCP Cork with 400 ms of latency. The comparative difference of UDP is higher in presence of latency like in the synthetic results. TLS No Delay outperforms both Nagle and Cork while HTTPS Nagle and No Delay rank between between TLS No delay and TLS Nagle like in the Synthetic measures. HTTPS CORK continue to under perform. No impact of TFO can be observed for TCP.

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/google-lag200-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2400 YM4200".png}
\caption{Google DNS 200ms latency: Boxplot of the connection duration per scenario. Y axis range limited between 2400 and 4200 ms}\label{fig:google_lag200}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/google-lag400-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2600 YM6100".png}
\caption{Google DNS 400ms latency: Boxplot of the connection duration per scenario. Y axis range limited between 2600 and 6100 ms}\label{fig:google_lag400}
\end{figure}

\subsection{Packet Loss}
As mentioned in Subsection \ref{sub:synth_loss}, the chosen methodology to evaluate packet loss is the same than for the baseline or lag and as visible in FIG.  \ref{fig:google_loss1} \&  \ref{fig:google_loss.5} this is not a good approach and another one needs to be used.
\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/google-loss1-Nagle-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2050 YM2400".png}
\caption{Google DNS 1\% Packet Loss: Boxplot of the connection duration per scenario. Y axis range limited between 2050 and 2350 ms}\label{fig:google_loss1}
\end{figure}

\begin{figure}[!ht]
\centering
\includegraphics[width=1\textwidth]{"image2/google-loss.5-Nagle-50 merged/charts_merged/Exchange duration UDP TCP TLS HTTPS linear short labels Ym2050 YM2400".png}
\caption{Google DNS 0.5\% Packet Loss: Boxplot of the connection duration per scenario. Y axis range limited between 2050 and 2400 ms}\label{fig:google_loss.5}
\end{figure}

\clearpage
\section{Recommendation on the options to use in different use case}
\label{sec:recomendation}
\subsection{Standard environment}
As shown in our results, from a pure performance point of view UDP always outperforms the others.
However if there isn't much latency, TCP No Delay, TLS No Delay and HTTPS No Delay are close in performance.
If in addition to performance, privacy is also a concern then TLS No Delay is good choice with HTTPS No delay following him closely in terms of performance. Additionally, it might be easier to DNS over HTTPS public servers as the industry seems to be adopting it faster than DNS over TLS.
Unfortunately, we didn't see much performance improvements from using TFO in our results and weren't able to test TLS resumption which could improve the results of both TLS No Delay and HTTPS No Delay when compared to TCP.

\subsection{Laggy environment}
In environments with latency, our results have shown that UDP has a significantly better performance than the alternatives. Here the difference between TCP No Delay, TLS No Delay and HTTPS increases while still being manageable. The main reason to switch away from UDP in this kind of environments would be privacy. In this case, for latency around 200 ms, I would recommend TLS No Delay but HTTPS No Delay is also acceptable. At around 400 ms, I would avoid to use HTTPS, as our results have shown that the difference in performance with TLS No Delay is much higher compared to 200 ms latency.

\subsection{Lossy environment}
While we weren't able to get results for lossy environments and thus can't recommend specific retransmission mechanism and protocols based on them. I would still favour a TCP based protocols because there is no standard UDP retransmission scheme which can thus be different for each software. I would prefer TCP because you avoid a potential packet loss during the TLS handshake. I would also prefer No Delay to avoid grouping multiple queries in a single packet. If privacy is needed or desired both TLS No Delay and HTTPS No Delay could be used as they have comparable performance in low latency environments.


