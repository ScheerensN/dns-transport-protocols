\label{sec:networkAspects}
\DNS messages can be transmitted on top of several protocols. RFC 1034 and RFC 1035 specified the transport over \acrfull{UDP} and \acrfull{TCP}\cite{rfc1034,rfc1035}. However the implementations of \DNS servers did not support \gls{TCP} as an alternative to \gls{UDP} for standard queries. \gls{TCP} was used for specific purpose such as zone transfers or as a fall back when the \DNS response was bigger than the limit (512 bytes or the limit negotiated with an EDNS(0) \RR). Nowadays, new RFCs proposing alternatives to \gls{UDP} are appearing such as \DNS over \gls{TCP} in RFC 7766 and \DNS over \gls{TLS} in RFC 7858\begin{comment}, \DNS over \gls{DTLS} in RFC 8094 and \DNS over HTTPS in a recent RFC draft\cite{rfc7766,rfc7858,rfc8094,draftHTTPS}\end{comment}
\cite{rfc7766,rfc7858}.
\hfill \break

This chapter presents the limitations of \DNS over \UDP and the advantages of transporting it via \TCP. Before doing that \TCP connection establishment and termination is described. Some \TCP options and protocols establishment optimisation, like delay or memory, are recommended for \DNS over \TCP. We then present \TLS and  and how \DNS can be transported over the protocol and conclude by discussing the motivations to use \TCP and \TLS instead of the more common \UDP.

\section{DNS over UDP}
\label{sec:DNSoUDP}
\DNS queries and zone maintenance operations use the default port for \DNS: 53. The \DNS header and message is put as payload of one \UDP packet and sent. RFC 1035 specifies that \DNS message, \DNS payload, transmitted over UDP should be 512 bytes or less. If the message is longer, the message is truncated to 512 bytes and the TC bit is set in the header. The sender of the query can then retry with \TCP. RFC 1035 recommends the usage of UDP for queries, however, it is not acceptable for zone transfers\cite{rfc1035}. Historically, exceeding this limit was avoided at all costs. 

This limit was set in RFC1035 for performance reason\cite{dnsAndBind10}. However due to the emergence of technologies such as IPv6 and \DNSSEC amongst others, it is now too restrictive to limit the size of all \DNS message over \UDP to 512 bytes. The \gls{EDNS} mechanisms, discussed in \ref{sec:EDNS0}, allow to overcome this limit to transmit bigger \DNS message over \UDP. It should be noted that his value is dissociated from the path MTU which could be smaller and would cause IP fragmentation.\cite{dnsAndBind10,rfc6891}
\hfill \break

Because \UDP isn't a reliable transport protocol, both the client and server should implement a retransmission mechanism.

\section{Transmission Control Protocol (TCP)}
\label{sec:TCP}
\TCP, specified in RFC 793 with corrections in RFC 1122 and updated by several other RFCs, provides a reliable connection-oriented byte stream service. It is connection-oriented because a connection must be established between two hosts before they can exchange data. \TCP also provides a byte stream abstraction to applications as the applications only see a stream of bytes with no indication on how they were bundled for transport. The service \TCP provides is a full Duplex which means that data can be sent independently in each direction.
\begin{comment}
\subsection{\TCP Header}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{image/TCP_header2.png}
\caption{TCP header format\cite{tcpBook}.}\label{fig:TCP_header}
\end{figure}

The \TCP header is shown in FIG. \ref{fig:TCP_header}. This header is significantly more complex than the \UDP header which is because \TCP must keep each end of the connection synchronized about the current state.
\begin{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\item The first two fields of the header are the Source Port and Destination Port which fulfil the same function than with \UDP.
\item The sequence number field identifies the byte in the stream of data that the first byte in the segment represent.
\item The Acknowledgement Number contains the next sequence number that the sender expect to receive. Therefore, this field should contain the sequence number of the last received byte + 1. This field is only valid if the ACK bit is set.
\item The header length: The length of the header in 32-bit words. This is required because the options field has a variable length.
\item Congestion Window Reduced (CWR) flag.
\item The ECN Echo (ECE) flag.
\item The urgent (URG) flag.
\item The acknowledgement (ACK) flag.
\item The push (PSH) flag.
\item The reset (RST) flag.
\item The SYN flag.
\item The FIN flag.
\item The Window Size. This value defines the number of bytes, with the first one being the ACK Number, the host is willing to receive.
\item \TCP Checksum. The checksum is calculated on the \TCP header, \TCP data and some fields of the IP header using a pseudo-header like \UDP.
\item The Urgent pointer is only used when the URG flag is set and specify a positive offset that must be added to the sequence number to yield the sequence number of the last byte of urgent data.
\item Several Options can be added. Common options include \gls{MSS} which specifies the maximum segment the sender is willing to receive,  \gls{SACK}, Timestamp and Window Scale.
\end{itemize}
\end{comment}
\subsection{\TCP Connection Establishment and Termination}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{image/TCP_3WH2.png}
\caption{TCP connection initiation (3-way Handshake) and connection termination. Numbers added for convenience\cite{tcpBook}.}\label{fig:TCP_3WH}
\end{figure}

FIG. \ref{fig:TCP_3WH} shows a time line of the segments transmitted between the client and the server to initiate and to close a \TCP connection. The graceful method is shown. The first step called the \gls{3WH} is to initiate a connection.:
\begin{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\item[1] The client sends a SYN segment, a TCP/IP packet with the SYN bit set, with the port number to which it wants to connect, with the  client's \gls{ISN} and with options. It should be noted that the \gls{ISN} is typically a semi-random value to avoid segments from previous connections to enter the current connection and to prevent someone to interrupt the session by forging a \TCP segment.
\item[2] The server responds with its own SYN segment which contains the server's \gls{ISN}. Additionally, the server also acknowledges the reception of the first segment. This is done by setting the \gls{ACK} bit and putting the client's \gls{ISN} + 1 in the Acknowledgement Number. The +1 in the acknowledgement is there because a SYN segment consumes one sequence number and it is retransmitted if it is lost. Options can be included
\item[3] The client then acknowledges the server's SYN segment by sending an ACK segment with the server's \gls{ISN} + 1 as Acknowledgement Number. Options can be included.
\end{itemize}
The connection is established after step 3 for the client and after the reception of the third segment for the server. Once the connection is established, data can be transmitted between two hosts.
\hfill \break
\begin{comment}
The connections can be terminated by both sides. For convenience, we will consider that the client close the connection. The process is as follows:
\begin{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\item[4] The client sends a FIN segment, segment with the FIN bit set, specifying the current expected sequence number K. Additionally, an ACK for the last data received L.
\item[5] The server send an ACK segment with value K+1 to indicate reception of the FIN segment.
\item[6] The application is notified of that the other end has performed a close. This usually results in the application executing its own close. It then sends its own FIN segment which also acknowledges the last data received (the FIN segment) and thus the Acknowledgement Number is K+1. 
\item[7] Finally, the client send an ACK segment to acknowledged the last FIN segment.
\end{itemize}
It should be noted that FIN segment are retransmitted until and ACK is received. Simultaneous open, simultaneous close and half-close are also possible but they won't be discussed here as they are less common.

\subsection{TCP Keep Alive}
An idle \TCP connection doesn't exchange any data between host. This means if neither of the hosts is sending data, nothing is exchange between the hosts. \TCP Keep Alive is a method to probe its peer without affecting the content of the datastream. \TCP Keep Alive is turned off by default but can be requested by each side for their direction of the connection. Keep Alive can be set for one side, both side or neither. TCP Keep Alive is driven by a timer. A Keep Alive Probe, an empty or 1 byte (garbage data) segment with segment number equal to one less than the largest ACK number seen,  is sent hen the timer expires. Either the receiver responds with an ACK and the timer is reset or no response is received. In the second case, the probe is retransmitted regularly with a period of \texttt{Keepalive Interval} until a number of probes equal to \texttt{Keepalive probes} is reached. At this point the peer is determined as unreachable and the connection is terminated. It should be noted that because neither the probe nor the ACK contain new data, the are not retransmitted if lost. Thus the lack of response of single probe isn't sufficient to determine if the connection has stopped working.

\subsection{Reliability}
The reliability in \TCP is provided by the use of checksums, a retransmission timer, acknowledgements, sequence numbers.
When \TCP receives data, it sends an \gls{ACK} to acknowledge reception. The \gls{ACK}s are cumulative because an ACK with value N+1 implies the successful reception of all bytes before N+1. This provides robustness against ACK loss. Additionally, ACK are usually delayed by a fraction of second before being sent. This allows to send less pure ACK segment because either new data has been received and the sent ACK value is higher or a data segment is sent and the ACK piggy back on it.

When \TCP sends a group of segments, it normally sets a single retransmission timer waiting for the other party to acknowledge reception. This timer is not segment specific. Rather the timeout is updated as ACKs arrive. If an ACK isn't received in time, the segment is retransmitted.

The checksum and is used to check if the data received has been corrupted. If the packet's checksum is invalid, the packet is discarded by TCP without sending an acknowledgement. Optionally it might sent an acknowledgement for a previously received segment.

The sequence number is used to reorder the incoming segments when they arrive out of order and to discards duplicate segments. 

\subsection{Sliding Window}
A window of packets is the collection of packets injected by the sender in the bytestream that have not yet been acknowledged. The window size is the number of packet in the window. Suppose that the last packet acknowledged is N. Only the packet in the window can be sent. For a window size of W, this means that only packets N+1 to N+W can be sent. It should noted that the window size can be 0 which prevents any packet to be sent. When an ACK with value N+2 is received, the window slides by 1. Now the packet N+2 to N+1+W can be sent. The window can also slide by more than one. For example if the received ACK had a value of N+3, the new window would be N+3 to N+2+W. The acknowledged packet can be discarded by the sender as they will not need to be retransmitted. 
\end{comment}
\subsection{\TCP Options}
\subsubsection{\acrfull{MSS}}
The \acrfull{MSS} is a TCP option which defines the largest segment that the host is willing to receive from the \TCP connection. Because the \gls{MSS} only counts the data bytes, it means that no segment with more than MSS data bytes should be sent to the host. The \gls{MSS} of each host is usually announced through the inclusion of the \gls{MSS} option in both SYN segments of the \gls{3WH}. This option defines a non-negotiated limit. If the option isn't used, the default \gls{MSS} value is 536 bytes.
\begin{comment}
\subsubsection{\acrfull{SACK}}
Because \TCP use cumulative ACKs, the sender isn't able to acknowledge data correctly received segments that are not contiguous in terms of sequence numbers. Those segments can't be consumed by the application to preserve the bytestream abstraction. If the sender was able to know which non contiguous segments were received, it could select which segments it doesn't needs to retransmit. The \gls{SACK} option provides this capability. The support for \gls{SACK} is advertised by the inclusion of the SACK-Permitted option in a SYN or SYN+ACK segment. Afterwards, when out-of-sequence data is received a SACK option describing the out-of-sequence data can be transmitted to help the sender perform retransmissions more effectively. The information contained in the option are two bytes for the kind and length of the SACK option and pairs of sequence numbers representing the data blocks received successfully out-of-order. The maximum number of pairs sent in a single segment is three if the timestamp option is also used. The timestamp option is used to calculate \glspl{RTT} and will not be discussed here.
\end{comment}

\subsubsection{\acrfull{TFO}}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{image/tfo_connection.jpg}
\caption{Three way handshakes with TFO. Cookie exchanges in the first connection. Data transmission in the three-way handshake in the second connection. Image from \emph{TCP Fast Open}\cite{tfoArticle}.}\label{fig:TFO_3WH}
\end{figure}

\gls{TFO} is an experimental \gls{TCP} option that allows hosts to speed up successive connections to the same server by sending data directly inside the first SYN segment, without having to wait for the whole three-way handshake to be completed, thus saving one round-trip time of delay. The change to the \gls{3WH} is shown in FIG.\ref{fig:TFO_3WH}\\
To achieve this, \gls{TFO} uses a cryptographic cookie, requested by the client during his first connection to the server. This first connection is the same as a regular \gls{TCP} connection, with the exception that the \gls{TFO} option is included in the SYN packet from the client, with an empty cookie field. Since this option is still experimental, both client and server have to support it, otherwise it is ignored and the connection falls back to a regular \gls{TCP} connection.\\
If the server support \gls{TFO}, it will generate a cookie specific to the client who sent the SYN segment. This cookie is a \gls{MAC} performed on the client's IP address, as well as possibly other fields depending on the server's implementation of \gls{TFO}. This cookie is sent with the SYN+ACK segment from the server and cached by the client, who completes the handshake normally.\\
On subsequent connection, the client will provide its cookie in its SYN segment, as well as the beginning of its data. If the server fails to verify the provided cookie, it will ignore the data and only acknowledge the SYN, and the connection again falls back to a regular three-way handshake. If the cookie is valid, however, the server sends its SYN+ACK as normal but can then immediately start to send its data, without having to wait for the final ACK from the client finishing the handshake. If that is the case, this final ACK will then acknowledge the server's SYN as well as any data already received from the server to the client. Afterwards, the connection continues to operate like a regular \gls{TCP} connection.\\

Once a client has been provided a cookie by a server, it can use it for any subsequent connections to this same server until this cookie is expired and it has to request another one. Since clients usually has a fixed \gls{DNS} server, this could save a round-trip time almost every time a \gls{DNS} request is made.

\subsection{\acrfull{PMTUD}}
\gls{PMTUD} is used by \TCP to avoid IP fragmentation and operates as follows:
Once the connection is established, \TCP use the \gls{MTU} of the outgoing interface and the \gls{MSS} as a basis to select its \gls{SMSS}. Once the \gls{SMSS} is chosen, all sent IPv4 datagrams have the DF bit set. For IPv6 datagrams, it is considered implicitly set. For simplicity the ICMPv4 Destination Unreachable (Fragmentation Required) message and ICMPv6 packet too, big message will be referred as Packet Too Big (PTB). If a PTB message is received, \TCP will reduce the segment size and retransmit with this segment size. If the suggested next-hop MTU is present in the PTB, the suggested next-hop MTU minus the IP and TCP header size can be used as the new segment size. Otherwise a variety of value can be tried. \gls{PMTUD} is hindered when PTB messages can't be received. This can be caused by Firewalls, NATs and so on.

%\subsubsection{\TCP Fast Retransmit}
\subsection{\TCP Fast Retransmit}
\gls{TCP} Early Retransmit is defined in RFC 5827\cite{rfc5827}. It is a mechanism to allow \gls{TCP} to retransmit segments based on feedback from the receiver instead of requiring the retransmission timer to expire. Packet loss can often be more quickly and efficiently repaired this way. A typical \TCP implements both Fast Retransmit and Retransmission timers.
\TCP is required to send an immediate ACK when an out-of-order segment is received and the loss of a segment implies out-of-order arrivals. When it happens, the sender should try to fill the gaps as soon as possible.

The immediate ACK is sent without delay when out-of-order segments arrives to let the sender know that a segment was received out-of-order and which sequence number is expected. With \gls{SACK}, the immediate ACK can also contain information about blocks received out-of-order.

Because the missing segment could be either lost or delayed, \TCP waits for a threshold of duplicate ACKs to be received before doing a fast retransmit. Usually, this threshold is 3.

When the threshold is reached, one or more packets that appear to be missing are retransmitted even if the retransmission timer hasn't expired. Without SACK, no more than one packet is usually retransmitted until reception of an acceptable ACK. With SACK, more than one hole can be filled per RTT as there is additional information available.

\subsection{\TCP Tail Loss Probe}
\gls{TCP} \gls{TLP} is defined most recently in draft \texttt{draft-ietf-tcpm-rack-02} from the IETF TCP Maintenance Working Group\cite{RACKTLP}.  Similarly to TCP Fast Retransmit, \gls{TLP} aims to provide faster recovery time in case of losses based on feedback from the receiver. After a data transmission, a specific timer is set between one and two RTTs. When the timer expires, either new, unsent or previously sent, data is sent in a segment (the TLP probe) to the receiver. The goal is to elicit an ACK or \gls{SACK}. If an ACK or \gls{SACK} is received which indicates one or more lost segment, the recovery algorithm is triggered and the missing segments are retransmitted.

\subsection{Nagle} \label{subsec:nagle}
Another possible improvement is related to the data contained in each \gls{TCP} segment. \gls{TCP} receives data from applications to send on the network, but there are several ways in which this data can be handled. By default, \gls{TCP} uses \textit{Nagle's algorithm}, which is defined as follows: if the amount of data provided by the application, waiting to be transmitted, is greater than the \gls{MSS}, the algorithm send it immediately. Otherwise, it continues to accumulate data from the application until the acknowledgement from the previously sent data is received, at which point it sends the following accumulated data.

The aim of this algorithm is to reduce the number of packets containing a low amount of data travelling through the network in order to reduce the risk of congestion. However, having to wait for an acknowledgement when the amount of data cannot fill a complete segment can increase latency at the application level. Nagle's algorithm is a compromise between those two risks.

\subsection{Cork}\label{subsec:cork}
Compared to Nagle's algorithm, TCP\_CORK will accumulate data even more aggressively before sending it on the wire. Similarly to the behaviour dictated by Nagle's algorithm, Cork will not send data until its size reaches the \gls{MSS}. It differs from Nagle, however, in the fact, it won't send the partial segment's accumulated data even if the previous data has already been acknowledged. Instead, it will only send the buffered data after 200 ms has passed since the previous transmission.

\subsection{No delay}\label{subsec:nodelay}
The third possible configuration, TCP\_NODELAY, can be considered the opposite of Cork. Whereas Cork aims to minimize the amount of partial segments at the cost of latency, NODELAY's goal is the optimize latency, without regard to the amount of small segments transmitted and their overhead. Its behaviour is quite simple: NODELAY disables Nagle's algorithm, and data is sent on the wire as soon as it is provided by the application.

There are obviously risks to this approach. Consider for example an application, such as Telnet, that sends data on each keystroke for a user. Since each keystroke represents one byte of data, and each \gls{TCP} packet header is 40 bytes (20 bytes of \gls{TCP} header and 20 bytes of IPv4 header), a total of 41 bytes would be sent on the wire after each keystroke from the user, but only 1 of those 41 bytes is actual data! This represents a gigantic overhead, and requires an enormous amount of packets to be transmitted through the network, potentially leading to congestion collapse.

\section{DNS over TCP}
The transport of \DNS over TCP is defined in RFC 7766. This RFC specifies that all general-purpose \DNS implementations should now support both \UDP and \TCP and that \TCP ought to be considered a valid alternative to \UDP\cite{rfc7766}.

The generally perceived disadvantage of \DNS over \TCP is the added connection setup latency which is generally equal to one \gls{RTT}. To amortize connection setup, both the client and server should support connection reuse by sending multiple queries and responses over a single persistent \TCP connection. Additionally clients must not reuse the \DNS message ID of an in-flight query on that connection to avoid Message ID collision. This is particularly important if the server could be preforming out-of-order processing.

Clients should also pipeline their request to improve performance. This means that clients should not wait for an outstanding reply to send the next query. It is likely that servers need to process pipelined queries concurrently and also send out-of-order responses over TCP to provide a level of performance similar to transport over UDP. It is recommended by the RFC that servers support processing queries concurrently and send answers out-of-order. Additionally, clients should be able to process answers received out-of-order. \TFO can also be used to transport data in the SYN packet during the TCP \gls{3WH} which improves performance when reconnecting to a server if the cookie is still valid.

To mitigate the risk of unintentional server overload, DNS clients must take care to minimize the number of concurrent \TCP connections made to any individual server. The recommendation is that there should no more than one connection for regular queries, one for zone transfers and one for each protocol used on top of \TCP such as \TLS. However more connections might be needed for zone transfers for operational reasons.

Servers can also limit the number of concurrent \TCP connections based on IP addresses or subnets. However the restrictions should be looser as because the server does not know what is behind the IP.

Additionally to mitigate the risk of unintentional server overload, clients should take care to minimize idle time of established connections. DNS clients should close the \TCP connection of an idle session unless an idle timeout is established using another signalling mechanism such as \TCP keepalive. The recommendation for the default server idle timeout is that it should in the order of seconds. It should be at least a few seconds and can vary dynamically according to resource availability. Servers under heavy loads or attacked may use a zero-second timeout. The timer should be reset when a full \DNS message is received to avoid a slow-read attack.

While the connection should typically be closed by clients, it can also be closed by servers if the idle timeout is exceeded. Both sides can also close the connection under unusual conditions such as defending against an attack or system failure. If the \DNS client connection is closed before it received all outstanding responses, it should retry the unanswered queries. No retry algorithm is specified in the RFC. If the \DNS server finds that the client as closed the connection before all pending responses have been sent, it must not attempt to send those responses but can cache them.  

In section \ref{sec:DNSoUDP}, we talked about the size limit for \DNS messages over \UDP which is 512 bytes or theoretically 65536 bytes if \gls{EDNS} is used. However \gls{EDNS} recommends a value between 1280 and 1410 bytes as reasonable\cite{rfc6891}.
With \TCP,  the TCP Message Length field precedes every \DNS message sends on the connection. This field is two bytes and as such the longest message that can be sent theoretically. It is also 65536 bytes because the message length is encoded in two bytes. While a single \DNS message of that length is unlikely at this time, sending \DNS message of bigger size is easier with \TCP. There is multiple reason for that. The byte stream abstraction of \TCP allow a single \DNS message to be sent on multiple packets transparently. \TCP implements the \gls{PMTUD} algorithm that reduces the likelihood of IP fragmentation. In case packets or fragments are lost, it has its own retransmission algorithm. Additionally if packets are lost, TCP will reduce the size of the segment sent, which help with IP fragmentation problems. 


\section{TLS Handshake and Session Resumption}
The \TLS protocol, defined in RFC2246\cite{rfc2246} RFC 4346\cite{rfc4346}, RFC5246\cite{rfc5246} and RFC8446\cite{rfc8446} respectively for version 1.0, 1.1, 1.2 and 13, is used to provide privacy and data integrity between two communicating applications. The \TLS protocol is used on top of another reliable transport protocol. The TLS protocol is composed of two layers: The TLS handshake Protocol and TLS Record Protocol. The TLS Handshake Protocol is used to allow the client and server to authenticate each other and to negotiate the encryption algorithm and cryptographic keys before transmitting data. The TLS Record Protocol is used for encapsulation of higher-level protocols and provides privacy through encryption and reliability through the usage of Keyed MAC.

The Handshake Protocol is used to negotiate a session which consists of a session ID, the peer certificate (may be null), the data compression method, the master secret and whether the connection can be resumed.  The Handshake Protocol is executed in multiple steps:

First, The client sends a ClientHello message which contains the protocol version, session ID, Cipher suite, compression method and a random value.

The server then responds with ServerHello message containing the same type of parameters. The server can optionally send three types of message after this one: a certificate message if it is to be authenticated, a ServerKeyExchange message and if it is authenticated and the cipher suite supports it, a CertificateRequest message. The server then sends a ServerHelloDone message to indicate that the hello phase is finished.

If a CertificateRequest has been received, the client must then send the certificate message. The ClientKeyExchange is the send which depends on the public key algorithm selected in the hello messages. If the certificate of the CertificateRequest has signing ability, a digitally signed CertificateVerify message is sent to verify possession of the private key. The client then sends a Finished message. The server sends a Finished message. At this point the Handshake is concluded. 

Additionally RFC 5077\cite{rfc5077} specifies a mechanism for fast TLS session resumption that enables \TLS servers to resume sessions without keeping per-client state. To do so, the server sends during the initial connection a ticket with the session state which can later be used by the client to resume the session.  When a ticket is present at the client and accepted by the server, the handshake is abbreviated in the following way:

The client send its ClientHello message which contains the SessionTicket extension (with the ticket). The server responds with a ServerHello message which contains an empty SessionTicket extension. It then renew the ticket by sending a NewSessionTicket message followed by the Finished message. The client then sends the Finished message and the session is resumed.

\section{DNS over TLS}
\DNS over \TLS, defined in RFC 7858\cite{rfc7858}, provides privacy and tampering protection to \DNS traffic through the use of TLS. A \DNS over \TLS connection is made on port 853. Once the client has established a \TCP connection on port 853, it proceeds with the TLS Handshake. The connection is encrypted after the handshake concludes. An earlier mechanism called "STARTTLS for DNS" allowed upgrading a DNS over TCP connection to TLS (on port 53) but was abandoned.

To minimize latency, the RFC specifies that multiple query should be pipelined similarly to what is recommended for TCP. Clients and servers should reuse existing connections for subsequent queries as long as they have resources. Thus idle connections may need to stay open for some time. For the management of idle connections, RFC 7858 refers to the recommendation of RFC 7766 (DNS over TCP). Additionally software implementing DNS over TLS should support idle, persistent connections and be able to manage multiple potentially long-lived TCP connections. Similarly to DNS over TCP, the connection can be closed by both parties.

TLS-FALSESTART defined in RFC 7918 can also be used to send encrypted data before the end of the TLS Handshake for TLS up to 1.2.
When re-establishing a DNS over TCP, \TFO can be used to improve performance as long as the initiation or resumption of a TLS handshake is immediate (No clear DNS Data is sent). Additionally \TLS session resumption discussed here above should be used when re-establishing connections.

There is two different usage profile for DNS over TLS. The first one is the Opportunistic Privacy Profile, where the client does not require privacy but desire it. The client discovers the TLS-enabled server from an untrusted source which it might or might not validate. This profile is susceptible to on-path attacks. The second profile is Out-Of-Band Key-Pinned Privacy where a trust relationship is present between the client and server. The server will only connect to the server if it can authenticate it.

\section{DNS over HTTPS}
DNS over HTTPS is defined in RFC8484\cite{rfc8484}. HTTPS is a layer above TLS which HTTP to benefit from the encryption provided by TLS but also suffer it's detriment. To query a Doh sever, an HTTP request is made to the server with a specific URI. The example given in the RFC is "https://dnsserver.example.net/dns-query". The URI is not imposed and can potentially vary from server. To perform the query, both POST and GET query can be used. With POST, the query is sent in wire format with content-type 'application/dns-message' whereas with GET the ?dns variable is encoded with base64dns. All client must be able to accept answers in 'application/dns-message' format. In addition to this format, a popular is 'application/dns-json'.
Since we're using HTTP, the server gains the possibility to cache the request responses and Website's front end developers gain another tool for their toolbelt. But this also means that we now have to deal with HTTP error. For example, during our measure we received a HTTP 502 Bad Gateway error which at the time , the software was not ready to deal with.
The minimum version of HTTP recommended by the RFC is HTTP/2 thus all our measure will respect this recommendation and use multiplexed HTTP/2. The reason HTTP/2 is preferred is because it implements pipelining, out-of-order delivery. In contrast, the tentative to implement pipelining in HTTP/1.1 was so bad that both mozilla and chromium gave up on it\cite{ChrHttpPipe}\cite{MozHttpPipe}.

\section{Motivation}
We have now presented DNS over TCP, TLS and HTTPS. The question now becomes if and for which reasons should the change be made. We will first explore in more detail the motivations to use each of them.

\subsection{TCP}
At first glance the use of TCP instead of UDP to provide the DNS service may seem like a net negative as the additional RTT to perform the three way handshake will increase the time needed to get your response. However as this is often the case, the situation isn't so simple. We will first talk about IP Fragmentation as this point comes back frequently before moving to performance and security considerations.

The IP fragmentation in UDP occurs when the packet is bigger than the path MTU. The packet is then fragmented at the IP level and each fragment is transmitted to the destination. The IPV6 behaviour differs slightly as an ICMPv6 Packet Too Big message will be sent back to the host who will then be responsible to fragment the packet to the correct size. These IP fragments require resend-all loss recovery which is blocked by about 8\% of middleboxes\cite{connectionOrientedDNS}. There is also about 8\%  of  the  sessions which  cannot  send  fragmented  UDP and 9\%  who cannot receive fragmented UDP\cite{netalyzr}. 
Furthermore, they can also be exploited by an attacker in DNS Cache Poisoning Attacks\cite{connectionOrientedDNS}. For these reasons, the limitation of the size of DNS UDP packet is seen as an operational and has influenced decisions pertaining to its evolution and security. An example, of such a security decision would be the size of the keys to use in \DNSSEC \cite{connectionOrientedDNS}.
IP fragmentation problems have been increasing due to the evolution of \DNS and the deployment of \DNSSEC which contributed to the increase in the reply size of DNS messages. Currently over 75\% of the Alexa top 1000 websites have replies of at least 738 bytes\cite{connectionOrientedDNS}. 

On the other hand, TCP face this much more easily. TCP Stacks generally implement Path MTU Discovery which reduce the occurrence of fragmentation by sending smaller packets. Sending smaller doesn't impact TCP like UDP because TCP provides a bytestream abstraction which means that how much information fit in a packet will not impact the upper layer. In addition, when fragmentation does occur, TCP has more robust methods for retransmitting.
 
Regarding performance, the main culprit is the Three Way Handshake which will delay the query and subsequently the response. However by using the connection for multiple queries and pipelining them (multiple concurrent queries), it will be amortized. This is likely to happen because the \DNS Traffic is bursty\cite{burstyDNS}. In addition to amortization, \TFO can also be used to reduce performance losses due to the Three Way Handshake as it allows to send data in the Three Way Handshake when reconnecting to a server. \TFO{}'s cookie system also allow to move the state to the client which reduces the impact of TCP on the server. The performance of TCP compared to UDP will be studied in more detail in \ref{ch:results}.
However currently the TCP port 53 can suffer problem when interacting with middleboxes\cite{rfc7766}.

The usage of \TCP improves the security of the internet ecosystem because it generally prevents address spoofing which in turn prevent reflection and amplification attacks that plague UDP. The reason is that source address spoofing with UDP is easy and unfortunately that's unlikely to change soon\cite{connectionOrientedDNS, ipSpoofing}.
However it does not provide secrecy like TLS or HTTPS do

\subsection{DNS over TLS}
% Privacy
The main goal of DNS over TLS is to provide privacy to the transaction which is something that can't be achieved with just UDP or TCP. To do so, a protocol such as TLS is needed. It should be noted that performance aspects of TCP also apply to TLS as TCP is the underlying protocol. However, specific mechanisms are also needed for example to speed up the TLS Handshake done after the TCP Three Way Handshake. 
The privacy induced by the use of TLS allows the user to protect himself against Pervasive Attacks where attacks means a "behavior that subverts the intent of communicating parties without the agreement of those parties.  An attack may change the content of the communication, record the content or external characteristics of the communication, or through correlation with other communication events, reveal information the parties did not intend to be revealed.  It may also have other effects that similarly subvert the intent of a communicator."\cite{rfc7258}

\subsection{DNS over HTTPS}
So reading the previous it seems that DNS over TLS solves all our problems but not quite! However DNS over TLS has two issues that makes it less popular than HTTPS. First it has a dedicated port which makes it easier to block. And two it does not support multiplexing. The only DNS over TLS/TCP resolvers that supports multiplexing are proxies that convert them back to DNS over UDP with UDP doing the multiplexing.

Additionally, DNS over HTTPS offer freedom to developer to use dns as they see fit with no regard for the computer configuration which can be considered as either a good or a bad thing depending on the situation and the goal to attain.





