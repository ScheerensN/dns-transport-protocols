\label{sec:security}
%We will discuss the different methods that can be used to secure \DNS{} in this chapter.
Security is a major challenge nowadays. This chapter presents the addition of security feature over \DNS and discusses about different methods that can be used to secure \DNS. We will then have a deeper look at what is DNSSEC and how it works as it will be used during our tests.

As with other systems, \DNS can be made more secure through its configuration, the network architecture and several other standard methods. We won't discuss these methods as they will change depending on the level of security required and the expected threats\cite{dnsAndBind10}.

\section{Transaction Level Authentication}
The first level of security is to authenticate the \DNS transaction. This require new \RRs which we will present hereafter. We will present three of them in this section.

\begin{flushright}
\underline{\small{TYPE}} \hspace*{2cm} \small{\underline{RFC}}
\end{flushright}

\subsection{TSIG \hfill{\texttt{250} \hspace*{1.75cm} 2845}}
The \acrfull{TSIG} \RR use an \gls{HMAC} to provide point-to-point authentication and integrity check for \DNS transactions\cite{rfc2845}. Because the authentication mechanism relies on shared public key, it is not suitable to use it with all clients of the \DNS service but it is better suited to secure zone transfers and \gls{DDNS} transfers. Furthermore, the RFC doesn't specify a mechanism to share the key.
\begin{comment}
\begin{figure}[!ht]
\centering
\includegraphics[width=0.5\textwidth]{image/HMAC.png}
\caption{HMAC authentication.\cite{dnsAndBind10}}\label{fig:HMAC}
\end{figure}

We will first explain how an HMAC is used to authenticate a message. As shown in FIG.\ref{fig:HMAC}, the server wants to generate the secure hash of some plaintext. To do so, it will use a one-way keyed hash function with a symmetric key to generate a message digest. This mail digest is a fixed length message which will be the same each time the same message, key and keyed hashing function are used.
The server can then send both the message and the message digest to the second host. When receiving the message and the message digest, the second host will use the pre-shared key and the keyed hash algorithm to generate a message digest of the received message. If the two message digest are the same, the transaction is authentic because the key is only known by the server and the second host.


The \TSIG \RR name is used to identify which key should be used. The RDATA field contains multiple values: The algorithm name, the time signed (The time it was signed in seconds from 1-JAN-70 UTC), a fudge (seconds of error permitted in the previous field), the MAC size (in octet), the MAC, the original message id, an error code, other len. (length of other data), other data (emtpy unless the error is badtime).

The \TSIG mechanism works in the following way. The client generate the digest of the message and a subset of the \TSIG variables. The client then append the \TSIG \RR to the additional data section and transmits the query to the \DNS server. The message digest is stored while waiting the response.
When the server respond to the a signed query, it signs the response using the same key and algorithm. The request MAC, \DNS message and \TSIG variables are hashed.

The \TSIG mechanism works differently when a TCP connection is used because multiple \DNS envelopes can be used. Instead of a \TSIG \RR for each \DNS message, the \TSIG \RR is included on the first, the last envelopes and at least each 100th envelope. This is done because it is expensive to place one on each envelope. Additionally while the first digest is the same as usual, subsequent digest are composed of the prior digest (running), \DNS messages (unsigned messages since the last \TSIG) and \TSIG timers (current message). This allows the client to rapidly detect when the session has been altered. If the connection has been altered, the connection can be closed and retried. If the \TSIG verification fails or \TSIG are not sent often enough, the connection should be closed and treated as an interrupted transfer.

\TSIG can be used to secure zone transfers.

RFC 2845, 3645 and 4635 specify several algorithms that can be used: HMAC-MD5, GSS-TSIG, HMAC-SHA1, HMAC-SHA224, HMAC-SHA256, HMAC-SHA384 and HMAC-SHA512. Only HMAC-MD5, HMAC-SHA1 and HMAC-SHA256 have to be implemented.\cite{rfc2845,rfc3645,rfc4635}
\end{comment}

\subsection{TKEY \hfill{\texttt{249} \hspace*{1.75cm} 2930}}
The \acrfull{TKEY} \RR can be used to establish a share secret between a resolver and a \DNS server for mechanisms such a \TSIG \cite{rfc2930}. The minimum requirements are to be able to derive a secret with the Diffie-Hellman scheme and to delete a key.
\begin{comment}
The name of the \TKEY \RR is used to name the key and the RDATA field contains the following: the algorithm,  the inception time, the expiration time, the mode (scheme used or the purpose of the message), the error, the key size, the key data, other size and other data. The mode field can take the following values: server assignment (0), Diffie-Hellman (1), GSS-API negotiation (2), resolver assignment (4) and key deletion (5). 1 and 5 have to be implemented. All values not cited are reserved. The other size and other data fields are currently unused. \TKEY \RR should only be exchanged with transaction authentication and queries should be authenticated except for the GSS-API mode. The authentication should be done through \TSIG with an existing shared secret or with a public key and SIG(0).
\hfill \break

The exchange can be made by the resolver through a DNS queries which are syntactically DNS queries for type TKEY. Such queries should be accompanied by a \TKEY RR in the additional information section to indicate the mode in use and other information if required. As the exchange depends on the mode used, I will only detail the Diffie-Hellman scheme and the key deletion as those are the only mode required to implement.

Diffie-Helman (DH) is an asymmetric cryptography method used to derive a shared secret which can be used to generate a symmetric key. The advantage of Diffie-Hellman is that it doesn't require secrecy of the exchange however it is vulnerable to man-in-the-middle attacks\cite{secu1}. This is why TKEY queries and responses for Diffie-Hellman must be authenticated.

Diffie-Hellman use a prime \texttt{p} which should about 512 bits or more, \texttt{g} a primitive root of \texttt{p} which are public. The initiator will choose a secret number a and send $ g^{a}\: mod\: p $ to second party. The second party choose a secret number b and send $ g^{b}\: mod\: p $ to the initiator. The shared secret is $ g^{ab}\: mod\: p = ( g^{b}\: mod\: p)^{a} = ( g^{a}\: mod\: p )^{b} $. The security is derived from the fact that deriving a from $ g^{a}\: mod\: p $ is hard. \cite{secu1}

The Diffie-Hellman scheme used with \TKEY is slightly different. The resolver sends a query of type \TKEY which include a \TKEY \RR and a KEY \RR in the additional information section. RFC 2539 defines how to store the Diffie-Hellman p, g and $ g^{i}\: mod\: p $ values in a KEY \RR which is itself defined in RFC 2535.\cite{rfc2539,rfc2535}. The \TKEY \RR algorithm field is set to the authentication 	algorithm the resolver plan to use and the key data is used as a random nonce to avoid deriving the same keying material from the same set of DH keys.
The server response contains a \TKEY in its answer with the Diffie-Hellman mode and accompanied by the resolver KEY \RR and the server KEY \RR in the additional information section.
Both parties can then calculate the keying material in the following way: 
\begin{equation}
\begin{split}
keying\: material = XOR ( DH\: shared\: secret, MD5 ( query\: nonce | DH\: shared\: secret) | \\
MD5 (server\: nonce\: | DH\: shared\: secret)
\end{split}
\end{equation}

For the key deletion mode, a resolver send an authenticated \TKEY \RR with the key's name. On reception the server will discard the key.
\end{comment}
\subsection{SIG(0) \hfill{\texttt{24} \hspace*{0,25cm} 2535 \& 2931}}
The SIG(0) \RR contains a signature and additional information such as the signer's name and expiration information. It aims to provide transaction authentication similarly to \TSIG but use public key cryptography instead of symmetric key cryptography. The public key is stored in a KEY \RR in the \DNS. The intended scope of SIG(0) is to be used when it is necessary to authenticate that the requester has some identity or privilege\cite{rfc2931, rfc2535}.
\begin{comment}
A \DNS query may be optionally signed by including one SIG(0) \RR at the end of the query additional information section. It signs the following data: \\
\begin{equation}
\begin{split}
 data = SIG\: RR's\: RDATA\: except\: the\: signature\: subfield| \\
 DNS\: query\: including\: DNS\: headers\: before\: adding\: the\: SIG\: RR 
\end{split}
\end{equation}
Except where needed to authenticate an update, \TKEY or similar privileged request, servers are not required to check a request SIG(0).

The SIG \RR can also be used to secure a \DNS response and the query that produced it. The only difference is the data signed:
\begin{equation}
\begin{split}
 data = SIG\: RR's\: RDATA\: except\: the\: signature\: subfield|\: the\: entire\: DNS\: query\: | \\
 the\: DNS\: response\: including\: the\: DNS\: headers\: before\: including\: the\: SIG\: RR 
\end{split}
\end{equation}
The verification of a response SIG(0) guarantees that neither the query nor the answer have been tampered with in transit. Furthermore request and responses can either have a \TSIG or a SIG(0) but not both.
\end{comment}
\section{Data origin authentication and data integrity}
\label{sub:DNSSEC}
%% Page 13, DNSSEC used by ICANN to update the root servers
The data origin authentication and data integrity feature in \DNS is provided by \acrfull{DNSSEC}. It should be noted that the OPT \RR discussed above is a prerequisite for the use of \DNSSEC because the flag that indicates the desire to use \DNSSEC is contained in that \RR

\DNSSEC defines a process enabling a correctly configured name server to verify the authenticity and integrity of query results from a signed zone. Public key or symmetric cryptography and a special set of \RRs, \gls{RRSIG}, \gls{DS}, DNSKEY, \gls{NSEC}, and \gls{NSEC3} \RRs, are used by \DNSSEC to enable the security aware resolver, validating resolver, to:
\begin{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\item Authenticate that the received data originates from the requested zone.
\item Verify that the data received was sent from the queried named server.
\item Verify that if a negative response, NXDOMAIN, is received, the target \RR does not exist. This is a proof of nonexistence or denial of existence.
\end{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\DNSSEC{}'s mechanisms and \RRs are defined in RFCs 4033, 4034 and 4035 and are updated by RFCs 4470, 4509, 5011, 5155 and 6944\cite{rfc4033,rfc4034,rfc4035,rfc4470,rfc4509,rfc5011,rfc5155,rfc6944}.
\hfill \break

Because it isn't reasonable to consider that every name server and resolver support \DNSSEC, the following must stay possible:
\begin{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\item A security-aware resolver must provide query results for both secure and insecure domain. Including passing through a secure domain to an insecure subdomain.
\item A security-oblivious resolver must continue to obtain transparent results from both secure and insecure domains.
\item If the security-aware resolver send a query with an OPT \RR which has the DO bit set and the name server has \DNSSEC enabled, the server's response includes additional security information such as \RRSIG \RRs that authenticates the requested \RRs.
\end{itemize}

\subsection{Islands of Security}
Public key cryptography relies on a pair of public and private key. With DNSSEC, the zone example.com is signed with the zone's private key. However the public key of the zone is needed by the security-aware resolver to perform the security verification. The problem is how to securely transmit the public key in such a way that it could only be from example.com. There are two methods: the first one is to put it in a DNSKEY \RR in the zone file but this means that the public key is needed to verify the \RR that contains it. The second one is to us to use an out-of-band process such as secure email, telephone,... This can be done with \DNSSEC by configuring the validating resolver to trust the public key which is called a trusted anchor. 

This setup is an Island of security which is defined as a signed delegated zone that does not have an authentication chain from its delegating parent.  Meaning that there is no \gls{DS} \RR containing a hash of a DNSKEY RR for the island in its delegating parent zone.

\subsection{Chains of trust}
Any Islands of security can be joined to another signed domain through its delegation point, NS \RRs pointing to a child's domain, and can be authenticated through \acrfull{DS} \RRs. Three points to note:
\begin{itemize}
\setlength\itemsep{0em}
\setlength\parskip{0em}
\item The child zone must be secure before secure delegation can occur. This is a prerequisite to create a chain of trust.
\item The trusted anchor of the parent domain covers the secure zones that are delegated from it. The delegation can be securely tracked from the parent domain to the child using a chain of trust provided by the \gls{DS} \RR. The number of levels that can be covered through chains of trust is not limited.
\item Delegation chains can also be built upwards as well as downwards. If the parent domain of the Island of Trust we considered was secure, the domain can join the chain. If the resolver now as a trust anchor to the parent domain, it could cover all the previously cited domains.
\end{itemize}
\hfill \break
The \DNS root is signed with two published trust anchors since July 15, 2010. The Root Trust anchors can be found at \verb|https://data.iana.org/root-anchors/|\cite{RootDNSSEC,RootTrustAnchors}. This means that the root zone is an Island of Security. Furthermore on July 17, 2017, 1397 TLDs have trust anchors published as \gls{DS} \RR in the root zone and 1406 TLDs are signed from the 1547 TLDs in the root zone. On August 18, 2019, these numbers have evolved to 1388, 1400 respectively which show a small decrease\cite{TldDNSSEC}. Furthermore several registrars offer end user DNSSEC management including entry of \gls{DS} records into TLDs\cite{DNSSECRegistrars}. For example, domains such as \emph{be.} , \emph{brussels.} and \emph{beer.} have their \gls{DS} \RR in the root zone and  have a registrar that supports the inclusion of end user \gls{DS} \RR into those TLD's zones\cite{TldDNSSEC,DNSSECRegistrars}.

Some additional information about DNSSEC can be found in Appendix \ref{ax:dnssec}

\section{Confidentiality}
We saw that authentication and data integrity is done through several \RR. However, the confidentiality is also a one of main challenges of the security. With \DNS, this confidentiality can be obtained through the encryption of the \TLS traffic. The usage and the description of \DNS over \TLS and DNS over HTTPS is discussed in chapter\ref{sec:networkAspects}.
