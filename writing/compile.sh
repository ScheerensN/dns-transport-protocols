# First build
echo "#### PDFLATEX RUN 1 ########################################################################################"
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build Thesis.tex
echo "Run 1 finished"

# build bibliography
echo "#### BIBTEX ################################################################################################"
cp biblio.bib build
cd build
bibtex Thesis.aux
cp biblio.bib ../
cd ..

# build glossary
echo "#### GLOSSARY ##############################################################################################"
makeglossaries -d build Thesis

# Two build to update table of contents, references, ...
echo "#### PDFLATEX RUN 2 ########################################################################################"
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build Thesis.tex
echo "Run 2 finished"
echo "#### PDFLATEX RUN 3 ########################################################################################"
pdflatex -synctex=1 -interaction=nonstopmode -output-directory=build Thesis.tex | grep -v -e "hyperref" -e "(/usr/share/texlive" -e "xcolor" -e "underfull" -e "^$"  #| grep -e "Package glossaries" -e "&" -e "undefined" -e "LaTeX Warning" -e "LaTeX Error" -e "Undefined control sequence"
echo "Run 3 finished"

cp build/Thesis.pdf Thesis.pdf
