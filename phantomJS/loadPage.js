"use strict";

var system = require('system');
var os = system.os;
var fs = require('fs');

var OSVersion = os.name + " " + os.version + " " + os.architecture;
var platformVersion = system.platform + " " + phantom.version.major + "." + phantom.version.minor + "." + phantom.version.patch;

console.log( "\n" + platformVersion + " on " + OSVersion );

var EXIT_SUCCESS = 0;
var EXIT_CONNECTION_REFUSED = 1;
var EXIT_CONNECTION_CLOSED = 2;
var EXIT_HOST_NOT_FOUND = 3;
var EXIT_SOCKET_TIMEOUT = 4;

var EXIT_URL_LOAD_FAILED = 8;
var EXIT_BAD_ARGUMENTS = 9;

var http = "http://";
var https = "https://";

var page;

// Setting up argument variables with default values and if specified change their value.
var address = "http://";
var domainJson = "./domains.json";
var prefetchJson = "./prefetch.json";
var debug = false;
var onLoadMax = 30000;

var maxNumberOfArgs = 6;
if (system.args.length > 1 && system.args.length <= maxNumberOfArgs){
    // URL parameter
    if (system.args[1].indexOf("://") > -1){
        address = system.args[1];
    } else {
        address += system.args[1];
    }
    // path for the domains file
    if (system.args.length > 2){
        if (system.args[2].indexOf("/") === 0){
            domainJson = system.args[2];
        } else {
            domainJson = "./";
            domainJson += system.args[2];
        }
    }
    // path for the prefetch file
    if (system.args.length > 3){
        if (system.args[3].indexOf("/") === 0){
            prefetchJson = system.args[3];
        } else {
            prefetchJson = "./";
            prefetchJson += system.args[3];
        }
    }
    // debug parameter
    if (system.args.length > 4){
        if (system.args[4].toUpperCase() == "true".toUpperCase()){
            debug = true
        } else if (system.args[4].toUpperCase() == "false".toUpperCase()){
            debug = false
        }
    }
    // maximal value of onload before we stop.
    if (system.args.length > 5){
        if ( !isNaN(system.args[5]) && Number(system.args[5]) > 0){
            onLoadMax = Number(system.args[5]);
        }

    }
} else {
    usage();
    phantom.exit(EXIT_BAD_ARGUMENTS);
}

if (debug){
    console.log(system.platform + " " + system.args[0] + " " + address + " " + domainJson + " " + prefetchJson + " " + debug.toString() + " " + onLoadMax.toString())
}

function usage(){
    console.log("Usage:");
    console.log("   " + system.platform + " " + system.args[0] + " URL [ domains_path [ prefetch_path [ debug(true or false) [ maximal_number_of_onload_events ]]]]\n");
    console.log("Return codes:");
    console.log( "  " + EXIT_SUCCESS + " successfully exited.");
    console.log( "  " + EXIT_BAD_ARGUMENTS + " Incorrect arguments.");
    console.log( "See http://doc.qt.io/qt-4.8/qnetworkreply.html#NetworkError-enum for other values.");
}

function splitProtocolFromURL(URL){
    var index = URL.indexOf("://");
    if (index === -1){
        return URL;
    }
    return URL.slice(index+3, URL.length)
}

function getDomainFromURL(URL){
    var url = URL;
    var index = url.indexOf("://");
    if (index > -1) {
        url = url.slice(index+3, url.length)
    }
    index = url.indexOf("/");
    if (index > -1){
        url = url.slice(0, index);
    }
    return url;
}




function sleep(timeout, onTimeout, arg1, arg2){
    var start = new Date().getTime();
    var interval = setInterval(function(){
        if( new Date().getTime() - start > timeout * 1000 ){
            clearInterval(interval);
            if (arg2 === undefined){
                if (arg1 === undefined){
                    onTimeout()
                } else {
                    onTimeout(arg1)
                }
            } else {
                onTimeout(arg1, arg2);
            }
        }
    }, 250)
}

function ifFinished(callback){
    return function (page, loadEnded) {
        if (page.loadCount != loadEnded && page.loadCount < onLoadMax) {
            // the last load hasn't ended yet.
            // onLoadMax prevent infinite loop for page with infinite onloadEvent such as dropbox.com
            return
        }
        if (page.mainStatus == 'success'){
            succeeded(page);
        } else {
            callback(page);
        }
    }
}

function succeeded(page) {
    console.log("Successfullly loaded " + page.address);
    var fetchALinks = getDomainsFromCrossDomainALinks(page);
    var prefetchHTML = prefetchList(page);
    var prefetchHTTP = page.httpPrefetchdomains;
    var fetch = [fetchALinks, prefetchHTML, prefetchHTTP];

    var file = fs.open(prefetchJson, 'w');
    file.write(JSON.stringify(fetch) + "\n");
    file.close();

    file = fs.open(domainJson, 'w');
    file.write(JSON.stringify(page.domain) + "\n");
    file.close();

    console.log(pageInfo(page, fetch));

    phantom.exit(EXIT_SUCCESS);
}

function failed(page) {
    console.log("Failed to load " + page.address);
    phantom.exit(EXIT_URL_LOAD_FAILED);
}

function exitOnError(page, resourceError){
    if (resourceError.errorCode == EXIT_CONNECTION_REFUSED) {
        console.log("Error: connection refused on " + resourceError.url);
        phantom.exit(EXIT_CONNECTION_REFUSED);

    } else if (resourceError.errorCode == EXIT_CONNECTION_CLOSED) {
        console.log("Error: connection closed on " + resourceError.url);
        phantom.exit(EXIT_CONNECTION_CLOSED);

    } else if (resourceError.errorCode == EXIT_HOST_NOT_FOUND) {
        console.log("Error: host not found.");
        phantom.exit(EXIT_HOST_NOT_FOUND);

    } else if (resourceError.errorCode == EXIT_SOCKET_TIMEOUT) {
        console.log("Error: Socket operation timed out on " + resourceError.url);
        phantom.exit(EXIT_SOCKET_TIMEOUT);
    } else if (resourceError.errorCode != 5) {
        // 5 is for canceled operations and can happen in normal operations.
        console.log('\nError: Unable to load ' + resourceError.url);
        console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
        phantom.exit(resourceError.errorCode);
    }
}

function pageInfo(page, prefetch){
    var domains = page.domain.length,
        aLinkDomain = prefetch[0].length,
        HTMLDnsPrefetch = prefetch[1][0].length,
        HTMLPrefetch = prefetch[1][1].length,
        HMTLPrerender = prefetch[1][2].length,
        HTTPDnsPrefetch = prefetch[2][0].length,
        HTTPPrefetch = prefetch[2][1].length,
        HTTPPrerender = prefetch[2][2].length,
        str;
    str  = "domains:" + domains + "\n";
    str += "aLinkDomain:" + aLinkDomain + "\n";
    str += "HTMLDnsPrefetch:" + HTMLDnsPrefetch + "\n";
    str += "HTMLPrefetch:" + HTMLPrefetch + "\n";
    str += "HMTLPrerender:" + HMTLPrerender + "\n";
    str += "HTTPDnsPrefetch:" + HTTPDnsPrefetch + "\n";
    str += "HTTPPrefetch:" + HTTPPrefetch + "\n";
    str += "HTTPPrerender:" + HTTPPrerender + "\n";
    return str;
}

function getDomainsFromCrossDomainALinks(page){
    return page.evaluate(function(getDomain) {
        var domains = [];
        var domainsSet = [];

        var links = document.getElementsByTagName("a");
        var url = undefined;
        var protocolIndex = -1;
        for (var i =0 ; i < links.length; i++){
            url = links[i].href;

            protocolIndex = -1;
            if (typeof url === typeof "a string"){
                // The check is required for some websites such as leboncoin.fr that apparently store objects instead of
                // strings in href. Counted 26 instances that weren't strings on 2016-06-03.
                protocolIndex = url.indexOf("://");
            }

            // Cross-domain links always contain the protocol in their url followed by ://
            if( protocolIndex > -1){
                url = getDomain(url);
                if ( url.indexOf(":") === -1 && !domainsSet[url]){
                    domains.push(url);
                    domainsSet[url] = true;
                }
            }
        }
        return domains;
    }, getDomainFromURL);
}

function prefetchList(page) {
    var prefetch = page.evaluate(function (getDomain) {
        var dns_prefetch = [];
        var domainDnsPrefetch = [];
        var prefetch = [];
        var domainPrefetch = [];
        var prerender = [];
        var domainPrerender = [];

        var links = document.getElementsByTagName("link");
        for (var i = 0; i < links.length; i++) {
            var url = links[i].href;
            url = getDomain(url);

            if (links[i].rel === "dns-prefetch" && !domainDnsPrefetch[url]) {
                dns_prefetch.push(url);
                domainDnsPrefetch[url] = true;
            }
            else if (links[i].rel === "prefetch" && !domainPrefetch[url]) {
                prefetch.push(url);
                domainPrefetch[url] = true;
            }
            else if (links[i].rel === "prerender" && !domainPrerender[url]) {
                prerender.push(url);
                domainPrerender[url] = true;
            }
        }
        return [dns_prefetch, prefetch, prerender]
    }, getDomainFromURL);

    return prefetch;
}


function listFieldsFromHeaders(headers){
    var fields = [];
    for(var i = 0; i < headers.length; i++){
        fields.push(headers[i].name);
    }
    return fields;
}


function getFieldFromHeaders(headers, fieldName){
    var values = [];
    for(var i = 0; i < headers.length; i++){
        if (fieldName.toUpperCase() === headers[i].name.toUpperCase()){
            values.push(headers[i].value);
        }
    }
    return values;
}

function getPrefetchFromHeaders(headers, domains, domainsSet){
    var values = getFieldFromHeaders(headers, "Link");
    if ( values.length === 0){
        return values;
    }
    // Parsing according to the syntax of RFC 5988
    // https://tools.ietf.org/html/rfc5988#page-7
    // rel values: dns-prefetch, prefetch and prerender registered at http://www.iana.org/assignments/link-relations/link-relations.xhtml
    // Explained in https://www.w3.org/TR/resource-hints/#dns-prefetch

    // TODO find an example to check if the parsing is correct.
    var index = undefined;
    var value = undefined;
    var url = undefined;
    var rel = undefined;
    for (var i = 0; i < values.length; i++) {
        value = values[i];

        // We are only interested in cross domain prefetch
        if (value.indexOf("://") > -1) {
            index = value.lastIndexOf(";");

            // Parsing url from header
            url = value.slice(0, index);
            url = url.slice(url.indexOf("<") + 1, url.length);
            url = url.slice(0, url.indexOf(">"));
            url = getDomainFromURL(url);

            rel = value.slice(index + 1, value.length);
            index = [rel.toUpperCase().indexOf("dns-prefetch".toUpperCase()),
                rel.toUpperCase().indexOf("prefetch".toUpperCase()),
                rel.toUpperCase().indexOf("prerender".toUpperCase())];

            if (!domainSet[url])
                if (index[0] > -1) { // dns-prefetch
                    domains[0].push(url);
                    domainSet[url] = true;
                } else if (index[1] > -1) { // prefetch
                    domains[1].push(url);
                    domainSet[url] = true;
                } else if (index[2] > -1) { // prerender
                    domains[2].push(url);
                    domainSet[url] = true;
                }
        }

    }
}

/**
 * loadPage define the hooks to gain information on the load page process and tell phantomJS to start loading the page
 *
 * */

function loadPage(address, callback){
    page = require('webpage').create();
    // No cache
    page.offlineStorageQuota = 0;
    page.address = address;
    page.domain = [];
    page.httpPrefetchdomains = [[],[],[]];
    page.ignored = [];
    page.loadCount = -1;
    page.loadEnded = -1;
    page.status = "failed";
    var domainSet = [];

    page.onLoadStarted = function(){
        page.loadCount++;
        if (page.loadCount === 0) {
            console.log("Started loading page: " + page.address);
        } else {
            console.log("Additional onLoadStarted event for: " + page.address);
        }

    };

    page.onResourceRequested = function(req, networkRequest){
        if (debug){
            console.log("Requested resource #" + req.id + " on " + req.time)
        }
        var domain = req.url;
        domain = getDomainFromURL(domain);
        if (domain.indexOf(":") !== -1){
            // To prevent results such as data:image to appear in the domain list.
            // This happens for example on nextinpact.com (26/05/2016)
            console.log("Warning: Found invalid domain: "+ domain);
        }
        else if (!domainSet[domain]) {
            // only add a domain once
            domainSet[domain] = true;
            page.domain.push(domain);
        }

        // HTTP prefetch
        getPrefetchFromHeaders(req.headers, page.httpPrefetchdomains, domainSet);
    };

    page.onResourceReceived = function(response){
        // HTTP prefetch
        getPrefetchFromHeaders(response.headers, page.httpPrefetchdomains, domainSet);
    };

    page.onResourceTimeout = function (request) {
        if (debug){
            console.log('Response timeout (#' + request.id + '): ' + JSON.stringify(request));
        }
    };

    page.onResourceError = function (resourceError) {
        if ( getDomainFromURL(resourceError.url) == getDomainFromURL(page.address) ){
            sleep(0, exitOnError, page, resourceError);
            // The sleep seems to fix the segfault with steamcommunity.com on exit for some reason.
            // segfault on exit inside onResourceReceived function seems to be a known bug,
            // this one probably has the same cause.
            // https://github.com/ariya/phantomjs/issues/11306


        } else if (debug) {
            console.log('\nUnable to load resource (#' + resourceError.id + ' URL:' + resourceError.url + ')');
            console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
        }
    };

    page.onNavigationRequested = function (url, type, willNavigate, main) {
        if (debug) {
            console.log('\nTrying to navigate to: ' + url);
            console.log('Caused by: ' + type);
            console.log('Will actually navigate: ' + willNavigate);
            console.log('Sent from the page\'s main frame: ' + main);
        }
    };

    page.onLoadFinished = function(status){
        if (page.loadEnded == 0){
            page.mainStatus = status;
        }
        if (debug) console.log(status);
        page.status = status;
        page.loadEnded +=1;
        // sleep is used to allow additional load event to be triggered during this time.
        // Only an error or the end of the last load event will cause the program to exit.
        var timeout = 1;
        console.log("Waiting "+ timeout +" seconds for additional events.");
        if (status != 'success') {
            sleep(1, ifFinished(failed), page, page.loadCount);
        } else {
            sleep(1, ifFinished(succeeded), page, page.loadCount);
        }

    };

    page.clearCookies();
    page.clearMemoryCache();
    page.open(page.address, callback);

}

function noop(status){}

loadPage(address, noop);
