#!/usr/bin/env python3
import os
import subprocess
import json

path = ".."
dns_server = "192.168.1.2"
cache = {}
count = 0

def addRecord(dico, domain, type):
    if domain in dico.keys():
        if type not in dico[domain]:
            dico[domain].append(type)
            return 1
    elif domain is not None:
        dico[domain] = [type]
        return 1
    return 0

print("Finding all domains with their respective record types")
for (directory_path, directory_names, file_names) in os.walk(path):
    for file_name in file_names:
        if file_name == "dns.json":
            file_path = os.path.join(directory_path, file_name)

            f = open(file_path, 'r')
            lines = f.readlines()
            f.close()

            dns = json.loads(lines[0])

            for e in dns['query']:
                # Format: ( time, trasanction_id, domain, record_type, layer3_name )
                count += addRecord(cache, e[2], e[3])

print("Caching " + str(count) + " domains using dig on server " + dns_server)
i = 0
for domain in cache.keys():
    for record_type in cache[domain]:
        if i % 10 == 0:
            print("Processed " + str(i) + " entries", end='\r')
        subprocess.call(["dig", "@"+dns_server, domain, record_type], stdout=subprocess.DEVNULL)
        i += 1
print("Processed " + str(i) + " entries", end='\n')
print("Cached " + str(i) + " entries")
