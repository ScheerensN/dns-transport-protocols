#!/usr/bin/env python3
import json
import logging

from launch import debug

queryKey = 'query'
responseKey = 'response'


def initDictionaryField(dictionary, key, value):
    """
    If the key isn't in dictionary, add value with that key
    :param dictionary: a dictionary
    :param key: a possible key for the dictionary
    :param value: a value
    :return: The value is added to dictionary if the key wasn't in the dictionary
    """
    if key not in dictionary.keys():
        dictionary[key] = value


def dnsToString(packet):
    """
    Returns A tuple with the pertinent information about the DNS packet
    :param packet: a DNS packet
    :return: A tuple with the pertinent information about the packet
    """
    return (float(packet.frame_info.time_relative),  # Time from the the first captured frame.
            str(packet.dns.id),  # Transaction id

            packet.dns.qry_name if int(packet.dns.flags_response) == 0 else  # Query or response.
            packet.dns.resp_name if int(packet.dns.count_answers) > 0 else None,  # Handle responses without answers

            packet.dns.qry_type.showname_value.split(" ")[0],  # String representing the record type
            str(packet[1].layer_name)  # ip or ipv6
            )


def ignoredToString(packet):
    """
    Return a formatted string about the dns packet. The format is the one for packet that will be ignored.
    :param packet: a DNS packet
    :return: a formatted string
    """
    return "%s %s %s" % (
        "Q" if int(packet.dns.flags_response) == 0 else "R",
        packet.dns.qry_name,
        packet.dns.qry_type.showname_value.split(" ")[0])


def dissectDns(dictionary, pcap, domains, filtered, url=None):
    """
    The function will store relevant information about the dns packets from pcap, whose queried hostname is in domains,
     into the dictionary with a list for queries and one for responses.
    Filtered will be filled with the queried hostnames preceeded by "Q " or "R ", respectively for queries and response,
    of the packets whose queried hostname isn't in domains.
    :param dictionary: a dictionary
    :param pcap: a pcap file containing dns packets
    :param domains: a list of the domains
    :param filtered: an empty list
    :return: dictionary and filtered are modified as described above.
    """
    dictionary[queryKey] = []
    dictionary[responseKey] = []

    for packet in pcap:
        layer2 = packet[1]  # Handle both IPv4 and IPv6 as layer 2
        try:
            if packet.dns.qry_name not in domains:
                filtered.append(ignoredToString(packet))

            else:
                if int(packet.dns.flags_response) == 0:  # Queries
                    if debug:
                        print("DNS %s Query %s from %s to %s for %s" % (
                            packet.dns.qry_type.showname_value.split(" ")[0],  # record type
                            packet.dns.id,
                            layer2.src,
                            layer2.dst,
                            packet.dns.qry_name
                        ))
                    dictionary[queryKey].append(dnsToString(packet))

                elif int(packet.dns.flags_response) == 1:  # Response
                    if debug:
                        print("DNS %s Response %s from %s to %s for %s" % (
                            packet.dns.qry_type.showname_value.split(" ")[0],
                            packet.dns.id,
                            layer2.src,
                            layer2.dst,
                            packet.dns.resp_name if int(packet.dns.count_answers) > 0 else None
                        # a valid response might not have an answer
                        ))
                    dictionary[responseKey].append(dnsToString(packet))
                else:
                    print("Packet isn't a DNS Query or Response")

        except AttributeError as e:
            if url is None:
                print("Warning: AttributeError: the packet isn't a DNS packet.\n")
                logging.warning("AttributeError: the packet isn't a DNS packet.%s" % packet)
            else:
                print("Warning: AttributeError: the packet isn't a DNS packet. [%s]\n" % url)
                logging.warning("AttributeError: the packet isn't a DNS packet. [%s]" % url)


def dissectDns(dictionary, pcap, domains, filtered, url=None):
    """
    The function will store relevant information about the dns packets from pcap, whose queried hostname is in domains,
     into the dictionary with a list for queries and one for responses.
    Filtered will be filled with the queried hostnames preceeded by "Q " or "R ", respectively for queries and response,
    of the packets whose queried hostname isn't in domains.
    :param dictionary: a dictionary
    :param pcap: a pcap file containing dns packets
    :param domains: a list of the domains
    :param filtered: an empty list
    :return: dictionary and filtered are modified as described above.
    """
    dictionary[queryKey] = []
    dictionary[responseKey] = []

    for packet in pcap:
        layer2 = packet[1]  # Handle both IPv4 and IPv6 as layer 2
        try:
            if packet.dns.qry_name not in domains:
                filtered.append(ignoredToString(packet))

            else:
                if int(packet.dns.flags_response) == 0:  # Queries
                    if debug:
                        print("DNS %s Query %s from %s to %s for %s" % (
                            packet.dns.qry_type.showname_value.split(" ")[0],  # record type
                            packet.dns.id,
                            layer2.src,
                            layer2.dst,
                            packet.dns.qry_name
                        ))
                    dictionary[queryKey].append(dnsToString(packet))

                elif int(packet.dns.flags_response) == 1:  # Response
                    if debug:
                        print("DNS %s Response %s from %s to %s for %s" % (
                            packet.dns.qry_type.showname_value.split(" ")[0],
                            packet.dns.id,
                            layer2.src,
                            layer2.dst,
                            packet.dns.resp_name if int(packet.dns.count_answers) > 0 else None
                        # a valid response might not have an answer
                        ))
                    dictionary[responseKey].append(dnsToString(packet))
                else:
                    print("Packet isn't a DNS Query or Response")

        except AttributeError as e:
            if url is None:
                print("Warning: AttributeError: the packet isn't a DNS packet{}.\n".format(packet))
            else:
                print("Warning: AttributeError: the packet isn't a DNS packet. [%s]\n" % url)


def printFiltered(filtered):
    """
    Print information about the filtered queries.
    :param filtered: the list of filtered queries.
    :return: /
    """
    if len(filtered) > 0:
        print("\nThe following " + str(len(filtered)) + " dns queries and responses were filtered out "
              "because their domains don't appear in the resources loaded by phantomJS")
        print(filtered)


def printDnsDictionary(dictionary):
    """
    Print information about the dictionary
    :param dictionary: a dictionary with information about the dns queries.
    :return: /
    """
    print("\nQueries (" + str(len(dictionary[queryKey])) + "):")
    for tuple in dictionary[queryKey]:
        print(tuple)
    print("\nResponses(" + str(len(dictionary[responseKey])) + "):")
    for tuple in dictionary[responseKey]:
        print(tuple)
    print("\n")

def saveDictionaryAsJSON(dictionary, path):
    """
    Save the dictionary in JSON format in the file at path.
    :param dictionary: a dictionary
    :param path: a file path
    :return: /
    """
    dicoFile = open(path, 'w')
    dicoFile.write(json.dumps(dictionary))
    dicoFile.write("\n")  # All files should be terminated by a blank line.
    dicoFile.close()

# Analyse a pcap if called directly
if __name__ == '__main__':
    import sys
    import pyshark
    from optparse import OptionParser, OptionGroup

    parser = OptionParser(usage="usage: %prog [options]\n",
                          description="Display and optionally save information about the dns queries in the pcap file.")

    parser.add_option("-s", "--save-result", action="store_true", dest="save", default=False,
                      help="Whether the dns timings should be saved. Default is False")

    group = OptionGroup(parser, "Path options")
    group.add_option("--pcap", action='store', type='string', dest='pcap', default='dns.pcap',
                     help='Specify the path of the pcap file.')
    group.add_option("--domain-json", action='store', type='string', dest='domains_json', default='domains.json',
                     help="Specify the path of the JSON file containing the domains.")
    group.add_option("--dns-json", action='store', type='string', dest='dns_json', default='dns.json',
                     help="Specify the path to which the JSON file containing the DNS timings will be saved.")
    parser.add_option_group(group)

    (options, args) = parser.parse_args()

    _dnsTransactions = {}
    _filtered = []
    _pcap = pyshark.FileCapture(options.pcap)

    _domain_file = open(options.domains_json, 'r')
    _domains = json.loads(_domain_file.readline())
    _domain_file.close()

    dissectDns(_dnsTransactions, _pcap, _domains, _filtered)
    printDnsDictionary(_dnsTransactions)
    printFiltered(_filtered)

    saveDictionaryAsJSON(_dnsTransactions, options.dns_json)

