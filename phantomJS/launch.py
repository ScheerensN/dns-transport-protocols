#!/usr/bin/env python3
import os
from sys import stderr, argv
import subprocess
from time import sleep
import datetime
from enum import Enum, IntEnum, unique
import json

import logging
from optparse import OptionParser, OptionGroup

import pyshark

debug = False

VERSION = "1.0"


@unique
class PhJSReturnCode(IntEnum):
    # See http://doc.qt.io/qt-4.8/qnetworkreply.html#NetworkError-enum for standard ones.

    # custom
    BadArgumentsError = 8
    UrlLoadFailedError = 9

    # standard
    Success = 0
    ConnectionRefusedError = 1
    ConnectionClosedError = 2
    HostNotFoundError = 3
    SocketTimeoutError = 4
    OperationCanceledError = 5
    SslHandshakeFailedError = 6
    TemporaryNetworkFailureError = 7

    ProxyConnectionRefusedError = 101
    ProxyConnectionClosedError = 102
    ProxyNotFoundError = 103
    ProxyTimeoutError = 104
    ProxyAuthenticationRequiredError = 105

    ContentAccessDenied = 201
    ContentOperationNotPermittedError = 202
    ContentNotFoundError = 203
    AuthenticationRequiredError = 204

    ProtocolUnknownError = 301
    ProtocolInvalidOperationError = 302

    UnknownNetworkError = 99
    UnknownContentError = 299
    ProtocolFailure = 399

    def __str__(self):
        return self.name + "(" + str(self.value) + ")"

    def returnMessage(code):
        try:
            return PhJSReturnCode(code).name
        except ValueError as e:
            return "UndocumentedPhantomJSError"


@unique
class ReturnCode(Enum):
    success = 0
    phantomError = 1
    phantomUnableToLoadURL = 2
    phantomHostNotFound = 3
    timeout = 4


def analyze(URL, options):
    """
    Analyze an URL and save the DNS timings to a json file and info about the url in a txt file.
    :param URL: a string containing the Url to analyze
    :param options: The parsed options
    :return: an int: Success is 0, anything else is an error.
    """
    startTime = datetime.datetime(datetime.MINYEAR, 1, 1).now()
    # Init
    if not os.path.exists(options.path + "/" + URL + "/"):
        os.makedirs(options.path + "/" + URL + "/")

    pcapPath = options.path + "/" + URL + "/" + options.pcap
    domainsPath = options.path + "/" + URL + "/" + options.domains_json
    prefetchPath = options.path + "/" + URL + "/" + options.prefetch_json
    dnsPath = options.path + "/" + URL + "/" + options.dns_json
    infoPath = options.path + "/" + URL + "/" + options.info

    tcpdumpStart = "tcpdump -i " + options.interface + " -w " + pcapPath + " udp port 53 or tcp port 53 &"
    phantomJSPrefix = ["phantomjs", "--disk-cache=false", "--local-storage-path=/dev/null",
                      "--ssl-protocol=any", "--ignore-ssl-errors=true" ]  # PhantomJS doesn't handle https very well
    if options.debug_phantom:
        phantomJSPrefix.append("--debug=yes")

    phantomJSStart = list(phantomJSPrefix)
    phantomJSStart.extend([options.script, URL, domainsPath, prefetchPath, str(options.debug)])

    phantomJSStartWWW = list(phantomJSPrefix)
    phantomJSStartWWW.extend([options.script, "www." + URL, domainsPath, prefetchPath,
                              str(options.debug)])  # add www. prefix to URL

    tcpdumpStop = ["pkill", "tcpdump"]

    phantomJSReturnCode = -1
    failed = True
    tries = 0
    while failed and tries < options.tries:
        try:
            # Launch the processes
            print("")
            subprocess.call(tcpdumpStart, shell=True)
            sleep(1)  # wait a few seconds for tcpdump to start.
            phantomJSReturnCode = subprocess.call(phantomJSStart, timeout=options.timeout)

            # Special case. If we had a host not found without www, we retry with www.
            # If it works it is either a 404 page or a real page.
            if options.www_host_retry and URL.upper().count("WWW") == 0:
                if phantomJSReturnCode == PhJSReturnCode.HostNotFoundError:
                    print("\nFailed to find host %s. Retrying with www.%s" % (URL, URL))
                    logging.warning("Failed to find host %s. Retrying with www.%s" % (URL, URL))
                    phantomJSReturnCode = subprocess.call(phantomJSStartWWW, timeout=options.timeout)
                    if phantomJSReturnCode != PhJSReturnCode.Success:
                        # put back the original error code.
                        print("\n%s(%d): Failed with the additional www" % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode))
                        logging.warning("%s(%d): Failed with the additional www" % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode))
                        phantomJSReturnCode = PhJSReturnCode.HostNotFoundError

                elif phantomJSReturnCode == PhJSReturnCode.ConnectionClosedError:
                    print("\nHost %s closed the connection. Retrying with www.%s" % (URL, URL))
                    logging.warning("Host %s closed the connection.  Retrying with www.%s" % (URL, URL))
                    phantomJSReturnCode = subprocess.call(phantomJSStartWWW, timeout=options.timeout)
                    if phantomJSReturnCode != PhJSReturnCode.Success:
                        # put back the original error code.
                        print("\n%s(%d): Failed with the additional www" % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode))
                        logging.warning("%s(%d): Failed with the additional www" % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode))
                        phantomJSReturnCode = PhJSReturnCode.ConnectionClosedError

                elif phantomJSReturnCode == PhJSReturnCode.ConnectionRefusedError:
                    print("\nHost %s refused the connection. Retrying with www.%s" % (URL, URL))
                    logging.warning("Host %s refused the connection.  Retrying with www.%s" % (URL, URL))
                    phantomJSReturnCode = subprocess.call(phantomJSStartWWW, timeout=options.timeout)
                    if phantomJSReturnCode != PhJSReturnCode.Success:
                        # put back the original error code.
                        print("\n%s(%d): Failed with the additional www" % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode))
                        logging.warning("%s(%d): Failed with the additional www" % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode))
                        phantomJSReturnCode = PhJSReturnCode.ConnectionRefusedError

            if phantomJSReturnCode == PhJSReturnCode.Success:
                failed = False
            else:
                subprocess.call(tcpdumpStop)
                sleep(1)
                tries += 1
                print("%s(%d): PhantomJS could not load the page at %s on its %d try."
                      % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode, URL, tries), file=stderr)
                logging.error("%s(%d): PhantomJS could not load the page at %s on its %d try."
                              % (PhJSReturnCode.returnMessage(phantomJSReturnCode), phantomJSReturnCode, URL, tries))

        except subprocess.TimeoutExpired as e:
            subprocess.call(tcpdumpStop)
            sleep(1)
            tries += 1
            error_msg = "TimeoutError: %s has timed out on its %d try." % (URL, tries)
            print("\n" + error_msg + "\n", file=stderr)
            logging.error(error_msg)

    if failed and (phantomJSReturnCode == -1 or phantomJSReturnCode == PhJSReturnCode.Success):
        msg = "Error: %s has failed to load." % URL
        print(msg, file=stderr)
        logging.error(msg)
        return ReturnCode.timeout

    # Stop tcpdump.
    sleep(2)  # wait a few seconds for the packets to be delivered to tcpdump.
    # Some or all packets might not show up in the pcap without this.
    subprocess.call(tcpdumpStop)
    print("")

    if phantomJSReturnCode == PhJSReturnCode.UrlLoadFailedError:
        msg = "Error: PhantomJS could not load the page at %s." % URL
        logging.error(msg)
        print(msg, file=stderr)
        return ReturnCode.phantomUnableToLoadURL

    elif phantomJSReturnCode == PhJSReturnCode.HostNotFoundError:
        msg = "Error: Host %s not found." % URL
        logging.error(msg)
        print(msg, file=stderr)
        return ReturnCode.phantomHostNotFound

    elif phantomJSReturnCode != PhJSReturnCode.Success:
        logging.error("%s(%s): PhantomJS couldn't load %s." %
                      (PhJSReturnCode.returnMessage(phantomJSReturnCode), str(phantomJSReturnCode), URL))
        print("%s(%s): PhantomJS couldn't load %s." %
                      (PhJSReturnCode.returnMessage(phantomJSReturnCode), str(phantomJSReturnCode), URL), file=stderr)
        return ReturnCode.phantomError

    # Read the domains from the resources loaded by phantomJS
    domain_file = open(domainsPath, 'r')
    domains = json.loads(domain_file.readline())
    domain_file.close()

    # Read the domains from the links and prefetches (HTML and HTTP)
    fetch_file = open(prefetchPath, 'r')
    # Format of prefetchs should be [aLinks, [HTMLDNSPrefetch, HTMLPrefetch, HTMLPrerender], [HTTPDNSPrefetch, HTTPPrefetch, HTTPRender]]
    prefetchs = json.loads(fetch_file.readline())
    fetch_file.close()
    prefetchs = [prefetchs[0], prefetchs[1][0], prefetchs[1][1], prefetchs[1][2], prefetchs[2][0], prefetchs[2][1], prefetchs[2][2]]

    mergedDomains = mergeLists([domains, prefetchs[0], prefetchs[1], prefetchs[2], prefetchs[3], prefetchs[4], prefetchs[5], prefetchs[6]])


    # Extract information from the pcap
    pcapFile = pyshark.FileCapture(pcapPath)
    dnsTransactions = {}
    filtered = []

    dissectDns(dnsTransactions, pcapFile, mergedDomains, filtered, URL)
    pcapFile.close()

    # Print information on STDOUT
    printDnsDictionary(dnsTransactions)
    printFiltered(filtered)

    # save dnsTransactions to a file in JSON format.
    saveDictionaryAsJSON(dnsTransactions, dnsPath)

    stopTime = datetime.datetime(datetime.MINYEAR, 1, 1).now()

    # save a text file with some info about the website domains.
    info = "%s:%s\n" % ("version", VERSION)
    info += "%s:%s\n" % ("url", URL)
    info += "%s:%s\n" % ("status", "success")
    info += "%s:%s\n" % ("startedAt", str(startTime))
    info += "%s:%s\n" % ("stoppedAt", str(stopTime))
    info += "%s:%s\n" % ("duration", str(stopTime - startTime))
    info += "%s:%d\n" % ("domains", len(domains))
    info += "%s:%d\n" % ("aLinks", len(prefetchs[0]))
    info += "%s:%d\n" % ("HTMLDnsPrefetch", len(prefetchs[1]))
    info += "%s:%d\n" % ("HTMLPrefetch", len(prefetchs[2]))
    info += "%s:%d\n" % ("HTMLPrerender", len(prefetchs[3]))
    info += "%s:%d\n" % ("HTTPDnsPrefetch", len(prefetchs[4]))
    info += "%s:%d\n" % ("HTTPPrefetch", len(prefetchs[5]))
    info += "%s:%d\n" % ("HTTPPrerender", len(prefetchs[6]))
    info += "%s:%s\n" % ("Ignored", filtered)

    infoFile = open(infoPath, 'w')
    infoFile.write(info)
    infoFile.close()

    if options.top:
        print("Took %s for %s" % (str(stopTime - startTime), URL))
    return ReturnCode.success



def mergeLists(listOfLists):
    """
    Merge the lists in listOfLists and return the merged list. No element is duplicated inside the merged list.
    :param listOfLists: a list of list
    :return: a merged list without duplicated elements.
    """
    mergedList = []

    if len(listOfLists) == 0:
        return mergedList

    for l in listOfLists:
        for item in l:
            if item not in mergedList:
                mergedList.append(item)

    return mergedList


def isCommented(string):
    """
    return true if the string starts with #. # can be preceded by spaces and tabs.
    :param string: a string
    :return: a boolean
    """
    position = string.find("#")
    if position > -1:
        if len(string[0:position]) == string[0:position].count(" ") + string[0:position].count("    "):
            return True
    return False


def parsing():
    """
    Defines the options for optparse and return the parser.
    :return: the parser.
    """
    default_timeout = 5

    parser = OptionParser(usage="usage: %prog [options] URL\n       %prog [options] -a\n       %prog [options] --top",
                          description="Display and save information about the dns queries generated when browsing the URL(s) with phantomjs.")
    # TODO put eth0 as default
    parser.add_option("-i", "--interface", action='store', type='string', dest='interface', default='wlan0',
                      help="Specify on which interface the script will listen.")
    parser.add_option("-p", "--port", action='store', type='string', dest='port', default="53",
                      help="Specify on which port the script will listen. Shouldn't be changed for DNS.")
    parser.add_option("-a", "--top", action='store_true', dest="top", default=False,
                      help="Whether to use the alexa top 500 URLs or the specified one.")
    parser.add_option("--timeout", action='store', type='int', dest="timeout", default=default_timeout*60,
                      help="The time before loading a page timeout. Default is " + str(default_timeout) + " minutes.")
    parser.add_option("--try", action='store', type='int', dest="tries", default=3,
                      help="The number of times we will try to load a URL before skipping it. Default is 3 times.")

    group = OptionGroup(parser, "Input path options")
    group.add_option("-s", "--script", action='store', type='string', dest='script', default='loadPage.js',
                     help="The path to the phantomJS script. You shouldn't have to change this.")
    group.add_option("-r", "--top-alexa-raw", action='store', type='string', dest='top1M', default='top-1m.csv',
                     help="Only used if the file at -t doesn't exists. The path of the top 1 000 000 from alexa (available zipped at "
                          "'https://support.alexa.com/hc/en-us/articles/200449834-Does-Alexa-have-a-list-of-its-top-ranked-websites-'"
                          " this program require the csv to be extracted) and updated daily")
    group.add_option("-t", "--top-alexa-500", action='store', type='string', dest='top500', default='top-500.txt',
                     help="The path to which the alexa top 500 will be saved. If the file already exists, "
                          "it will be used as input for the URLs regardless of its size.")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Output path options")
    group.add_option("--path", action='store', type='string', dest='path', default='./websites',
                     help="Path to which the outputs of the program will be saved.")
    group.add_option("-e", "--error-log", action='store', type='string', dest='errorLog', default='launch.log',
                     help="The name of the log file.")
    group.add_option("--pcap", action='store', type='string', dest='pcap', default='dns.pcap',
                     help='Specify the name of the pcap file.')
    group.add_option("--domain-json", action='store', type='string', dest='domains_json', default='domains.json',
                     help="Specify the name of the JSON file containing the domains.")
    group.add_option("--prefetch-json", action="store", type='string', dest='prefetch_json', default='prefetch.json',
                     help="Specify the name of the JSON file containing the domains.")
    group.add_option("--dns-json", action='store', type='string', dest='dns_json', default='dns.json',
                     help="Specify the name of the JSON file containing the DNS timings.")
    group.add_option("--info", action='store', type='string', dest='info', default='info.txt',
                     help="Specify the name of the text file containing the information "
                          "about the number of domains coming from different type of sources..")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Debug Options")
    parser.add_option("-d", "--debug", action='store_true', dest="debug", default=False,
                      help="Whether to show general debug information.")
    parser.add_option("--debug-phantomjs", action='store_true', dest="debug_phantom", default=False,
                      help="Whether to show PhantomJS debug information.")
    parser.add_option("--www-host-retry", action='store_true', dest="www_host_retry", default=True,
                      help="Whether to retry with a www prefix for specific errors when pertinent.")
    parser.add_option_group(group)

    return parser


def startPrint():
    """
    Print the time at which something started and return the datetime object representing that time.
    :return: a datetime set to the moment the function was called.
    """
    startTime = datetime.datetime(datetime.MINYEAR, 1, 1).now()
    msg = "Started at: " + str(startTime)
    print(msg)
    logging.info(msg)
    return startTime


def stopPrint(startTime):
    """
    Print the time at which something started (startTime) and stopped (now) and the time it took.
    :param startTime: a datetime object representing the time at which something started
    :return: a datetime object representing the time at which something stopped.
    """
    stopTime = datetime.datetime(datetime.MINYEAR, 1, 1).now()
    msg = "Started at: " + str(startTime) + "\n"
    msg += "Finished at: " + str(stopTime) + "\n"
    msg += "Took: " + str(stopTime - startTime) + "\n"
    print(msg)
    logging.info(msg)
    return stopTime


if __name__ == '__main__':
    from dissect import dissectDns, saveDictionaryAsJSON, printDnsDictionary, printFiltered

    # Arguments parsing
    parser = parsing()
    (options, args) = parser.parse_args()

    debug = options.debug

    # Start logging.
    logPath = os.path.split(options.path + "/" + options.errorLog)[0]
    if not os.path.exists(logPath):
        os.makedirs(logPath)

    logging.basicConfig(filename=options.path + "/" + options.errorLog, level=logging.INFO)

    logging.info("Launching " + argv[0] + " " + VERSION)

    # Arguments check
    if len(args) < 1 and not options.top:
        parser.print_help()
        logging.error("Error: Argument Missing")
        exit("\nError: Argument Missing")

    ret = None
    failed = "Failed %d urls: "
    timedout = "Timeout (%d): "
    notFound = "%d urls were not found: "
    failedCount = 0
    timedoutCount = 0
    notFoundCount = 0
    # Top alexa
    if options.top:
        try:
            top500 = open(options.top500, 'r')
        except FileNotFoundError as e:
            print("\nThe top 500 file doesn't exist yet.\n Creating it.")
            try:
                # Accumulates in a string to reduce the number of IO calls.
                # If the goal is to reduce the memory footprint, put the writes in the loop.
                top_string = ""

                top1M = open(options.top1M, 'r')
                for line in top1M:
                    elem = line.split(",")
                    if int(elem[0]) > 500:
                        break
                    top_string += elem[1]
                top1M.close()

                top_string += "\n"
                top500 = open(options.top500, 'w')
                top500.write(top_string)
                top500.close()
                print("The file has been created at: " + options.top500)
                # Whether we created the file or not we finish in the same state with regards to top500
                top500 = open(options.top500, 'r')
            except FileNotFoundError as e:
                logging.error("FileNotFoundError: Couldn't find the top 1 000 000 from " + options.top1M)
                print("FileNotFoundError: Couldn't find the top 1 000 000 from " + options.top1M)
                raise

        start = startPrint()
        for line in top500:
            url = line.split("\n")[0]
            if url != "" and not isCommented(url):
                ret = analyze(url, options)
                if ret != ReturnCode.success:
                    if ret == ReturnCode.phantomHostNotFound:
                        notFound += url + " "
                        notFoundCount += 1
                    elif ret == ReturnCode.timeout:
                        timedout += url + " "
                        timedoutCount += 1
                    else:
                        failed += url + " "
                        failedCount += 1

        if failedCount > 0:
            print(failed % failedCount, file=stderr)
            logging.error(failed % failedCount)
        if notFoundCount > 0:
            print(notFound % notFoundCount, file=stderr)
            logging.error(notFound % notFoundCount)
        if timedoutCount > 0:
            print(timedout % timedoutCount, file=stderr)
            logging.error(timedout % timedoutCount)

        stop = stopPrint(start)
        top500.close()
    else:
        # one URL
        start = startPrint()
        ret = analyze(args[0], options)

        if ret != ReturnCode.success:
            if ret == ReturnCode.phantomUnableToLoadURL or ret == ReturnCode.phantomError:
                failed += args[0]
                print(failed % 1, file=stderr)
                logging.error(failed % 1)
            elif ret == ReturnCode.timeout:
                timedout += args[0]
                print(timedout % 1, file=stderr)
                logging.error(timedout % 1)
            elif ret == ReturnCode.phantomHostNotFound:
                notFound += args[0]
                print(notFound % 1, file=stderr)
                logging.error(notFound % 1)

        stop = stopPrint(start)





