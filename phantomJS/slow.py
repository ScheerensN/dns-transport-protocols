#!/usr/bin/env python3
import os

path = "websites-2016-07-14-16-06"
time_limit = 5
total_m = 0
total_s = 0

remove_list = []
for (directory_path, directory_names, file_names) in os.walk(path):
    for file_name in file_names:
        if file_name == "info.txt":
            file_path = os.path.join(directory_path, file_name)
            #print(file_path)

            f = open(file_path, 'r')
            i = 0
            for line in f.readlines():
                i += 1
                if (i == 3):
                    line = line.strip('\n').split(':')[1]
                    if (line != "success"):
                        break
                if (i == 6):
                    line = line.strip('\n').split(':')
                    if int(line[2]) >= time_limit:
                        remove_list.append(file_path + " " + line[2] + ":" + line[3].split(".")[0])
                    else:
                        total_m += int(line[2])
                        total_s += int(line[3].split(".")[0])
                    break
            f.close()

print("Total time to rerun test: " + str(total_m + total_s//60) + "m")
print("Websites exceeding the time limit (" + str(time_limit) + "):")
for e in remove_list:
    print(e)





