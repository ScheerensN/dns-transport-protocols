import sys
from collections import namedtuple
import json
import heapq


Dom = namedtuple('Dom', ['time', 'depth'])


def load(wprof_json_data, rr_type="A", debug=False):
    objects = dict()
    activity = dict()
    download = dict()
    offset_dict = dict()
    domains = dict()
    FLOAT_INFINITY = sys.float_info.max  # biggest possible float value
    INT_INFINITY = sys.maxsize # biggest possible int value

    # Generate a list of all the downloads with format:
    # [ absolute_time, object_id, domain, download_id, activity_dictionary, successor_list ]
    # absolute_time is set to 0 and successor_list is empty
    for obj in wprof_json_data["objs"]:
        oid = obj['id']
        domain = obj["url"].split("://")[1].split("/")[0].split(":")[0]
        if domain not in domains.keys():
            domains[domain] = Dom(FLOAT_INFINITY, INT_INFINITY)
        comp = {}
        for c in obj["comps"]:
            comp[c['id']] = (c["time"]/1000)
            activity[c['id']] = (oid, (c["time"])/1000)
        did = obj["download"]["id"]
        download[did] = (oid, 0)
        objects[oid] = [0.0, oid, domain, obj["download"]["id"], comp, []]
        offset_dict[oid] = None

    # clone the objects dictionary since it will be modified later.
    ref = dict(objects)
    activity.update(download)

    # Modify objects to become a tree where a node depends on its ancestor
    # Calculate and updates recursively the absolute_time where each downloads happen
    for dep in wprof_json_data["deps"]:
        if dep["a2"] in download.keys():
            (rootobj_id, actoffset) = activity[dep["a1"]]
            rootobj = ref[rootobj_id]
            offset = max((dep["time"]/1000) + actoffset, 0.0)
            (depobj_id, o) = activity[dep["a2"]]
            depobj = ref[depobj_id]
            depobj[0] = offset

            heapq.heappush(rootobj[5], depobj)
            try:
                del objects[depobj_id]
            except KeyError:
                pass

    shortest_time_to(domains, objects.values())

    #print(json.dumps(domains, sort_keys=True, indent=4))
    #print(json.dumps(objects.values(), sort_keys=True, indent=4))
    #print("###########################################################################################################")
    sparse_tree = drop_node(objects.values(), domains)
    #print(json.dumps(sparse_tree, sort_keys=True, indent=4))
    #print("###########################################################################################################")
    drop_duplicates(sparse_tree)
    #print(json.dumps(sparse_tree, sort_keys=True, indent=4))
    #print("###########################################################################################################")
    depedency_tree = convert_to_depedency_tree(sparse_tree, rr_type)
    #print(json.dumps(depedency_tree, sort_keys=True, indent=4))

    # Calculate the minimum absolute_time value for a given domain
    #min_dict = {}
    #dict_min_traversal(min_dict, objects.values())
    if debug:
        print(json.dumps(domains, sort_keys=True, indent=4))

    #sparse_wprof_tree = drop_branch(list(objects.values()), min_dict)
    #double_drop_fix(sparse_wprof_tree)

    #return sparse_wprof_tree
    #return wprof_to_dependency_tree(sparse_wprof_tree, query_record_type)

    # Sanity check (original number of domains equals the number of domains in the result)
    domains_dep = list()
    assert count_domains_in_result(depedency_tree, domains_dep) == len(list(domains.keys())), \
        "wprof.py: Sanity check failed"

    return depedency_tree


def count_domains_in_result(tree, domains=None):
    if domains == None:
        domains = list()

    for node in tree:
        if node[1] not in domains:
            domains.append(node[1])
        count_domains_in_result(node[3], domains)

    return len(domains)


def shortest_time_to(domains, tree, acc=0.0, depth=0):
    for node in tree:
        #  print("DDD " + str(node[0]) + " " + str(node[2]) + " " + str(acc) + " " + str(domains[node[2]]) + " " + str(domains.keys()))
        if (float(node[0]) + acc) < domains[node[2]].time:
            #  print("DDD True")
            domains[node[2]] = Dom(float(node[0]) + acc, depth)
        shortest_time_to(domains, node[5], acc + node[0], depth+1)


def drop_node(tree, dict_min_value, acc=0.0, depth=0):
    sparse_tree = []
    for i, node in enumerate(tree):
        if len(node) > 0:
            if dict_min_value[node[2]].time == (acc + node[0]) and dict_min_value[node[2]].depth == depth:
                node[5] = drop_node(node[5], dict_min_value, acc + node[0], depth+1)
                sparse_tree.append(node)
            elif len(node[5]) > 0:
                sparse_tree + drop_node(node[5], dict_min_value, acc + node[0], depth + 1)
    return sparse_tree


def drop_duplicates(tree):
    to_drop = set()
    for i, node_i in enumerate(tree):
        for j, node_j in enumerate(tree):
            if j > i:
                if node_i[2] == node_j[2] and node_i[0] == node_j[0]:
                    to_drop.add(j)
        if len(node_i[5]) > 0:
            drop_duplicates(node_i[5])

    for j in sorted(to_drop, reverse=True):
        # reversed order to not have problems object's indexes changing
        if len(tree[j][5]) == 0:
            #print("Poping {} {} {}".format(j, tree[j][2], tree[j][0]))
            tree.pop(j)
        else:
            #print("wprof.py: Warning: Poping {} {} {} which has successors (might be normal depending on data source)".format(j, tree[j][2], tree[j][0]))
            tree.pop(j)


def convert_to_depedency_tree(tree, rrtype="A"):
    dep_tree = []
    for q in tree:
        dep_tree.append([q[0], q[2], rrtype, convert_to_depedency_tree(q[5], rrtype)])
    return dep_tree


def dict_min_traversal(dictionary, tree):
    for node in tree:
        if node[2] in dictionary.keys():
            if node[0] < dictionary[node[2]]:
                dictionary[node[2]] = node[0]
        else:
            dictionary[node[2]] = node[0]
        if len(node[5]) > 0:
            dict_min_traversal(dictionary, node[5])


def drop_branch(tree, dict_min_value):
    sparse_tree = []
    for i, node in enumerate(tree):
        if len(node) > 0:
            if dict_min_value[node[2]] == node[0]:
                node[5] = drop_branch(node[5], dict_min_value)
                sparse_tree.append(node)
            elif len(node[5]) > 0:
                sparse_tree.append(drop_branch(node[5], dict_min_value))
    return sparse_tree


def double_drop_fix(tree):
    for i, node in enumerate(tree):
        if not len(node) == 6:
            # print("fix")
            double_drop_fix_rec(tree, i, node)
        else:
            # print('fine')
            double_drop_fix(node[5])


def double_drop_fix_rec(tree, j, node):
    #print(tree)
    to_remove = []
    for i, n in enumerate(node):
        #print(n)
        if len(n) == 6:
            double_drop_fix(n[5])
            tree.append(n)
            #print('fix_fine')
        elif len(n) > 0:
            #print('fix_rec')
            double_drop_fix_rec(tree, -1, n)
    if j != -1:
        tree.remove(node)


def wprof_to_dependency_tree(sparse_tree, record_type):
    dependency_tree = []
    for q in sparse_tree:
        dependency_tree.append([q[0], q[2], record_type, wprof_to_dependency_tree(q[5], record_type)])
    return dependency_tree


if __name__ == '__main__':
    f = open("../input/wprof/www.amazon.com_/www.amazon.com_.json", 'r')
    #f = open("../wprof/www.google.com_/www.google.com_.json", 'r')
    #f = open("../wprof/www.ebay.co.uk_/www.ebay.co.uk_.json", 'r')
    data = json.load(f)
    result = load(data)
    print(json.dumps(result, sort_keys=True, indent=4))
    f.close()

