# Copyright (C) 2003-2007, 2009-2011 Nominum, Inc.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose with or without fee is hereby granted,
# provided that the above copyright notice and this permission notice
# appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND NOMINUM DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL NOMINUM BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Modified code from the dnspython lib

import os
import sys
import traceback
import io
import socket
import select
import struct
import time
import heapq
import subprocess

import ssl
import pycurl
import dns
from dns.resolver import Resolver, NoMetaqueries
from dns.message import make_query
from dns.query import _connect

# We should ignore SIGPIPE when using pycurl.NOSIGNAL - see
# the libcurl tutorial for more info.
try:
    import signal
    from signal import SIGPIPE, SIG_IGN
    signal.signal(signal.SIGPIPE, signal.SIG_IGN)
except ImportError:
    pass

import log
import config as cfg
# from measurement import Measurement, start_capture, stop_capture
from struct_various import Protocol, PacketRetention, FastRetransmitType
# import benchmark

DNS_ROOT = dns.name.Name([b''])  # argument is a list of binary encoded domain. The root domain is reprsented as b''

event_read = select.POLLIN | select.POLLPRI
event_write = select.POLLOUT
event_closed = select.POLLRDHUP
event_error = select.POLLERR

https_retention = None
https_socket_info = None


def set_d_answer(d, answer, received):
    item = d[answer.id]
    item.pop(1)
    item.insert(1, answer)
    item.pop(3)
    item.insert(3, received)
    d[answer.id] = item


def set_d_sent(d, i, sent):
    item = d[i]
    item.pop(2)
    item.insert(2, sent)
    d[i] = item


def set_d_timeout(d, i, resent_time):
    item = d[i]
    timeout = item.pop(4)
    item.insert(4, timeout + 1)
    resend_list = item.pop(5)
    resend_list.append(resent_time)
    item.insert(5, resend_list)
    d[i] = item


def make_query_str(qname, rdtype, rdclass=dns.rdataclass.IN, use_edns=None, want_dnssec=False,
                   ednsflags=None, payload=None, request_payload=None, options=None):
    if isinstance(qname, str):
        qname = dns.name.from_text(qname, None)
    if isinstance(rdtype, str):
        rdtype = dns.rdatatype.from_text(rdtype)
    if dns.rdatatype.is_metatype(rdtype):
        raise NoMetaqueries
    if isinstance(rdclass, str):
        rdclass = dns.rdataclass.from_text(rdclass)
    if dns.rdataclass.is_metaclass(rdclass):
        raise NoMetaqueries
    return make_query(qname, rdtype, rdclass, use_edns, want_dnssec, ednsflags, payload, request_payload, options)


def _poll_for(fd, readable, writable, closed, error, timeout):
    """Poll polling backend.
    @param fd: File descriptor
    @type fd: socket
    @param readable: Whether to wait for readability
    @type readable: bool
    @param writable: Whether to wait for writability
    @type writable: bool
    @param timeout: Deadline timeout (expiration time, in seconds)
    @type timeout: float
    @return True on success, False on timeout
    """
    event_mask = 0
    if readable:
        event_mask |= event_read
    if writable:
        event_mask |= event_write
    if closed:
        event_mask |= event_closed
    if error:
        event_mask |= event_error

    pollable = select.poll()
    pollable.register(fd, event_mask)

    if timeout:
        event_list = pollable.poll(timeout * 1000)
    else:
        event_list = pollable.poll()

    event = 0
    for e in event_list:
        event |= e[1]

    return event


def _select_for(fd, readable, writable, error, timeout):
    """Poll polling backend.
    @param fd: File descriptor
    @type fd: socket
    @param readable: Whether to wait for readability
    @type readable: bool
    @param writable: Whether to wait for writability
    @type writable: bool
    @param timeout: Deadline timeout (expiration time, in seconds)
    @type timeout: float
    @return True on success, False on timeout
    """
    event_mask = 0
    fd_read = []
    fd_write = []
    fd_error = []
    if readable:
        fd_read.append(fd)
    if writable:
        fd_write.append(fd)
    if error:
        fd_error.append(fd)

    (fd_read_ready_list, fd_write_ready_list, fd_error_ready_list) = select.select(fd_read, fd_write, fd_error, float(timeout))
    flags = 0
    # print((fd_read_ready_list, fd_write_ready_list, fd_error_ready_list))
    if len(fd_read_ready_list):
        flags |= event_read

    if len(fd_write_ready_list):
        flags |= event_write

    if len(fd_error_ready_list):
        flags |= event_error

    return flags


def get_tcp_early_retransmit():
    p = subprocess.Popen(["sysctl", "-bn", "net.ipv4.tcp_early_retrans"], stdout=subprocess.PIPE,
                         stderr=subprocess.DEVNULL, universal_newlines=True)
    return str(p.communicate()[0])


def set_tcp_early_retransmit(value):
    if isinstance(value, type(0)):
        if int(value) in range(0, 5):
            p = subprocess.Popen(["sudo", "-S", "sysctl", "-w", "net.ipv4.tcp_early_retrans=" + str(value)],
                                 stdout=subprocess.PIPE)
            p.wait()
    elif isinstance(value, type(FastRetransmitType)):
        set_tcp_early_retransmit(value.value)


def dns_rcode_to_text(ans):
    mask = 1 + 2 + 4 + 8
    rcode = ans.rcode() & mask
    if rcode == 0:
        return "DNS Answer {} {} {}".format(ans.id, ans.question, ans.authority)
    elif rcode == 1:
        return "DNS Error {} {} {}".format(ans.id, ans.question, "{}: Format error - The name server was "
                                                                "unable to interpret the query."
                                           .format(rcode))
    elif rcode == 2:
        return "DNS Error {} {} {}".format(ans.id, ans.question, "{}: Server failure - The name server was "
                                                                "unable to process this query due to a "
                                                                "problem with the name server. "
                                           .format(rcode))
    elif rcode == 3:
        return "DNS Error {} {} {}".format(ans.id, ans.question, "{}: Name Error - Meaningful only for "
                                                                "responses from an authoritative name "
                                                                "server, this code signifies that the "
                                                                "domain name referenced in the query does "
                                                                "not exist. ".format(rcode))
    elif rcode == 4:
        return "DNS Error {} {} {}".format(ans.id, ans.question, "{}: Not Implemented - The name server "
                                                                "does not support the requested kind of "
                                                                "query.".format(rcode))
    elif rcode == 5:
        return "DNS Error {} {} {}".format(ans.id, ans.question, "{}: Refused - The name server refuses to "
                                                                "perform the specified operation for "
                                                                "policy reasons.  For example, "
                                                                "a name server may not wish to provide "
                                                                "the information to the particular "
                                                                "requester, or a name server may not wish "
                                                                "to perform a particular operation (e.g., "
                                                                "zone transfer) for particular data. "
                                                                "".format(rcode))
    else:
        return "DNS Error {} {} {}".format(ans.id, ans.question, "{}: Unknown Error ???".format(rcode))


class ImprovedAggResolver(Resolver):

    def __init__(self, yaml_profile, server, filename='/etc/resolv.conf', configure=True, stream_log_level=log.INFO,
                 file_log_level=log.INFO, file_log_path=None):
        """Initialize a resolver instance.

        @param filename: The filename of a configuration file in
        standard /etc/resolv.conf format.  This parameter is meaningful
        only when I{configure} is true and the platform is POSIX.
        @type filename: string or file object
        @param configure: If True (the default), the resolver instance
        is configured in the normal fashion for the operating system
        the resolver is running on.  (I.e. a /etc/resolv.conf file on
        POSIX systems and from the registry on Windows systems.)
        @type configure: bool"""
        super().__init__(filename=filename, configure=configure)
        self.logger = log.get_logger(__name__, stream_level=stream_log_level, file_level=file_log_level,
                                     log_path=file_log_path)
        self.profile = yaml_profile
        self.profile_server = server
        self.sslcontext = None
        self.sslsession = None
        self.conn = None
        self.protocol = None
        self.tfo = self.profile[Protocol.TCP.name]['tfo']
        self.ssl_version = 'TLS_Default'
        self.ssl_resumption = False
        os.environ['SSLKEYLOGFILE'] = "{}{}".format(cfg.options.path, "SSLKEYLOGFILE")  # Works with both TLS and HTTPS
        self.poll_timeout = self.profile['global']['poll_timeout']
        self.retention = PacketRetention.NAGLE
        self.logger.info("{} {}".format(self.__class__.__name__, self.__init__.__name__))

    def query_agg(self, queries, protocol):
        global https_socket_info
        aggregate_info = dict()
        self.protocol = protocol
        self.logger.debug("Capturing {}".format(self.protocol.name))
        if self.protocol == Protocol.UDP:
            for i in range(0, self.profile['global']['samples']):
                self.logger.debug("iter {}".format(i))
                stacktrace = None
                try:
                    aggregate_info[i] = self.query_udp_profile(queries[self.profile_server][Protocol.UDP.name][i])
                except KeyboardInterrupt as e:
                    raise e
                except Exception as e:
                    stacktrace = sys.exc_info()
                finally:
                    if stacktrace:
                        self.logger.error(traceback.print_exception(*stacktrace))
                    del stacktrace

        elif protocol == Protocol.TCP or protocol == Protocol.TLS:
            # remember initial value for net.ipv4.tcp_early_retrans
            initial_tcp_early_retransmit = get_tcp_early_retransmit()
            self.logger.info("initial value for net.ipv4.tcp_early_retrans: {}".format(initial_tcp_early_retransmit))
            for retention in self.profile[protocol.name]['packet_retention']:
                self.retention = PacketRetention[retention.upper()]
                aggregate_info[self.retention.name] = dict()
                # do stuff
                for retransmit in self.profile[protocol.name]['tcp_fast_retransmit']:
                    retransmit = FastRetransmitType[retransmit]
                    aggregate_info[self.retention.name][retransmit.name] = dict()
                    # self.logger.info("Retransmit scheme set to {}".format(retransmit.name))
                    self.logger.info("Setting net.ipv4.tcp_early_retrans to {}".format(retransmit.value))
                    for tfo in self.profile[protocol.name]['tfo']:
                        self.tfo = tfo
                        tfo_str = "TFO" if self.tfo else "NO_TFO"
                        aggregate_info[self.retention.name][retransmit.name][tfo_str] = dict()
                        if protocol == Protocol.TCP:
                            self.logger.debug(
                                "Testing {} {} {}".format(self.retention.name, retransmit.name, tfo_str))
                            for i in range(0, self.profile['global']['samples']):
                                self.logger.debug("iter {}".format(i))
                                stacktrace = None
                                try:
                                    aggregate_info[self.retention.name][retransmit.name][tfo_str][i] = \
                                        self.query_tcp_profile(queries[self.profile_server][protocol.name][
                                                                   self.retention.name][retransmit.name][tfo_str][i])
                                except KeyboardInterrupt as e:
                                    stacktrace = None
                                    raise e
                                except Exception as e:
                                    stacktrace = sys.exc_info()
                                finally:
                                    if stacktrace:
                                        self.logger.error(traceback.print_exception(*stacktrace))
                                    del stacktrace

                        elif protocol == Protocol.TLS:
                            if not 'ssl_version' in list(self.profile[protocol.name].keys()):
                                # Reset TLS Resumption parameters for each iteration
                                self.sslcontext = None
                                self.sslsession = None

                                self.logger.debug(
                                    "Testing {} {} {}".format(self.retention.name, retransmit.name, tfo_str))
                                for i in range(0, self.profile['global']['samples']):
                                    self.logger.debug("iter {}".format(i))
                                    stacktrace = None
                                    try:
                                        aggregate_info[self.retention.name][retransmit.name][tfo_str][i] = \
                                            self.query_tcp_profile(queries[self.profile_server][protocol.name][
                                                                       self.retention.name][retransmit.name][tfo_str][
                                                                       i])
                                    except KeyboardInterrupt as e:
                                        stacktrace = None
                                        raise e
                                    except Exception as e:
                                        stacktrace = sys.exc_info()
                                    finally:
                                        if stacktrace:
                                            self.logger.error(traceback.print_exception(*stacktrace))
                                        del stacktrace

                            else:
                                for ssl_version_str in self.profile[protocol.name]['ssl_version']:
                                    if ssl_version_str in ['TLSv1', 'TLSv1_1', 'TLSv1_2', 'TLSv1_3']:
                                        self.ssl_version = ssl_version_str
                                    else:
                                        self.ssl_version = 'TLS_Default'
                                    aggregate_info[self.retention.name][retransmit.name][tfo_str][ssl_version_str] = dict()
                                    self.logger.info("Using {}".format(ssl_version_str))

                                    for ssl_resumption in self.profile[protocol.name]['tls_resumption']:
                                        ssl_resumption_str = 'Resumption' if ssl_resumption else 'No_Resumption'
                                        aggregate_info[self.retention.name][retransmit.name][tfo_str][
                                            ssl_version_str][ssl_resumption_str] = dict()
                                        self.ssl_resumption = ssl_resumption

                                        # Reset TLS Resumption parameters for each iteration
                                        self.sslcontext = None
                                        self.sslsession = None
                                        self.logger.debug("Testing {} {} {} {} {}".format(self.retention.name, retransmit.name, tfo_str, ssl_version_str, ssl_resumption_str))
                                        for i in range(0, self.profile['global']['samples']):
                                            stacktrace = None
                                            try:
                                                self.logger.debug("iter {}".format(i))
                                                aggregate_info[self.retention.name][retransmit.name][tfo_str][
                                                    ssl_version_str][ssl_resumption_str][i] = \
                                                    self.query_tcp_profile(queries[self.profile_server][protocol.name][
                                                                               self.retention.name][retransmit.name][
                                                                               tfo_str][
                                                                               ssl_version_str][ssl_resumption_str][i])
                                            except ssl.SSLError as e:
                                                stacktrace = sys.exc_info()
                                                if i == 0:
                                                    self.logger.error('SSLError: No common protocol. Stop trying {}'.format(ssl_version_str))
                                                else:
                                                    # should happen on first iteration
                                                    self.logger.error('SSLError: No common protocol on iter > 0 !. Stop trying {}'.format(ssl_version_str))
                                                # stop trying this protocol and remove unneeded data from data structure
                                                if ssl_version_str in aggregate_info[self.retention.name][retransmit.name][tfo_str]:
                                                    del aggregate_info[self.retention.name][retransmit.name][tfo_str][ssl_version_str]
                                                break
                                            except KeyboardInterrupt as e:
                                                stacktrace = None
                                                raise e
                                            except Exception as e:
                                                stacktrace = sys.exc_info()
                                            finally:
                                                if stacktrace:
                                                    self.logger.error(traceback.print_exception(*stacktrace))
                                                del stacktrace
                                        else:
                                            continue
                                        break
            set_tcp_early_retransmit(initial_tcp_early_retransmit)
            self.logger.info("net.ipv4.tcp_early_retrans set back to {}".format(get_tcp_early_retransmit()))
        elif protocol == Protocol.HTTPS:
            global https_retention
            aggregate_info = dict()
            self.protocol = protocol

            # remember initial value for net.ipv4.tcp_early_retrans
            initial_tcp_early_retransmit = get_tcp_early_retransmit()
            self.logger.info("initial value for net.ipv4.tcp_early_retrans: {}".format(initial_tcp_early_retransmit))
            for retention in self.profile[protocol.name]['packet_retention']:
                self.retention = PacketRetention[retention.upper()]
                aggregate_info[self.retention.name] = dict()
                # do stuff
                for retransmit in self.profile[protocol.name]['tcp_fast_retransmit']:
                    retransmit = FastRetransmitType[retransmit]
                    aggregate_info[self.retention.name][retransmit.name] = dict()
                    # self.logger.info("Retransmit scheme set to {}".format(retransmit.name))
                    self.logger.info("Setting net.ipv4.tcp_early_retrans to {}".format(retransmit.value))
                    self.logger.debug("Testing {} {}".format(self.retention.name, retransmit.name))
                    for i in range(0, self.profile['global']['samples']):
                        self.logger.debug("iter {}".format(i))
                        https_socket_info = None #reset
                        stacktrace = None
                        try:
                            aggregate_info[self.retention.name][retransmit.name][i] = self.query_https_profile(queries[self.profile_server][protocol.name][self.retention.name][retransmit.name][i])
                        except KeyboardInterrupt as e:
                            stacktrace = None
                            raise e
                        except Exception as e:
                            stacktrace = sys.exc_info()
                        finally:
                            if stacktrace:
                                self.logger.error(traceback.print_exception(*stacktrace))
                            del stacktrace

            set_tcp_early_retransmit(initial_tcp_early_retransmit)
            self.logger.info("net.ipv4.tcp_early_retrans set back to {}".format(get_tcp_early_retransmit()))

        else:
            try:
                raise NotImplementedError
            except NotImplementedError as e:
                self.logger.error(type(e).__name__)
                return dict()
        return aggregate_info

    def queries_flatten(self, l, queries):
        """
        Append the every item of queries to l and call itself on each item.next to do the same.
        Do not modify the items.
        :param l: The list to append the elements in.
        :param queries: The Tree to walk trough
        :return:
        """
        for node in queries:
            l.append(node)
            self.queries_flatten(l, node.next)

    def query_udp_profile(self, queries):
        # queries format [QueryNode(offset, query, next),]. list used as a heapq
        queries = list(queries)  # Don't modify original list
        queries_waiting_for_response = dict()  # index is the query id to find it easily
        query_sent = 0
        response_received = 0
        anormal_response_received_list = []

        delta_dep_send = []
        delta_dep_receive = []

        transfer_times = dict()  # Format dns_id = [query, answer, sent, received, timeout number, [resent_time_after_timeout,]]

        # use to find quickly the query to retransmit based on id
        retransmit_query = dict()
        q = []
        self.queries_flatten(q, queries)
        response_to_receive = len(q)
        for node in q:
            retransmit_query[node.query.id] = node.query
            transfer_times[node.query.id] = [node.query, None, 0, 0, 0, []]
        retransmit_timer = []  # heapq. (time, id) added when a query is sent.

        dest_profile = None
        for server_info in self.profile['global']['server']:
            if self.profile_server in list(server_info.keys()):
                dest_profile = server_info[self.profile_server]

        if dest_profile is None:
            self.logger.error("Couldn't find server information from profile for {}".format(self.profile_server))

        (af, destination, source) = dns.query._destination_and_source(None, dest_profile['host'],
                                                                      dest_profile['port'][Protocol.UDP.name],
                                                                      None, None)
        dest = destination[0]

        self.conn = socket.socket(af, socket.SOCK_DGRAM)
        self.conn.setblocking(0)

        transfer_start = time.time()
        timeout = False

        while len(queries) > 0 or len(queries_waiting_for_response) > 0:
            event = _poll_for(self.conn, True, len(queries) > 0 or len(retransmit_timer) > 0, True, True,
                              self.poll_timeout)

            if event & event_read:
                delta = time.time()
                (wire, from_address) = self.conn.recvfrom(65535)

                if dns.query._addresses_equal(af, from_address, destination) \
                        or (dns.inet.is_multicast(dest) and from_address[1:] == dest):
                    ans = dns.message.from_wire(wire, question_only=False,
                                                one_rr_per_rrset=self.profile['global']['one_rr_per_rrset'],
                                                ignore_trailing=self.profile['global']['ignore_trailing'])
                    received = time.time()
                    if transfer_times[ans.id][3] == 0:  # not a duplicate packet
                        set_d_answer(transfer_times, ans, received)
                        response_received += 1
                        # Add dependency to be transmitted
                        node = queries_waiting_for_response[ans.id]
                        for n in node.next:
                            n.offset = received - transfer_start + n.offset
                            heapq.heappush(queries, n)
                        del queries_waiting_for_response[ans.id]

                        # don't need to be retransmitted:
                        to_remove = None
                        for elem in retransmit_timer:
                            if elem[1] == ans.id:
                                to_remove = elem
                        if to_remove is not None:
                            retransmit_timer.remove(to_remove)
                            heapq.heapify(retransmit_timer)
                        delta_dep_receive.append(time.time() - delta)

                        # Check return code

                        # if len(ans.answer) > 0:
                        #    print("DNS Answer {} {} {}".format(ans.id, ans.question, ans.answer))
                        # else:
                        #    print(dns_rcode_to_text(ans))
                        mask = 1 + 2 + 4 + 8
                        if (ans.rcode() & mask) != 0:
                            # if the answer's rcode is not 0
                            anormal_response_received_list.append(
                                {'id': ans.id, 'question': str(ans.question), 'error': dns_rcode_to_text(ans)})
                            self.logger.warning("ID '{}' Q '{}' ERROR '{}'".format(ans.id, ans.question, dns_rcode_to_text(ans)))

                elif not self.profile['global']['ignore_unexpected']:
                    error = dns.query.UnexpectedSource('got a response from %s instead of %s'
                                                     % (from_address, destination))
                    self.logger.error(error)
                    raise dns.query.UnexpectedSource('got a response from %s instead of %s'
                                                     % (from_address, destination))

            else:
                # Check if there is something to retransmit otherwise transmit new data if available
                retransmitted = False
                if len(retransmit_timer) > 0 and event & event_write:
                    # Retry queries when there is nothing to read or send.
                    elem = heapq.heappop(retransmit_timer)

                    if (time.time() - elem[0]) > self.profile[Protocol.UDP.name]['retry_timeout'] \
                            and transfer_times[elem[1]][4] < self.profile[Protocol.UDP.name]['retry_occurence']:
                        # if timeout and retries left
                        self.logger.debug('Retransmit')
                        retransmitted = True
                        query = retransmit_query[elem[1]]
                        wire = query.to_wire(origin=DNS_ROOT)
                        set_d_timeout(transfer_times, elem[1], time.time())
                        self.conn.sendto(wire, destination)

                    elif transfer_times[elem[1]][4] < self.profile[Protocol.UDP.name]['retry_occurence']:
                        # no timeout and retry left for timer
                        #self.logger.debug('Retransmit: put back in the pot')
                        heapq.heappush(retransmit_timer, elem)
                    else:
                        # elem has reach its maximum number of retries and is not put back.
                        # The query is also removed from the list of query to send.
                        self.logger.debug('Retransmit: Exceeded retransmit limit. Give up')
                        queries.remove(retransmit_query[elem[1]])
                        del queries_waiting_for_response[elem[1]]  # Not waiting on it anymore
                        self.logger.warning("Gave up retransmission for {} after {} try"
                                            .format(str(elem[1]), self.profile[Protocol.UDP.name]['retry_occurence']))

                if not retransmitted and len(queries) > 0 and event & event_write:
                    # check if its time to send the next query
                    delta = time.time()

                    if (transfer_start + queries[0].offset) <= time.time():
                        node = heapq.heappop(queries)
                        wire = node.query.to_wire(origin=DNS_ROOT)
                        sent = time.time()
                        self.conn.sendto(wire, destination)
                        set_d_sent(transfer_times, node.query.id, sent)
                        queries_waiting_for_response[node.query.id] = node
                        query_sent += 1

                        # set retransmit
                        heapq.heappush(retransmit_timer, (sent, node.query.id))
                        delta_dep_send.append(time.time() - delta)

            if transfer_start + (3.0 * 60.0) < time.time():  # Usually takes 2 sec
                timeout = True
                self.logger.error("Stuck in the loop for more than 3 minutes. Breaking out.")
                self.logger.error("len: {} Query: {}".format(len(queries), queries))
                self.logger.error("len: {} Waiting for answer: {}".format(len(queries_waiting_for_response), queries_waiting_for_response))
                break

        conn_info = dict()
        conn_info['src_port'] = self.conn.getsockname()[1]
        self.conn.close()
        self.conn = None
        conn_info['response'] = {
            'received': response_received,
            'expected': response_to_receive,
            'anormal': len(anormal_response_received_list),
            'anormal_list': anormal_response_received_list,
            'timeout': timeout
        }

        self.logger.debug("Received {} response of {}".format(str(response_received), str(response_to_receive)))
        return conn_info

    def _net_read(self, count):
        """Read the specified number of bytes from sock.  Keep trying until we
        either get the desired amount, or we hit EOF.
        A Timeout exception will be raised if the operation is not completed
        by the expiration time.
        """
        s = b''
        # SSL fix !
        n = 0
        try:
            n = self.conn.recv(count)
            if n == b'':
                raise EOFError
            count -= len(n)
            s += n
        except ssl.SSLWantReadError:
            # we will wait for data on poll.
            pass
        # SSL fix !
        while count > 0:
            _poll_for(self.conn, True, False, True, True, self.poll_timeout)
            try:
                n = self.conn.recv(count)
                if n == b'':
                    raise EOFError
                count = count - len(n)
                s = s + n
            except ssl.SSLWantReadError:
                # we will wait for data on poll.
                pass
        return s

    def _net_write(self, data):
        """Write the specified data to the socket.
        A Timeout exception will be raised if the operation is not completed
        by the expiration time.
        """
        current = 0
        l = len(data)
        while current < l:
            _poll_for(self.conn, False, True, True, True, self.poll_timeout)
            current += self.conn.send(data[current:])

    def _net_write_tfo(self, destination, data):
        """Write the specified data to the socket.
        A Timeout exception will be raised if the operation is not completed
        by the expiration time.
        TCP will use the TFO option.
        """
        MSG_FASTOPEN = 0x20000000  # Magic value to do a TFO Handshake
        current = 0
        l = len(data)
        while current < l:
            _poll_for(self.conn, False, True, True, True, self.poll_timeout)
            current += self.conn.sendto(data[current:], MSG_FASTOPEN, destination)

    def query_tcp_profile(self, queries):
        """
        Query over udp for the Messages in queries.
        :param queries: [query,]. query type is Message
        :param protocol: Enum Protocol: Acceptable input are TCP or TLS. Default and fallback if invalid value is TCP.
        :return: (answers, measurement). answers format is [(time received, answer),...]
        ;raise: dns.
        """

        queries = list(queries)  # Don't modify original list
        queries_waiting_for_response = dict()  # index is the query id to find it easily
        query_sent = 0
        response_received = 0
        anormal_response_received_list = []
        conn_reset_by_peer = False
        conn_EOF = False
        if not isinstance(self.protocol, Protocol) or \
                (self.protocol is not Protocol.TCP and self.protocol is not Protocol.TLS):
            self.protocol = Protocol.TCP
        cork_tls_handshake_optimization = self.profile[Protocol.TLS.name]['cork_tls_handshake_optimization']

        transfer_times = dict()  # Format dns_id = [query, answer, sent, received, timeout number, [resent_time_after_timeout,]]

        q = []
        self.queries_flatten(q, queries)
        response_to_receive = len(q)
        for node in q:
            transfer_times[node.query.id] = [node.query, None, 0, 0, 0, []]

        dest_profile = None
        for server_info in self.profile['global']['server']:
            if self.profile_server in list(server_info.keys()):
                dest_profile = server_info[self.profile_server]
        if dest_profile is None:
            self.logger.error("Couldn't find server information from profile for {}".format(self.profile_server))

        (af, destination, source) = dns.query._destination_and_source(None, dest_profile['host'],
                                                                      dest_profile['port'][self.protocol.name],
                                                                      None, None)

        # Connect
        self.conn = socket.socket(af, socket.SOCK_STREAM)
        # Cork is stronger than NO_DELAY and NAGLE
        if self.retention is PacketRetention.NODELAY:
            self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
        elif self.retention is PacketRetention.NAGLE:
            self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 0)
            self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
        elif self.retention is PacketRetention.CORK:
            self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)
        self.conn.setblocking(True)  # Needed by TLS and TFO
        if self.protocol is Protocol.TLS:
            if not self.sslcontext:
                # We need to reuse the same SSLContext for TLS Resumption
                if self.ssl_version == 'TLS_Default':
                    self.sslcontext = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, capath='/etc/ssl/certs/')
                else:
                    self.sslcontext = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, capath='/etc/ssl/certs/')
                    if self.ssl_version != "TLSv1":
                        self.sslcontext.options |= ssl.OP_NO_TLSv1
                    if self.ssl_version != "TLSv1_1":
                        self.sslcontext.options |= ssl.OP_NO_TLSv1_1
                    if self.ssl_version != "TLSv1_2":
                        self.sslcontext.options |= ssl.OP_NO_TLSv1_2
                    if self.ssl_version != "TLSv1_3":
                        self.sslcontext.options |= ssl.OP_NO_TLSv1_3
            self.sslcontext.check_hostname = self.profile[Protocol.TLS.name]['check_hostname']
            if not self.profile[Protocol.TLS.name]['verify_cert']:
                self.sslcontext.check_hostname = False
                self.sslcontext.verify_mode = ssl.CERT_NONE

                # self.sslcontext.keylog_filename = "{}{}".format(cfg.options.path, "SSLKEYLOGFILE")
            # If we don't use TLS Resumption self.sslsession will stay None see end of function.
            self.conn = self.sslcontext.wrap_socket(self.conn, server_hostname=destination[0], session=self.sslsession)


            if cork_tls_handshake_optimization and self.retention is PacketRetention.CORK:
                # Switch to TCP_NODELAY during the TLS handshake to improve performance as no uncorking is done during the handshake.
                self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
                self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
            self.conn.connect(destination)
            if cork_tls_handshake_optimization and self.retention is PacketRetention.CORK:
                # Switch back to TCP_CORK
                self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)
            self.conn.setblocking(False)  # We can use nonblocking now

        else:
            if self.tfo is True:
                node = heapq.heappop(queries)
                wire = node.query.to_wire(origin=DNS_ROOT)
                l = len(wire)
                tcpmsg = struct.pack("!H", l) + wire
                sent = time.time()
                self._net_write_tfo(destination, tcpmsg)
                #self.logger.debug("DNS Query {} {}".format(node.query.id, node.query.question))
                set_d_sent(transfer_times, node.query.id, sent)
                queries_waiting_for_response[node.query.id] = node
                query_sent += 1
                self.conn.setblocking(False)
            else:
                _connect(self.conn, destination)
                self.conn.setblocking(False)

        transfer_start = time.time()
        timeout_t = 0
        while len(queries) > 0 or len(queries_waiting_for_response) > 0:
            event = _poll_for(self.conn, True, len(queries) > 0, True, True, self.poll_timeout)
            # Break if we have been waiting for more than tcp_timeout
            if event != 0:
                timeout_t = 0  # reset if poll didn't timeout
            elif timeout_t == 0:
                timeout_t = time.time()  # record the time of the first timeout and wait
            else:
                if time.time() > timeout_t + self.profile[self.protocol.name]['tcp_timeout']:
                    # Threshold exceeded. We break.
                    self.logger.warning("Waited > {}s for an answer. Giving up".format(self.profile[self.protocol.name]['tcp_timeout']))
                    break

            # Check the return value of poll to decide if we send or receive.
            if event & event_closed:
                # closed connection => break
                conn_reset_by_peer = True
                self.logger.error('Connection reset by peer')
                break
            elif event & event_read:
                # Priority to read
                delta = time.time()
                try:
                    # Read the two bytes that give the size of the DNS packet
                    ldata = self._net_read(2)
                    (l,) = struct.unpack("!H", ldata)
                    # Read a full DNS packet
                    wire = self._net_read(l)
                except EOFError as e:
                    conn_EOF = True
                    self.logger.warning('EOFError: read EOF on the socket')
                    break
                ans = dns.message.from_wire(wire, question_only=False,
                                            one_rr_per_rrset=self.profile['global']['one_rr_per_rrset'],
                                            ignore_trailing=self.profile['global']['ignore_trailing'])
                received = time.time()
                set_d_answer(transfer_times, ans, received)

                #if len(ans.answer) > 0:
                #    print("DNS Answer {} {} {}".format(ans.id, ans.question, ans.answer))
                #else:
                #    print(dns_rcode_to_text(ans))
                mask = 1 + 2 + 4 + 8
                if (ans.rcode() & mask) != 0:
                    # if the answer's rcode is not 0
                    anormal_response_received_list.append({'id': ans.id, 'question': str(ans.question), 'error': dns_rcode_to_text(ans)})
                    self.logger.warning("ID '{}' Q '{}' ERROR '{}'".format(ans.id, ans.question, dns_rcode_to_text(ans)))

                response_received += 1
                # Add dependency to be transmitted
                node = queries_waiting_for_response[ans.id]
                for n in node.next:
                    n.offset = received - transfer_start + n.offset
                    heapq.heappush(queries, n)
                del queries_waiting_for_response[ans.id]

            elif event & event_write and len(queries) > 0:
                # If we can write and there is something to write.git
                if transfer_start + queries[0].offset <= time.time():
                    node = heapq.heappop(queries)
                    wire = node.query.to_wire(origin=DNS_ROOT)
                    l = len(wire)
                    tcpmsg = struct.pack("!H", l) + wire
                    sent = time.time()
                    self._net_write(tcpmsg)
                    set_d_sent(transfer_times, node.query.id, sent)
                    #self.logger.debug("DNS Query {} {}".format(node.query.id, node.query.question))
                    queries_waiting_for_response[node.query.id] = node
                    query_sent += 1

                    # If we wrote something and don't have something to send anymore. Uncork.
                    if (self.retention is PacketRetention.CORK and len(queries) == 0) or \
                            (self.retention is PacketRetention.CORK and transfer_start + queries[0].offset < time.time() + 0.0003):
                        self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
                        self.conn.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)

        self.logger.info("Received {} response of {}".format(response_received, response_to_receive))
        if len(anormal_response_received_list) > 0:
            self.logger.info("{} abnormal DNS Answer (RCode != 0)".format(len(anormal_response_received_list)))

        if self.protocol is Protocol.TLS and self.profile[self.protocol.name]['tls_resumption']:
            self.sslsession = self.conn.session

        conn_info = dict()
        conn_info['src_port'] = self.conn.getsockname()[1]
        conn_info['response'] = {
            'received': response_received,
            'expected': response_to_receive,
            'anormal': len(anormal_response_received_list),
            'anormal_list': anormal_response_received_list
        }
        conn_info['termination'] = {
            'reset_by_peer': conn_reset_by_peer,
            'EOFError': conn_EOF,
            'client_timeout': timeout_t != 0
        }
        if self.protocol is Protocol.TLS:
            conn_info['tls_cipher'] = list(self.conn.cipher())  # Yaml compatibility
            conn_info['tls_session'] = {
                'id': self.conn.session.id.hex(),
                'time': self.conn.session.time,
                'has_ticket': self.conn.session.has_ticket,
                'lifetime_hint': self.conn.session.ticket_lifetime_hint,
                'timeout': self.conn.session.timeout
            }
            conn_info['tls_session_reused'] = self.conn.session_reused

        self.conn.close()
        self.conn = None

        return conn_info

    https_retention = None
    https_socket = None

    @staticmethod
    def __socket_open(purpose, curl_address):
        family, socktype, protocol, address = curl_address
        s = socket.socket(family, socktype, protocol)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)

        if ImprovedAggResolver.https_retention is PacketRetention.NODELAY:
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
        elif ImprovedAggResolver.https_retention is PacketRetention.NAGLE:
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 0)
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
        elif ImprovedAggResolver.https_retention is PacketRetention.CORK:
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            s.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)
        ImprovedAggResolver.https_socket = s
        return s

    def query_https_profile(self, queries):
        ImprovedAggResolver.https_retention = self.retention
        ImprovedAggResolver.https_socket = None
        queries = list(queries)  # Don't modify original list
        queries_waiting_for_response = dict()  # index is the query id to find it easily
        query_sent = 0
        response_received = 0
        anormal_response_received_list = []

        transfer_times = dict()  # Format dns_id = [query, answer, sent, received, timeout number, [resent_time_after_timeout,]]

        q = []
        self.queries_flatten(q, queries)
        response_to_receive = len(q)
        for node in q:
            transfer_times[node.query.id] = [node.query, None, 0, 0, 0, []]

        dest_profile = None
        for server_info in self.profile['global']['server']:
            if self.profile_server in list(server_info.keys()):
                dest_profile = server_info[self.profile_server]
        if dest_profile is None:
            self.logger.error("Couldn't find server information from profile for {}".format(self.profile_server))

        if dest_profile['host'] == '8.8.8.8' or dest_profile['host'] == '8.8.4.4':
            # Bug bypass for dns.google
            dest_profile['host'] = 'dns.google'

        # Based on https://fragmentsofcode.wordpress.com/2011/01/22/pycurl-curlmulti-example/ adapted for DNS over HTTPS with HTTP/2 and to integrate it with the rest
        doh_provider = "https://{}:{}/dns-query".format(dest_profile['host'], dest_profile['port'][self.protocol.name])
        num_conn = len(q)  # Number of query to do

        # Pre-allocate a list of curl objects
        m = pycurl.CurlMulti()
        m.handles = []
        for i in range(num_conn):
            c = pycurl.Curl()
            c.buffer = None
            c.setopt(pycurl.PIPEWAIT, 1)  # To allow HTTP/2 multiplexing
            # c.setopt(pycurl.M_PIPELINING, pycurl.PIPE_MULTIPLEX)  # Doesn't seem to work as described in https://curl.haxx.se/docs/http2.html
            c.setopt(pycurl.FOLLOWLOCATION, 1)
            c.setopt(pycurl.MAXREDIRS, 5)
            c.setopt(pycurl.CONNECTTIMEOUT, 30)
            c.setopt(pycurl.TIMEOUT, 300)
            c.setopt(pycurl.NOSIGNAL, 1)
            c.setopt(pycurl.OPENSOCKETFUNCTION, ImprovedAggResolver.__socket_open)
            #c.setopt(pycurl.CLOSESOCKETFUNCTION, ImprovedAggResolver.__socket_close)  # cause SIGSEGV. Not sure why.
            c.setopt(pycurl.SSL_VERIFYHOST, 0)
            if not self.profile[Protocol.HTTPS.name]['verify_cert']:
                c.setopt(pycurl.SSL_VERIFYHOST, 0)
                c.setopt(pycurl.SSL_VERIFYPEER, 0)

            m.handles.append(c)
        freelist = m.handles[:]

        timeout_t = 0
        transfer_start = time.time()
        go_away = False
        while len(queries) > 0 or len(queries_waiting_for_response) > 0:
            # Select
            n = m.select(self.poll_timeout)

            # Break if we have been waiting for more than tcp_timeout
            if n > -1:
                timeout_t = 0  # reset if poll didn't timeout
            elif n == -1 and timeout_t == 0:
                timeout_t = time.time()  # record the time of the first timeout and wait
            else:
                if time.time() > timeout_t + self.profile[self.protocol.name]['tcp_timeout']:
                    # Threshold exceeded. We break.
                    self.logger.warning("Waited > {}s for an answer. Giving up".format(self.profile[self.protocol.name]['tcp_timeout']))
                    break

            # Read everything available at this moment. Priority to read.
            while 1:
                delta = time.time()
                num_q, ok_list, err_list = m.info_read()
                for c in ok_list:
                    # treating data:
                    # print("\nSuccess:", c.name, c.url, c.getinfo(pycurl.EFFECTIVE_URL))
                    rcode = c.getinfo(pycurl.RESPONSE_CODE)
                    if rcode == 200:
                        body = c.buffer.getvalue()
                        ans = dns.message.from_wire(body, question_only=False,
                                            one_rr_per_rrset=self.profile['global']['one_rr_per_rrset'],
                                            ignore_trailing=self.profile['global']['ignore_trailing'])
                        received = time.time()

                        mask = 1 + 2 + 4 + 8
                        if (ans.rcode() & mask) != 0:
                            # if the answer's rcode is not 0
                            anormal_response_received_list.append({'id': ans.id, 'question': str(ans.question), 'error': dns_rcode_to_text(ans)})
                            self.logger.warning("ID '{}' Q '{}' ERROR '{}'".format(ans.id, ans.question, dns_rcode_to_text(ans)))
                        ans.id = c.id
                        set_d_answer(transfer_times, ans, received)
                        response_received += 1

                        # Add dependency to be transmitted
                        node = queries_waiting_for_response[c.id]
                        for n in node.next:
                            n.offset = received - transfer_start + n.offset
                            heapq.heappush(queries, n)
                        del queries_waiting_for_response[c.id]
                    else:
                        body = c.buffer.getvalue()
                        if len(body) == 0:
                            body = "[No details]"
                        self.logger.error("HTTP error %i: %s" % (rcode, body))
                        go_away = True
                        break

                    # cleanup
                    c.buffer.close()
                    c.buffer = None
                    m.remove_handle(c)
                    freelist.append(c)
                for c, errno, errmsg in err_list:
                    c.buffer.close()
                    c.buffer = None
                    m.remove_handle(c)
                    self.logger.error("Failed: ", str(c.question[0].name), c.url, errno, errmsg)
                    freelist.append(c)

                if num_q == 0:
                    # Nothing to read anymore. Stop looping.
                    break

            if go_away:
                break

            # Write
            if len(queries) > 0 and len(freelist) > 0 and transfer_start + queries[0].offset <= time.time():
                node = heapq.heappop(queries)
                id = node.query.id
                node.query.id = 0
                wire = node.query.to_wire(origin=DNS_ROOT)
                buffer = io.BytesIO()

                c = freelist.pop()
                c.setopt(pycurl.CAPATH, '/etc/ssl/certs/')
                c.setopt(pycurl.URL, doh_provider)
                c.setopt(pycurl.POST, True)
                c.setopt(pycurl.POSTFIELDS, wire)
                c.setopt(pycurl.HTTPHEADER, ["Content-type: application/dns-message"])
                c.setopt(c.WRITEDATA, buffer)
                c.setopt(pycurl.HTTP_VERSION, pycurl.CURL_HTTP_VERSION_2)
                m.add_handle(c)
                sent = time.time()
                set_d_sent(transfer_times, id, sent)
                # Storing the values in the handle
                c.url = doh_provider
                c.buffer = buffer
                c.query_sent = query_sent
                c.id = id
                c.question = node.query.question

                queries_waiting_for_response[id] = node
                query_sent += 1

                # If we wrote something and don't have something to send anymore. Uncork.
                if (self.retention is PacketRetention.CORK and len(queries) == 0) or \
                        (self.retention is PacketRetention.CORK and transfer_start + queries[
                            0].offset < time.time() + 0.0003):
                    if isinstance(ImprovedAggResolver.https_socket, socket.socket):
                        # Only is a socket after ImprovedAggResolver.__socket_open is called for the first time (some time after m.perform)
                        ImprovedAggResolver.https_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 0)
                        ImprovedAggResolver.https_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_CORK, 1)

            # Run the internal curl state machine for the multi stack
            while 1:
                ret, num_handles = m.perform()
                if ret != pycurl.E_CALL_MULTI_PERFORM:
                    break

            # Loop

        # Close connection:
        https_socket_info = dict()
        https_socket_info['src_port'] = ImprovedAggResolver.https_socket.getsockname()[1]
        https_socket_info['response'] = {
            'received': response_received,
            'expected': response_to_receive,
            'anormal': len(anormal_response_received_list),
            'anormal_list': anormal_response_received_list
        }

        self.logger.info("Received {} response of {}".format(response_received, response_to_receive))
        if len(anormal_response_received_list) > 0:
            self.logger.info("{} abnormal DNS Answer (RCode != 0)".format(len(anormal_response_received_list)))

        # Cleanup
        ImprovedAggResolver.https_socket = None
        for c in m.handles:
            if c.buffer is not None:
                c.buffer.close()
                c.buffer = None
            c.close()
        m.close()

        ImprovedAggResolver.https_retention = None
        ImprovedAggResolver.https_socket = None

        return https_socket_info
