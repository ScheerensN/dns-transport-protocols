#!/usr/bin/env bash
LOG="current_test.log"
PROVIDER="google"

date
echo "Reset TC"
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc show dev enx00145c97c1ed
echo ''

# BASELINE

MName="${PROVIDER}-baseline-50-tls"
MProfile="./profile_${PROVIDER}_baseline_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"

#LAG 400ms
date
echo 'TC: Set delay to 400ms'
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc add dev enx00145c97c1ed root netem delay 400ms
sudo tc qdisc show dev enx00145c97c1ed
echo ''

MName="${PROVIDER}-lag400-50-tls"
MProfile="./profile_${PROVIDER}_baseline_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"

# LAG 200ms
date
echo 'TC: Set delay to 200ms'
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc add dev enx00145c97c1ed root netem delay 200ms
sudo tc qdisc show dev enx00145c97c1ed
echo ''

MName="${PROVIDER}-lag200-50-tls"
MProfile="./profile_${PROVIDER}_baseline_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"


# LOSS .5 NO DELAY
date
echo 'TC: Set loss to 0.5%'
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc add dev enx00145c97c1ed root netem loss 0.5%
sudo tc qdisc show dev enx00145c97c1ed
echo ''

MName="${PROVIDER}-loss.5-NoDelay-50-tls"
MProfile="./profile_${PROVIDER}_Loss_No_Delay_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"

# LOSS .5 NAGLE
date
echo 'TC: Set loss to 0.5%'
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc add dev enx00145c97c1ed root netem loss 0.5%
sudo tc qdisc show dev enx00145c97c1ed
echo ''

MName="${PROVIDER}-loss.5-Nagle-50-tls"
MProfile="./profile_${PROVIDER}_Loss_Nagle_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"


# LOSS 1 NO DELAY
date
echo 'TC: Set loss to 1%'
sudo tc qdisc add dev enx00145c97c1ed root netem loss 1%
sudo tc qdisc show dev enx00145c97c1ed
echo ''

MName="${PROVIDER}-loss1-NoDelay-50-tls"
MProfile="./profile_${PROVIDER}_Loss_No_Delay_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"

# LOSS 1 NAGLE
date
echo 'TC: Set loss to 1%'
sudo tc qdisc add dev enx00145c97c1ed root netem loss 1%
sudo tc qdisc show dev enx00145c97c1ed
echo ''

MName="${PROVIDER}-loss1-Nagle-50-tls"
MProfile="./profile_${PROVIDER}_Loss_Nagle_tls.yaml"
CMD="main.py --log ./Measure/$MName/event.log --experiment-path ./Measure/$MName/ -p $MProfile"
date
echo "python3.8 $CMD" >> $LOG
echo "python3.8 $CMD"
python3.8 $CMD
echo ''
./dissect_dns.py "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/$MName/" "B-Muspelheim-XPS13U_client.pcap"

date
echo "Reset TC"
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc show dev enx00145c97c1ed
echo ''

