#!/usr/bin/env python3.5
import dns
from dns.exception import Timeout
from dns.resolver import NXDOMAIN, NoAnswer, NoNameservers

import benchmark
from improved_resolver import Protocol, make_query_str
from struct_various import QueryNode


def test_server(rslvr, name, rdtype, rrset, proto=Protocol.UDP, options=None):
    msg = rslvr.name + " " + \
          (Protocol.UDP.name if proto is Protocol.UDP
           else Protocol.TCP.name if proto is Protocol.TCP
           else Protocol.TLS.name if proto is Protocol.TLS
           else "???")
    msg += " IPv6: " if options.ip6 else " IPv4: "

    query = list()
    # edns enabled as the response is too big for 512 bytes
    query.append(QueryNode(0.0, make_query_str(name, rdtype, use_edns=0, want_dnssec=False, ), []))

    try:
        r = None
        if proto is Protocol.UDP:
            r = rslvr.udp
        elif proto is Protocol.TCP:
            r = rslvr.tcp
        elif proto is Protocol.TLS:
            r = rslvr.tls

        if options.ip6:
            m = r.query(query, dest=r.nameservers[1], proto=proto, raise_on_no_answer=True, query_loopback=False,
                        silent=True)
        else:
            m = r.query(query, dest=r.nameservers[0], proto=proto, raise_on_no_answer=True, query_loopback=False,
                        silent=True)

        answer = list(m.transfer_times.values())[0][1]

    except Timeout as e:
        print(msg + "Timeout")
        return False
    except NXDOMAIN as e:
        print(msg + "NXDOMAIN")
        return False
    except NoAnswer as e:
        print(msg + "No Answer Section")
        print(str(e))
        return False
    except NoNameservers as e:
        print(msg + "Name Server is broken")
        return False
    except Exception as e:
        print(msg + "Error " + str(e))
        raise e
        return False

    answer_rrset = ""
    for rr in answer.answer:
        answer_rrset += rr.to_text()

    if rrset in str(answer_rrset):
        print(msg + "OK")
        return True
    else:
        print(msg + "RRSET NOK")
        return False


def test_connection(server_list, options):
    status = []
    for server in server_list:
        if server.udp is not None:
            udp = test_server(server, options.ct_name, options.ct_rdtype, options.ct_rrset, proto=Protocol.UDP, options=options)
        if server.tcp is not None:
            tcp = test_server(server, options.ct_name, options.ct_rdtype, options.ct_rrset, proto=Protocol.TCP, options=options)
        if server.tcp is not None:
            tls = test_server(server, options.ct_name, options.ct_rdtype, options.ct_rrset, proto=Protocol.TLS, options=options)
        status.append((server.name, udp, tcp, tls))
    return status

if __name__ == '__main__':
    test_connection()
