#!/bin/bash

ROUTER_SSH_HANDLE="n900"

# Path
DATE_NOW=`date +%Y-%m-%d-%H%M%S`
OUTPUT_PATH="../measurements/loss/amazon"

# Handle benchmark arguments and create a folder for each set 
function sample {
    
    RETENTION=("nodelay" "nagle" "cork")

    for((k=0; k<3; k++));
    do
        for((l=0; l<5; l++));
        do  
            FOLDER=${OUTPUT_PATH}"/"${LAG}-${JITTER}-${LOSS}-${k}-${l}"/"
            start=`date +%Y-%m-%d-%H-%M`
            echo "$start $k $l"
            #echo "Running benchmark DNSSEC"
            #./benchmark.py -c --dnssec --udp --tcp --tls --tfo --packet-retention ${k} --fast-retransmit-level ${l} --substitute-tld test -o ${FOLDER}"dnssec" -r $1 -f $2
            echo "Running benchmark "
            ./benchmark.py -c --udp --tcp --tls --tfo --packet-retention ${k} --fast-retransmit-level ${l} --substitute-tld test -o ${FOLDER}"nodnssec" -r $1 -f $2
        done
    done
}

echo "Init"
ssh $ROUTER_SSH_HANDLE "insmod sch_tbf 2> /dev/null; insmod sch_netem 2> /dev/null"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 0ms 0ms loss 0.0% reorder 0% 0%"

### YOUR MEASUREMENT HERE ###
RESOLVER="resolver.json"

JITTER="jitter_10ms"
LAG="lag_50ms"
LOSS="loss_5%"
echo "amazon 5% loss"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 25ms 5ms loss 2.5% reorder 0% 0%"
   sample ${RESOLVER}  ../dependencytree/amazon.com.json

LOSS="loss_3%"
echo "amazon 3% loss"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 25ms 5ms loss 1.5% reorder 0% 0%"
   sample ${RESOLVER}  ../dependencytree/amazon.com.json

### YOUR MEASUREMENT END ###

# reset
ssh $ROUTER_SSH_HANDLE "tc qdisc del dev eth0 root netem"

STOP_TIME=`date +%Y-%m-%d-%H-%M`
echo "Finished at "$STOP_TIME". Started at "$START_TIME
