#!/bin/bash

ROUTER_SSH_HANDLE="n900"

# Path
DATE_NOW=`date +%Y-%m-%d-%H%M%S`
OUTPUT_PATH="../measurements/TFO"

# Handle benchmark arguments and create a folder for each set 
function sample {
    
    RETENTION=("nodelay" "nagle" "cork")

    for((k=0; k<3; k++));
    do
        FOLDER=${OUTPUT_PATH}
        start=`date +%Y-%m-%d-%H-%M`
        echo "$start $k"
        #echo "Running benchmark TFO DNSSEC"
        #./benchmark.py --dnssec --udp --tcp --tls --tfo --packet-retention 1 --fast-retransmit-level 3 --substitute-tld test -o test -f $2
        echo "Running benchmark DNSSEC"${k}
        ./benchmark.py -c --dnssec --udp --tcp --tls --tfo --packet-retention ${k} --substitute-tld test -o ${FOLDER}/dnssec-${k} -r $1 -f $2
        echo "Running benchmark TFO "${k}
        ./benchmark.py -c --udp --tcp --tls --tfo --packet-retention ${k} --substitute-tld test -o ${FOLDER}/nodnssec-${k} -r $1 -f $2
        #echo "Running benchmark"
        #./benchmark.py --udp --tcp --tls --packet-retention ${k} --fast-retransmit-level ${l} --substitute-tld test -o ${FOLDER}"nodnssec-notfo" -f ../dependencytree/burst10.json
  
    done
}

echo "Init"
ssh $ROUTER_SSH_HANDLE "insmod sch_tbf 2> /dev/null; insmod sch_netem 2> /dev/null"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 0ms 0ms loss 0.0% reorder 0% 0%"

### YOUR MEASUREMENT HERE ###
RESOLVER="resolver.json"

echo "TFO"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 25ms 0ms loss 0.0% reorder 0% 0%"
    sample ${RESOLVER} ../dependencytree/burst3.json


### YOUR MEASUREMENT END ###

# reset
ssh $ROUTER_SSH_HANDLE "tc qdisc del dev eth0 root netem"

STOP_TIME=`date +%Y-%m-%d-%H-%M`
echo "Finished at "$STOP_TIME". Started at "$START_TIME
