#!/bin/bash

ROUTER_SSH_HANDLE="n900"

# Path
DATE_NOW=`date +%Y-%m-%d-%H%M%S`
OUTPUT_PATH="../measurements/reorder/google"

# Handle benchmark arguments and create a folder for each set 
function sample {
    
    RETENTION=("nodelay" "nagle" "cork")

    for((k=0; k<3; k++));
    do
        FOLDER=${OUTPUT_PATH}"/"${LAG}-${JITTER}-${k}"/"
        start=`date +%Y-%m-%d-%H-%M`
        echo "$start $k"
        echo "Running benchmark DNSSEC"
        ./benchmark.py --dnssec --udp --tcp --tls --tfo --packet-retention ${k} --substitute-tld test -o ${FOLDER}"dnssec" -r $1 -f $2
        echo "Running benchmark"
        ./benchmark.py --udp --tcp --tls --tfo --packet-retention ${k} --substitute-tld test -o ${FOLDER}"nodnssec" -r $1 -f $2
    done
}

echo "Init"
ssh $ROUTER_SSH_HANDLE "insmod sch_tbf 2> /dev/null; insmod sch_netem 2> /dev/null"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 0ms 0ms loss 0.0% reorder 0% 0%"

### YOUR MEASUREMENT HERE ###
RESOLVER="resolver.json"

JITTER="jitter_10ms"
LAG="lag_100ms"
echo "google"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 50ms 5ms loss 0.0% reorder 25% 50%"
   sample ${RESOLVER}  ../dependencytree/google.com.json

echo "amazon"
OUTPUT_PATH="../measurements/reorder/amazon"
ssh $ROUTER_SSH_HANDLE "tc qdisc replace dev eth0 root netem limit 10000 delay 50ms 5ms loss 0.0% reorder 25% 50%"
   sample ${RESOLVER}  ../dependencytree/amazon.com.json

### YOUR MEASUREMENT END ###

# reset
ssh $ROUTER_SSH_HANDLE "tc qdisc del dev eth0 root netem"

STOP_TIME=`date +%Y-%m-%d-%H-%M`
echo "Finished at "$STOP_TIME". Started at "$START_TIME
