from collections import namedtuple
import matplotlib
matplotlib.use('Agg')  # Needed to generate plots when there is no display
from matplotlib import pyplot as plt
import numpy as np

from measurement import Measurement
import benchmark
### Measurement plot ###

_PlotData = namedtuple('_PlotData', 'x_sent x_received y x_timeout y_timeout')

marker_size = 3.0

def measurement_to_plot_data(m, y_multi=100):
    transfer_times = m.transfer_times
    data = []
    for k, v in transfer_times.items():
        e = v[2:4]
        e += v[-1:]
        data.append(tuple(e))
    data.sort()
    data_min = m.connection_start
    # tuples (start_time, stop_time). after sort, the first tuple is the one with the smallest start_time
    # by definition of the ordering on tuples

    x_sent = []
    x_received = []
    y = []
    x_timeout = []
    y_timeout = []
    # add normalize times to x* and request order to y
    for i, t in enumerate(data):
        start = t[0]
        stop = t[1]
        timeout_list = t[2]
        y_value = y_multi * ((i+1) / len(data))
        x_sent.append(start - data_min)
        x_received.append(stop - data_min)
        y.append(y_value)
        if len(timeout_list) > 0:
            for resent in timeout_list:
                x_timeout.append(resent - data_min)
                y_timeout.append(y_value)  # multiple timeout for the same query
    return _PlotData(x_sent, x_received, y, x_timeout, y_timeout)


def plot_scatter(m, c1='b', c2='g', c3='r', qs_label="Query sent", rr_label="Response received",
                 timeout_label="Query resent after timeout"):
    if not isinstance(m, Measurement):
        if benchmark.options.verbose:
            print("scatter plot error: measurement is not an instance of Measurement")
        return None

    data = measurement_to_plot_data(m, len(m.transfer_times))

    plt.plot(data.x_sent, data.y, label=qs_label, linestyle="none", marker="o", markersize=marker_size, c=c1)
    plt.plot(data.x_received, data.y, label=rr_label, linestyle="none", marker="o", markersize=marker_size, c=c2)
    if len(data.x_timeout) > 0:
        plt.plot(data.x_timeout, data.y_timeout, label=timeout_label, linestyle="none", marker="x", markersize=marker_size, c=c3)


def plot_cdf(m, c1='b', c2='g', qs_label="Query sent", rr_label="Response received"):
    if not isinstance(m, Measurement):
        if benchmark.options.verbose:
            print("cdf plot: measurement is not an instance of Measurement")
        return None

    data = measurement_to_plot_data(m, y_multi=100)

    x_received = data.x_received
    x_received.sort()

    x_min = 0
    x_max = data.x_sent[-1] if data.x_sent[-1] > x_received[-1] else x_received[-1]
    weigth_sent = 100*(np.zeros_like(data.x_sent) + 1. / len(data.x_sent))
    weigth_received = 100*(np.zeros_like(x_received) + 1. / len(x_received))
    h1 = plt.hist(data.x_sent, label=qs_label, range=(x_min, x_max), bins=len(data.x_sent),
                  histtype='step', cumulative=True, normed=False, weights=weigth_sent, color=c1)
    h2 = plt.hist(x_received, label=rr_label, range=(x_min, x_max), bins=len(x_received),
                  histtype='step', cumulative=True, normed=False, weights=weigth_received, color=c2)


def axes_label(title, x_name, y_name):
    plt.title(title)
    plt.xlabel(x_name)
    plt.ylabel(y_name)


def xscale(*args):
    plt.xscale(*args)


def legend(location="lower right"):
    plt.legend(loc=location)


def close_figure():
    plt.close()


def save_figure(path):
    plt.savefig(path)


if __name__ == '__main__':
    from improved_resolver import make_query_str
    from config_resolver import nsd

    benchmark.options.capture_client = False
    benchmark.options.capture_server = False

    queries = list()
    queries.append(make_query_str("test.", "DNSKEY", want_dnssec=True))
    queries.append(make_query_str("test.", "SOA", want_dnssec=True))
    queries.append(make_query_str("test.", "NS", want_dnssec=True))
    queries.append(make_query_str("pi4.test.", "A", want_dnssec=True))
    queries.append(make_query_str("test.", "MX", want_dnssec=True))
    queries.append(make_query_str("pi4.test.", "A", want_dnssec=True))
    queries.append(make_query_str("www.test.", "A", want_dnssec=True))

    m = nsd.query(queries, query_loopback=False)

    # add some timeout to test
    t = m.transfer_times

    for i, e in t.items():
        idx = len(e)-1
        v = e.pop(idx)
        v.append(e[2] + 0.001)
        e.insert(idx, v)
        t[i] = e
    m.transfer_times = t

    # Do before plot in case there is a problem with them
    m.save_measure("measure.dat")
    print("\n" + m.measure_csv_format() + m.measure_to_csv())

    # scatter plot
    print()
    plot_scatter(m)
    axes_label("Queries sent and received", "time[s]",
                           "Percentage of Query sent (response by their query value) [%]")
    legend()
    save_figure("graph_scatter.png")

    # scatter plot log x axis
    xscale('log')
    save_figure("graph_scatter_log.png")
    close_figure()

    # cdf plot
    print()
    plot_cdf(m)
    axes_label("Queries sent and received", "time[s]",
                           "Percentage of Query sent/Response received [%]")
    legend()
    save_figure("graph_cdf.png")

    # cdf plot log x axis
    xscale('log')
    save_figure("graph_cdf_log.png")

