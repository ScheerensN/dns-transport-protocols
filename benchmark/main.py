#!/usr/bin/env python3.8
import sys
import os
import traceback
import datetime
import yaml
import json
import socket
import subprocess
import time
import datetime
import math

import config as cfg
import log
from struct_various import PacketRetention, FastRetransmitType, Protocol
import file_parser as fparser
import improved_resolver as iresolver
import MPLchart as mpl
import dissect_dns

log_level_stream = log.DEBUG
log_level_file = log.DEBUG
dumps = []


def jprint(text):
    print(json.dumps(text, sort_keys=True, indent=4))


def dump_start(filter, interface=None):
    filename = "{}{}_{}.pcap".format(cfg.options.path, socket.gethostname(), "client", "pcap")
    if interface:
        cmd = ["tshark", "-i", interface, "-w", filename, filter]
    else:
        cmd = ["tshark", "-w", filename, filter]
    logger.info("Starting '{}'".format(" ".join(cmd)))
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    time.sleep(1)
    logger.debug("dump process has started: {}".format(" ".join(cmd)))
    dumps.append((process, cmd))
    return process, filename


def dump_stop(process):
    cmd = ""
    for dump in dumps:
        if dump[0] == process:
            cmd = dump[1]
            dumps.remove(dump)
            break
    logger.info("Stopping '{}'".format(" ".join(cmd)))
    time.sleep(3)

    process.terminate()
    try:
        process.wait(5)
    except TimeoutError as e:
        logger.warning(e)
        logger.warning("process is still running after 5s. The program will resume.")
    else:
        stderr_data = process.communicate()[1]
        logger.debug('process stderr output:')
        logger.debug(stderr_data)
        logger.debug("dump process is stopped: {}".format(" ".join(cmd)))


def __generate_packets(queries):
    return fparser.generate_packets(
                    queries,
                    rr_type=cfg.profile['global']['queries_format_wprof_rrtype'],
                    use_edns=cfg.profile['global']['use_edns'],
                    want_dnssec=cfg.profile['global']['want_dnssec'],
                    ednsflags=cfg.profile['global']['ednsflags'],
                    payload=cfg.profile['global']['payload'],
                    request_payload=cfg.profile['global']['request_payload'],
                    options=cfg.profile['global']['edns_options'])


def capture():
    ### load query file
    fparser.start_logger(stream_level=log_level_stream, file_level=log_level_file)
    queries_input = fparser.parse(cfg.profile['global']['queries_path'], generate_packets=False)
    # we will generate packets later

    ### Generating the DNS messages for the queries ( for each sample individually )
    logger.info("Generate DNS queries")
    queries = dict()
    # per server, per protocol, tcp: nagle + no_delay + Cork, tcp: tcp_fast_retransmit, instance
    for nameserver in cfg.profile['global']['server']:
        if len(list(nameserver.keys())) > 1:
            logger.warning("nameserver should only have one key with a correct profile")
        name = str(list(nameserver.keys())[0])
        if name in queries.keys():
            logger.warning("Overwriting {} in queries dict".format(name))

        queries[name] = dict()
        prot = Protocol.UDP.name
        if cfg.profile[prot]['enable']:
            queries[name][prot] = dict()
            for i in range(0, cfg.profile['global']['samples']):
                queries[name][prot][i] = __generate_packets(queries_input)
                #logger.debug('{} {} iter {}: {}'.format(name, prot, i, queries[name][prot][i]))

        prot = Protocol.TCP.name
        if cfg.profile[prot]['enable']:
            queries[name][prot] = dict()
            for retention in cfg.profile[prot]['packet_retention']:
                retention = retention.upper()
                if retention not in \
                        [PacketRetention.NAGLE.name, PacketRetention.CORK.name, PacketRetention.NODELAY.name]:
                    logger.warning("packet_retention: invalid value: {}".format(retention))
                    continue

                queries[name][prot][retention] = dict()
                for retransmit in cfg.profile[prot]['tcp_fast_retransmit']:
                    queries[name][prot][retention][retransmit] = dict()

                    for tfo in cfg.profile[prot]['tfo']:
                        tfo_str = "TFO" if tfo else "NO_TFO"
                        queries[name][prot][retention][retransmit][tfo_str] = dict()

                        for i in range(0, cfg.profile['global']['samples']):
                            queries[name][prot][retention][retransmit][tfo_str][i] = __generate_packets(queries_input)
                            '''
                            logger.debug('{} {} {} {} {} iter {}: {}'.format(name, prot, retention, retransmit, tfo_str, i,
                                                                          queries[name][prot][retention][retransmit][tfo_str][i]))
                            '''

        prot = Protocol.TLS.name
        if cfg.profile[prot]['enable']:
            queries[name][prot] = dict()
            for retention in cfg.profile[prot]['packet_retention']:
                retention = retention.upper()
                if retention not in \
                        [PacketRetention.NAGLE.name, PacketRetention.CORK.name, PacketRetention.NODELAY.name]:
                    logger.warning("packet_retention: invalid value: {}".format(retention))
                    continue
                if len(cfg.profile[prot]['packet_retention']) > 1 and retention == 'unchanged':
                    logger.warning("packet_retention: unchanged should not be used with other values")
                    continue

                queries[name][prot][retention] = dict()
                for retransmit in cfg.profile[prot]['tcp_fast_retransmit']:
                    queries[name][prot][retention][retransmit] = dict()

                    for tfo in cfg.profile[prot]['tfo']:
                        tfo_str = "TFO" if tfo else "NO_TFO"
                        queries[name][prot][retention][retransmit][tfo_str] = dict()

                        if 'ssl_version' not in cfg.profile[prot]:
                            cfg.profile[prot]['ssl_version'] = ['TLS_Default']
                        for ssl_version_str in cfg.profile[prot]['ssl_version']:
                            if not (ssl_version_str == 'TLSv1' or ssl_version_str == 'TLSv1_1' or ssl_version_str == 'TLSv1_2' or ssl_version_str == 'TLSv1_3'):
                                ssl_version_str = 'TLS_Default'
                            queries[name][prot][retention][retransmit][tfo_str][ssl_version_str] = dict()

                            for resumption in cfg.profile[prot]['tls_resumption']:
                                resumption_str = 'No_Resumption'
                                if resumption:
                                    resumption_str = 'Resumption'
                                queries[name][prot][retention][retransmit][tfo_str][ssl_version_str][resumption_str] = dict()

                                for i in range(0, cfg.profile['global']['samples']):
                                    queries[name][prot][retention][retransmit][tfo_str][ssl_version_str][resumption_str][i] = __generate_packets(queries_input)
                                    '''
                                    logger.debug('{} {} {} {} {} {} {} iter {}: {}'.format(name, prot, retention,
                                                retransmit, tfo_str, ssl_version_str, resumption_str, i,
                                                queries[name][prot][retention][retransmit][tfo_str][ssl_version_str][resumption_str][i]))
                                    '''

        prot = Protocol.HTTPS.name
        if cfg.profile[prot]['enable']:
            queries[name][prot] = dict()
            for retention in cfg.profile[prot]['packet_retention']:
                retention = retention.upper()
                if retention not in \
                        [PacketRetention.NAGLE.name, PacketRetention.CORK.name, PacketRetention.NODELAY.name]:
                    logger.warning("packet_retention: invalid value: {}".format(retention))
                    continue
                if len(cfg.profile[prot]['packet_retention']) > 1 and retention == 'unchanged':
                    logger.warning("packet_retention: unchanged should not be used with other values")
                    continue

                queries[name][prot][retention] = dict()
                for retransmit in cfg.profile[prot]['tcp_fast_retransmit']:
                    queries[name][prot][retention][retransmit] = dict()

                    for i in range(0, cfg.profile['global']['samples']):
                        queries[name][prot][retention][retransmit][i] = __generate_packets(queries_input)
                        '''
                        logger.debug('{} {} {} {} iter {}: {}'.format(name, prot, retention, retransmit, i,
                                                                      queries[name][prot][retention][retransmit][i]))
                        '''

    ### Measurements
    # Start capture
    logger.info("Starting Capture")

    experiment_conn = dict()
    index = list(cfg.profile['global']['server'][0].keys())[0]

    host = cfg.profile['global']['server'][0][index]['host']
    filter_pcap = "host {}".format(cfg.profile['global']['server'][0][index]['host'])
    if host == '8.8.8.8' or host == '8.8.4.4':
        # Bypass usual filtering with google because doh over IP doesn't work for dns.google. Using IP gives a 404 error
        filter_pcap = "host 8.8.8.8 or host 8.8.4.4"

    (dump_process, dump_filename) = dump_start(filter_pcap, interface=cfg.profile['global']['capture_interface'])
    yaml_dump = dict()
    yaml_dump['pcap'] = dump_filename

    for nameserver in cfg.profile['global']['server']:
        name = str(list(nameserver.keys())[0])
        rslvr = iresolver.ImprovedAggResolver(cfg.profile, name, stream_log_level=log_level_stream,
                                              file_log_level=log_level_file, file_log_path=cfg.options.log_path)
        experiment_conn = dict()

        # Do experiment stuff
        stacktrace = None
        try:
            if cfg.profile[Protocol.UDP.name]['enable']:
                experiment_conn[Protocol.UDP.name] = rslvr.query_agg(queries, Protocol.UDP)
        except Exception as e:
            stacktrace = sys.exc_info()
        finally:
            if stacktrace:
                logger.error(traceback.print_exception(*stacktrace))
            del stacktrace

        stacktrace = None
        try:
            if cfg.profile[Protocol.TCP.name]['enable']:
                experiment_conn[Protocol.TCP.name] = rslvr.query_agg(queries, Protocol.TCP)
        except Exception as e:
            stacktrace = sys.exc_info()
        finally:
            if stacktrace:
                logger.error(traceback.print_exception(*stacktrace))
            del stacktrace

        stacktrace = None
        try:
            if cfg.profile[Protocol.TLS.name]['enable']:
                experiment_conn[Protocol.TLS.name] = rslvr.query_agg(queries, Protocol.TLS)
        except Exception as e:
            stacktrace = sys.exc_info()
        finally:
            if stacktrace:
                logger.error(traceback.print_exception(*stacktrace))
            del stacktrace

        stacktrace = None
        try:
            if cfg.profile[Protocol.HTTPS.name]['enable']:
                experiment_conn[Protocol.HTTPS.name] = rslvr.query_agg(queries, Protocol.HTTPS)
        except Exception as e:
            stacktrace = sys.exc_info()
        finally:
            if stacktrace:
                logger.error(traceback.print_exception(*stacktrace))
            del stacktrace


        yaml_dump[name] = experiment_conn

    dump_stop(dump_process)

    with open('{}capture.yaml'.format(cfg.options.path), 'w') as f:
        f.write(yaml.dump(yaml_dump))
    with open('{}profile.yaml'.format(cfg.options.path), 'w') as f:
        f.write(yaml.dump(cfg.profile))
    # Finished


def flatten_queries(q, queries):
    for node in queries:
        q.append(node)
        flatten_queries(q, node.next)


def read_tstat_result(path, prefix_length):
    header = None
    data = None
    with open(path, 'r') as f:
        logger.debug("reading {}".format(path))
        data = f.readlines()
        data[0] = data[0][prefix_length:]
        data[0] = data[0].strip('\n')
        header = data[0].split(" ")
        rows = list()
        for row in data[1:]:
            row = row.strip('\n')
            row = row.split(" ")
            d = dict()
            for index in range(len(row)):
                d[header[index]] = row[index]
            rows.append(d)
        data = rows

    return header, data


def analyse():
    ## Loading yaml profile for the specified measure. Allow to check which protocol and options were used.
    logger.info("Reading data from the experiment")
    try:
        with open('{}profile.yaml'.format(cfg.options.path), 'r') as f:
            cfg.profile = yaml.load(f, Loader=yaml.SafeLoader)
        with open('{}capture.yaml'.format(cfg.options.path), 'r') as f:
            cfg.analysis = dict()
            cfg.analysis['capture_info'] = yaml.load(f, Loader=yaml.SafeLoader)
            cfg.analysis['data'] = dict()
    except FileNotFoundError as e:
        logger.critical("profile.yaml or capture.yaml not found at: {}".format(cfg.options.path))
        exit()

    ## run tstat on data
    logger.info("Generating stats")
    os.makedirs("{}tstat".format(cfg.options.path), exist_ok=True)
    cmd = ["tstat", "-s", "{}tstat".format(cfg.options.path), "-N", "./tstat/private.conf", '-T',
           './tstat/runtime.conf', cfg.analysis['capture_info']['pcap']]
    logger.info("Starting '{}'".format(" ".join(cmd)))
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    stdout_data, stderr_data = process.communicate()# wait for it
    logger.debug("tstat stdout:\n" + stdout_data)
    if stderr_data != "":
        logger.error("tstat stderr:\n" + stderr_data)
    logger.info("Loading stats")
    for r, directory, f in os.walk("{}tstat".format(cfg.options.path)):
        # only interrested in root dir contents
        break
    directory.sort()  # From experimentation, directory is NOT sorted
    if len(directory) > 1:
        logger.warning("Multiple directories in {}. Using the most recent ({}).".format("{}tstat".format(cfg.options.path), directory[-1]))

    logger.info("Parsing stats")
    cfg.analysis['data']['log_udp_complete_headers'], cfg.analysis['data']['log_udp_complete'] = \
        read_tstat_result("{}tstat/{}/log_udp_complete".format(cfg.options.path, directory[-1]), 1)
    cfg.analysis['data']['log_tcp_complete_headers'], cfg.analysis['data']['log_tcp_complete'] = \
        read_tstat_result("{}tstat/{}/log_tcp_complete".format(cfg.options.path, directory[-1]), 4)
    cfg.analysis['data']['log_tcp_nocomplete_headers'], cfg.analysis['data']['log_tcp_nocomplete'] = \
        read_tstat_result("{}tstat/{}/log_tcp_nocomplete".format(cfg.options.path, directory[-1]), 4)

    path = '{}analysis.yaml'.format(cfg.options.path)
    with open(path, 'w') as f:
        f.write(yaml.dump(cfg.analysis))
    logger.info("Saved analysis in {}".format(path))


def graph():
    logger.info("Reading analysis data")
    try:
        with open('{}analysis.yaml'.format(cfg.options.path), 'r') as f:
            cfg.analysis = yaml.load(f, Loader=yaml.SafeLoader)
    except FileNotFoundError as e:
        logger.critical("analysis.yaml not found at: {}".format('{}'.format(cfg.options.path)))
        exit()

    # map src port to experiment
    logger.debug("Setting up 'src_port => experiment' mapping")
    keys = list(cfg.analysis['capture_info'].keys())
    if len(keys) != 2:
        logger.critical("cfg.analysis['capture_info'] has {} keys instead of two".format(len(keys)))
        exit()
    key = keys[0] if keys[0] != 'pcap' else keys[1]

    cfg.graph = dict()
    cfg.graph['experiment_map'] = dict()
    cfg.graph['experiment_map']['UDP'] = dict()
    cfg.graph['experiment_map']['TCP'] = dict()

    for prot in list(cfg.analysis['capture_info'][key].keys()):
        if prot == 'UDP':
            logger.debug("    Mapping UDP")
            for iteration in list(cfg.analysis['capture_info'][key][prot].keys()):
                src_port = cfg.analysis['capture_info'][key][prot][iteration]['src_port']
                cfg.graph['experiment_map']['UDP'][src_port] = dict()
                cfg.graph['experiment_map']['UDP'][src_port]['experiment'] = ""
                cfg.graph['experiment_map']['UDP'][src_port]['iteration'] = iteration
        elif prot == 'TCP':
            logger.debug("    Mapping TCP")
            for retention in list(cfg.analysis['capture_info'][key][prot].keys()):
                for retransmit_type in list(cfg.analysis['capture_info'][key][prot][retention].keys()):
                    for tfo in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type].keys()):
                        for iteration in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type][tfo].keys()):
                            src_port = cfg.analysis['capture_info'][key][prot][retention][retransmit_type][tfo][iteration]['src_port']
                            cfg.graph['experiment_map']['TCP'][src_port] = dict()
                            cfg.graph['experiment_map']['TCP'][src_port]['protocol'] = prot
                            cfg.graph['experiment_map']['TCP'][src_port]['experiment'] = "{} {} {}".format(retention, retransmit_type, tfo)
                            cfg.graph['experiment_map']['TCP'][src_port]['iteration'] = iteration
        elif prot == 'TLS':
            logger.debug("    Mapping TLS")
            for retention in list(cfg.analysis['capture_info'][key][prot].keys()):
                for retransmit_type in list(cfg.analysis['capture_info'][key][prot][retention].keys()):
                    for tfo in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type].keys()):
                        for ssl_version in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type][tfo].keys()):
                            for ssl_resumption in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type][tfo][ssl_version].keys()):
                                for iteration in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type][tfo][ssl_version][ssl_resumption].keys()):
                                    src_port = cfg.analysis['capture_info'][key][prot][retention][retransmit_type][tfo][ssl_version][ssl_resumption][iteration]['src_port']
                                    cfg.graph['experiment_map']['TCP'][src_port] = dict()
                                    cfg.graph['experiment_map']['TCP'][src_port]['protocol'] = prot
                                    cfg.graph['experiment_map']['TCP'][src_port]['experiment'] = "{} {} {} {} {}".format(retention, retransmit_type, tfo, ssl_version, ssl_resumption)
                                    cfg.graph['experiment_map']['TCP'][src_port]['iteration'] = iteration
        elif prot == 'HTTPS':
            logger.debug("    Mapping HTTPS")
            for retention in list(cfg.analysis['capture_info'][key][prot].keys()):
                for retransmit_type in list(cfg.analysis['capture_info'][key][prot][retention].keys()):
                    for iteration in list(cfg.analysis['capture_info'][key][prot][retention][retransmit_type].keys()):
                        src_port = cfg.analysis['capture_info'][key][prot][retention][retransmit_type][iteration]['src_port']
                        cfg.graph['experiment_map']['TCP'][src_port] = dict()
                        cfg.graph['experiment_map']['TCP'][src_port]['protocol'] = prot
                        cfg.graph['experiment_map']['TCP'][src_port]['experiment'] = "{} {}".format(retention, retransmit_type)
                        cfg.graph['experiment_map']['TCP'][src_port]['iteration'] = iteration
        else:
            logger.warning("Unrecognized protocol: '{}'".format(prot))

    # sorting flows
    logger.info("Sorting flows by experiments")

    cfg.graph['experiment_chart_data'] = dict()
    cfg.graph['experiment_chart_data']['UDP'] = dict()
    cfg.graph['experiment_chart_data']['TCP'] = dict()
    cfg.graph['experiment_chart_data']['TLS'] = dict()
    cfg.graph['experiment_chart_data']['HTTPS'] = dict()

    logger.info("Sorting udp_complete")
    for flow in cfg.analysis['data']['log_udp_complete']:
        try:
            experiment = cfg.graph['experiment_map']['UDP'][int(flow['c_port:2'])]
        except KeyError as e:
            logger.warning("KeyError: cfg.graph['experiment_map']['UDP'][{}]".format(int(flow['c_port:2'])))
            # Skip
            continue

        # init experiment as needed
        if experiment['experiment'] not in \
                list(cfg.graph['experiment_chart_data']['UDP'].keys()):
            cfg.graph['experiment_chart_data']['UDP'][experiment['experiment']] = dict()

        cfg.graph['experiment_chart_data']['UDP'][experiment['experiment']][experiment['iteration']] = flow

    logger.info("Sorting tcp_complete")
    for flow in cfg.analysis['data']['log_tcp_complete']:
        try:
            experiment = cfg.graph['experiment_map']['TCP'][int(flow['c_port:2'])]
        except KeyError as e:
            logger.warning("Skipping client_port_flow {}. Might happen if a TLS flow failed to found a common protocol.".format(int(flow['c_port:2'])))
            # Skip
            continue

        # init experiment as needed
        if experiment['experiment'] not in \
                list(cfg.graph['experiment_chart_data'][experiment['protocol']].keys()):
            cfg.graph['experiment_chart_data'][experiment['protocol']][experiment['experiment']] = dict()
        cfg.graph['experiment_chart_data'][experiment['protocol']][experiment['experiment']][experiment['iteration']] = flow

    for prot in list(cfg.graph['experiment_chart_data'].keys()):
        logger.debug("Sorted {} {} experiments".format(len(cfg.graph['experiment_chart_data'][prot]), prot))

    mpl.boxplot('c_durat:4', 'durat:31', 'Exchange duration', 'ms', udp_mul=1000, tcp_mul=1,
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map': ['UDP', 'TCP', 'TLS', 'HTTPS'], 'yscale':'log'},
                         {'map': ['UDP', 'TCP', 'TLS', 'HTTPS'], 'yscale': 'linear'},
                         {'map': ['UDP'], 'yscale':'linear'},
                         {'map': ['TCP'], 'yscale':'linear'},
                         {'map': ['TLS'], 'yscale':'linear'},
                         {'map': ['HTTPS'], 'yscale': 'linear'},
                         {'map': ['UDP', 'TCP'], 'yscale':'linear'},
                         {'map': ['TCP', 'TLS'], 'yscale':'linear'},
                         {'map': ['TCP', 'TLS', 'HTTPS'], 'yscale':'linear'},
                         {'map': ['TLS', 'HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 'c_pkts_all:3', '# packets sent', 'count',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map': ['TCP', 'TLS'], 'yscale':'linear'},
                         {'map': ['TCP'], 'yscale':'linear'},
                         {'map': ['TLS'], 'yscale':'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 's_pkts_all:17', '# packets received', 'count',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map':['TCP', 'TLS'], 'yscale':'linear'},
                         {'map':['TCP'], 'yscale':'linear'},
                         {'map':['TLS'], 'yscale':'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 'c_ack_cnt_p:6', '# Pure ACK sent', 'count',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map':['TCP', 'TLS'], 'yscale':'linear'},
                         {'map':['TCP'], 'yscale':'linear'},
                         {'map':['TLS'], 'yscale':'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 's_ack_cnt_p:20', '# Pure ACK received', 'count',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map':['TCP', 'TLS'], 'yscale':'linear'},
                         {'map':['TCP'], 'yscale':'linear'},
                         {'map':['TLS'], 'yscale':'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 'c_bytes_retx:11', 'Bytes retrnasmitted by the client', 'bytes',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map': ['TCP', 'TLS'], 'yscale': 'linear'},
                         {'map': ['TCP'], 'yscale': 'linear'},
                         {'map': ['TLS'], 'yscale': 'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 's_bytes_retx:25', 'Bytes retrnasmitted by the server', 'bytes',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map': ['TCP', 'TLS'], 'yscale': 'linear'},
                         {'map': ['TCP'], 'yscale': 'linear'},
                         {'map': ['TLS'], 'yscale': 'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 'c_pkts_ooo:12', 'Out of sequence packets from the client', 'count',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map': ['TCP', 'TLS'], 'yscale': 'linear'},
                         {'map': ['TCP'], 'yscale': 'linear'},
                         {'map': ['TLS'], 'yscale': 'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])

    mpl.boxplot(None, 's_pkts_ooo:26', 'Out of sequence packet from the server', 'count',
                path="{}{}".format(cfg.options.path, "charts/"),
                mapping=[{'map': ['TCP', 'TLS'], 'yscale': 'linear'},
                         {'map': ['TCP'], 'yscale': 'linear'},
                         {'map': ['TLS'], 'yscale': 'linear'},
                         {'map': ['HTTPS'], 'yscale':'linear'}])


if __name__ == '__main__':
    start_time = time.time()
    now = datetime.datetime.now()

    ### Parsing command line options
    parser = cfg.parse_command_line(now)
    (cfg.options, cfg.args) = parser.parse_args()

    if not (cfg.options.capture or cfg.options.analyse or cfg.options.graph):
        # Default behaviour is to do them all if none of them are used. Otherwise the software would do nothing ^^.
        cfg.options.capture = True
        cfg.options.analyse = True
        cfg.options.graph = True

    ### Start logging
    if not os.path.exists(cfg.options.log_path):
        log_path = cfg.options.log_path.split('/')
        if '.' in log_path[-1]:
            log_path = "/".join(log_path[:-1]) # remove last one
        else:
            log_path = "/".join(log_path)
        os.makedirs(log_path, exist_ok=True)
        log_path = None

    logger = log.get_logger(__name__, stream_level=log_level_stream, log_path=cfg.options.log_path,
                            file_level=log_level_file)

    logger.info(" ".join(sys.argv[:]))
    # Adding file_handler to modules since log_path is not known when imported
    log.add_file_handler(mpl.logger, cfg.options.log_path, level=mpl.log_level_file,
                         file_format='%(asctime)s;%(name)s:%(lineno)d;%(levelname)s;%(message)s')

    logger.debug("validating arguments")
    if not os.path.exists(cfg.options.path) and cfg.options.capture:
        os.makedirs(cfg.options.path)
        logger.warning("created path: {}".format(cfg.options.path))
    elif not os.path.exists(cfg.options.path):
        logger.error("--experiment-path / -e: {} does not exists".format(cfg.options.path))
    elif not os.path.isdir(cfg.options.path):
        logger.error("{} is not a valid directory for --experiment-path / -e".format(cfg.options.path))
        exit(1)

    if cfg.options.path[-1] != '/':
        logger.debug("Adding '/' at the end of {} '{}'".format("cfg.options.path", cfg.options.path))
        cfg.options.path += '/'

    try:
        path = "{}/test_rights".format(cfg.options.path)
        with open(path, "w") as f:
            f.write("test")
    except PermissionError as e:
        logger.error(e)
        exit(2)
    else:
        os.remove(path)

    ### Loading yaml profile
    try:
        with open(cfg.options.profile, 'r') as f:
            cfg.profile = yaml.load(f, Loader=yaml.SafeLoader)
    except FileNotFoundError as e:
        logger.critical("Profile not found at: {}".format(cfg.options.profile))
        exit()

    if cfg.options.debug:
        print("cfg options: " + str(cfg.options))
        print("cfg args:    " + str(cfg.args))
        print("cfg profile: " + str(cfg.profile))

    ### Start executing the three main function of the software

    if cfg.options.capture:
        logger.info("Starting experiments")
        capture()

    if cfg.options.analyse:
        logger.info("Analysing results")
        analyse()

    if cfg.options.graph:
        logger.info("Plotting graphs")
        graph()

    execution_time = time.time() - start_time
    hours, remainder = divmod(execution_time, 60*60)
    minutes, seconds = divmod(remainder, 60)
    seconds = math.floor(seconds)

    logger.info("Finished in {}h{}m{}s\n\n".format(int(hours), int(minutes), int(seconds)))

