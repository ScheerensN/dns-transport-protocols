from enum import Enum
from collections import namedtuple
import dns


class Protocol(Enum):
    UDP = 0
    TCP = 1
    TLS = 2
    HTTPS = 3


class PacketRetention(Enum):
    NODELAY = 0
    NAGLE = 1
    CORK = 2


class FastRetransmitType(Enum):
    NOER = 0
    ER = 1
    ER_DELAYED = 2
    ER_AND_TLP_DELAYED = 3
    TLP = 4


class JSONType(Enum):
    TIMINGS = 0
    WPROF = 1
    DEPENDENCY = 2

Server = namedtuple('Server', 'name udp tcp tls')

MessageNode = namedtuple('MessageNode', 'offset name qtype next')
QueryNodeTuple = namedtuple('QueryNode', 'offset query next')


class QueryNode:

    def __init__(self, offset=0, query=None, next=[]):
        self.offset = offset
        self.query = query
        self.next = next

    # To allow heapq to order it as the query field is unorderable
    def __lt__(self, other):
        if self.offset != other.offset:
            return self.offset < other.offset
        else:
            return self.query.id < other.query.id

    def __gt__(self, other):
        if self.offset != other.offset:
            return self.offset > other.offset
        else:
            return self.query.id > other.query.id

    def __str__(self):
        return "<QueryNode " + str(self.offset) + " " + str(self.query) + " " + str(self.next) + ">"


def query_to_message_node(message_list, query_list):
    for q in query_list:
        offset = q.offset
        name = q.query.question[0].name.to_text()
        qtype = dns.rdatatype.to_text(q.query.question[0].rdtype)
        next_m = []
        query_to_message_node(next_m, q.next)
        message_list.append(MessageNode(offset=offset, name=name, qtype=qtype, next=next_m))
    # Finished

