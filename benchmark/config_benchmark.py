
# Default values
retry_timeout_default = 5  # resend after 5 sec
retry_number_default = 3  # retry three times

capture_path_default = "pcap"
capture_extension = "pcap"

capture_self = False
capture_interface_self = "eth0"

capture_dist = False
capture_interface_dist = "eth0"
capture_ssh_host_dist = "pi4"
capture_ssh_host_user = "pi"
capture_remove_file_dist = True

verbose_output = True

with_dnssec = False
with_edns = False
with_ednsflags = None
with_payload = None
with_request_payload = None

substitute_tld = None

stats = False

wprof_rdata = "A"

