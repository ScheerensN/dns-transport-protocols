#!/usr/bin/env bash
for (( ; ; ))
do
   echo "STATUS:"
   echo "`ps aux | grep experiment_run | grep -v grep`"
   echo "`ps aux | grep B-Muspelheim-XPS13U_client.pcap | grep -v grep | grep -v dumpcap`"
   echo ""
   echo "CURRENT: "
   cat current_test.log | tail -n 5
   echo "TAIL -n 5:"
   tail -n 25 2020.log
   echo ""
   sleep 1m
done