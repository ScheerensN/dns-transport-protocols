#!/usr/bin/env python3.5
from config_resolver import nsd
from improved_resolver import make_query_str

from struct_various import Protocol, QueryNode
import config_benchmark
import plot

queries = list()
queries.append(QueryNode(0.0, make_query_str("test", "DNSKEY", want_dnssec=True), [
    QueryNode(0.0, make_query_str("pi4.test.", "A", want_dnssec=True), []),
    QueryNode(0.0, make_query_str("pi4.test.", "A", want_dnssec=True), [
        QueryNode(0.0, make_query_str("pi4.test.", "A", want_dnssec=True), [])])
    ]))
queries.append(QueryNode(0.0, make_query_str("test.", "SOA", want_dnssec=True), []))
queries.append(QueryNode(0.0, make_query_str("test.", "NS", want_dnssec=True), []))
queries.append(QueryNode(0.0, make_query_str("pi4.test.", "A", want_dnssec=True), []))
queries.append(QueryNode(0.0, make_query_str("test.", "MX", want_dnssec=True), []))
queries.append(QueryNode(0.0, make_query_str("pi4.test.", "A", want_dnssec=True), []))
queries.append(QueryNode(0.0, make_query_str("www.test.", "A", want_dnssec=True), []))

config_benchmark.substitute_tld = "test"
#queries = json_file.load("../dependencytree/amazon.com.json")


m = nsd.query(queries, dest=nsd.nameservers[0], query_loopback=False, proto=Protocol.UDP, tfo=False)
m = nsd.query(queries, dest=nsd.nameservers[0], query_loopback=False, proto=Protocol.TCP, tfo=True)

#print(m.measure_to_csv())

# scatter plot
plot.plot_scatter(m)
plot.axes_label("Queries sent and received burst", "time[s]",
                       "Percentage of Query sent (response by their query value) [%]")
plot.legend()
plot.save_figure("graph_scatter_burst.png")

m = nsd.query(queries, dest=nsd.nameservers[0], query_loopback=False, proto=Protocol.TLS, tfo=True, dest_port=5854)

#print(m.measure_to_csv())
# scatter plot
plot.plot_scatter(m, c1='c', c2='m')
plot.axes_label("Queries sent and received dependency", "time[s]",
                       "Percentage of Query sent (response by their query value) [%]")
plot.legend()
plot.save_figure("graph_scatter_dep.png")

"""
# add some timeout to test
t = m.transfer_times
for i, e in t.items():
    print(e)
    idx = len(e)-1
    v = e.pop(idx)
    print(e[2])
    v.append(e[2] + 0.001)
    e.insert(idx, v)
    t[i] = e
m.transfer_times = t


# scatter plot
plot.plot_scatter(m)
plot.axes_label("Queries sent and received", "time[s]",
                       "Percentage of Query sent (response by their query value) [%]")
plot.legend()
plot.save_figure("graph_scatter.png")
# scatter plot log x axis
plot.xscale('log')
plot.save_figure("graph_scatter_log.png")
plot.close_figure()
# cdf plot
plot.plot_scatter(m)
plot.axes_label("Queries sent and received", "time[s]",
                       "Percentage of Query sent (response by their query value) [%]")
plot.legend()
plot.save_figure("graph_scatter.png")
# cdf plot log x axis
plot.xscale('log')
plot.save_figure("graph_scatter_log.png")
plot.close_figure()

# cdf plot
print()
plot.plot_cdf(m)
plot.axes_label("Queries sent and received", "time[s]",
           "Percentage of Query sent/Response received [%]")
plot.legend()
plot.save_figure("graph_cdf.png")

# cdf plot log x axis
plot.xscale('log')
plot.save_figure("graph_cdf_log.png")

"""

