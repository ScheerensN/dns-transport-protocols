#!/usr/bin/env bash
date
echo "Reset TC"
sudo tc qdisc del dev enx00145c97c1ed root
sudo tc qdisc show dev enx00145c97c1ed
echo ''

#date
#echo 'python3.8 main.py --log ./Measure/cork_opti/1/event.log --experiment-path ./Measure/cork_opti/1/ -p ./profile_cork_opti1.yaml'
#python3.8 main.py --log ./cork_opti/1/event.log --experiment-path ./cork_opti/1/ -p ./profile_cork_opti1.yaml
#echo ''
#
#date
#echo 'python3.8 main.py --log ./Measure/cork_opti/2/event.log --experiment-path ./Measure/cork_opti/2/ -p ./profile_cork_opti2.yaml'
#python3.8 main.py --log ./cork_opti/2/event.log --experiment-path ./cork_opti/2/ -p ./profile_cork_opti2.yaml
#echo ''

#date
#echo 'python3.8 main.py --log ./Measure/baseline/event.log --experiment-path ./Measure/baseline/ -p ./profile_baseline.yaml' > current.log
#echo 'python3.8 main.py --log ./Measure/baseline/event.log --experiment-path ./Measure/baseline/ -p ./profile_baseline.yaml'
#python3.8 main.py --log ./Measure/baseline/event.log --experiment-path ./Measure/baseline/ -p ./profile_baseline.yaml
#echo ''

date
echo 'python3.8 main.py --log ./Measure/baseline2/event.log --experiment-path ./Measure/baseline2/ -p ./profile_baseline2.yaml' > current.log
echo 'python3.8 main.py --log ./Measure/baseline2/event.log --experiment-path ./Measure/baseline2/ -p ./profile_baseline2.yaml'
python3.8 main.py -g --log ./Measure/baseline2/event.log --experiment-path ./Measure/baseline2/ -p ./profile_baseline2.yaml
echo ''

#date
#echo 'TC: Set loss to 1%'
#sudo tc qdisc add dev enx00145c97c1ed root netem loss 1%
#sudo tc qdisc show dev enx00145c97c1ed
#echo ''
#
#date
#echo 'python3.8 main.py --log ./Measure/loss_1/event.log --experiment-path ./Measure/loss_1/ -p ./profile_loss.yaml' > current.log
#echo 'python3.8 main.py --log ./Measure/loss_1/event.log --experiment-path ./Measure/loss_1/ -p ./profile_loss.yaml'
#python3.8 main.py --log ./Measure/loss_1/event.log --experiment-path ./Measure/loss_1/ -p ./profile_loss.yaml
#echo ''

#date
#echo 'python3.8 main.py --log ./baseline_cork/event.log --experiment-path ./baseline_cork/ -p ./profile_baseline_https_cork.yaml' > current.log
#echo 'python3.8 main.py --log ./baseline_cork/event.log --experiment-path ./baseline_cork/ -p ./profile_baseline_https_cork.yaml'
#python3.8 main.py --log ./baseline_cork/event.log --experiment-path ./baseline_cork/ -p ./profile_baseline_https_cork.yaml
#echo ''

#date
#echo 'TC: Set delay to 400ms'
#sudo tc qdisc del dev enx00145c97c1ed root
#sudo tc qdisc add dev enx00145c97c1ed root netem delay 400ms
#sudo tc qdisc show dev enx00145c97c1ed
#echo ''
#
#date
#echo 'python3.8 main.py --log ./Measure/lag_400/event.log --experiment-path ./Measure/lag_400/ -p ./profile_lag.yaml' > current.log
#echo 'python3.8 main.py --log ./Measure/lag_400/event.log --experiment-path ./Measure/lag_400/ -p ./profile_lag.yaml'
#python3.8 main.py --log ./Measure/lag_400/event.log --experiment-path ./Measure/lag_400/ -p ./profile_lag.yaml
#echo ''

#date
#echo 'TC: Set loss to 0.5%'
#sudo tc qdisc del dev enx00145c97c1ed root
#sudo tc qdisc add dev enx00145c97c1ed root netem loss 0.5%
#sudo tc qdisc show dev enx00145c97c1ed
#echo ''
#
#date
#echo 'python3.8 main.py --log ./loss_0.5/event.log --experiment-path ./loss_0.5/ -p ./profile_loss.yaml' > current.log
#echo 'python3.8 main.py --log ./loss_0.5/event.log --experiment-path ./loss_0.5/ -p ./profile_loss.yaml'
#python3.8 main.py --log ./loss_0.5/event.log --experiment-path ./loss_0.5/ -p ./profile_loss.yaml
#echo ''
#
#date
#echo 'TC: Set delay to 200ms'
#sudo tc qdisc del dev enx00145c97c1ed root
#sudo tc qdisc add dev enx00145c97c1ed root netem delay 200ms
#sudo tc qdisc show dev enx00145c97c1ed
#echo ''
#
#date
#echo 'python3.8 main.py --log ./lag_200/event.log --experiment-path ./lag_200/ -p ./profile_lag.yaml' > current.log
#echo 'python3.8 main.py --log ./lag_200/event.log --experiment-path ./lag_200/ -p ./profile_lag.yaml'
#python3.8 main.py --log ./lag_200/event.log --experiment-path ./lag_200/ -p ./profile_lag.yaml
#echo ''

date
echo 'TC: Reset'
sudo tc qdisc del dev enx00145c97c1ed root

echo 'Finished'  > current.log
echo 'Finished'
date
