from improved_resolver import ImprovedResolver
from collections import namedtuple

# split from config_benchmark to avoid circular import

### Server's resolver config ###

# name and rdtype used to check whether the DNS server is up and functioning correctly.
# test_rrset is used to validate the response.
test_name = "test."
test_rdtype = "DNSKEY"
test_rrset = "test. 1800 IN DNSKEY 256 3 7 AwEAAdj/+4M1gIJm511v12E1vQxFARvi eECtAX4wtz6GLcbuhQtpiS8OXVtgq/87 " \
             "amYehNfK6uYh1udpyRK5/Wwe3ytGBUji rxVxwDNVIkbq4NPlw4Dqd3VfdGOYASIK WlsmrlGq3lZ3sJu9njHQsZ/0CyqViC1a " \
             "08wgcAp8wQNMmPr9LCaknCGGSbr7OOo/ bVcxdWHCZnKx5Bejgk8uK0TqPSUXy0GI bML+N123ilOrOzfor5EgFuVKZI9h9Yp3 " \
             "+1S4DlyRBi+hRxzHp/BLbCW1V8ptEga0 cAaz42JSFH36RfegB8w598Wky27o21Nc QyOlkqu4olwv2Ct232sBNvNS9Ak=\n" \
             "test. 1800 IN DNSKEY 257 3 7 AwEAAbgHRhP6zkH8XkVugQOlzUSRqEKF 0mUzmT6/IvKbu5qIDwgobMPeZSgdY2Ew " \
             "aGBRl43Y6d8F3N4e8Q1DEfadXDDkHNfp vFBS3aFUQQ26wxlSCVgTwblQYdhWICbc Nykd2PkRkkXAEf4typd16SqvAB2y7HOR " \
             "CBoR7jOMP9cS/Sbkif3EwSvcvEXeedQk 4fxTlpxRULHZdRvkMOCLO8OgDmeqfepZ Mv2RxKd9N31OasqhVAaQozmREBS8bf9U " \
             "OL98cC8YW3ikLchsuua31QG7NrQg0Nvg rxIIVTaky3wLDTvGlEYMTSJKvj3TIqZA zrD+dAuaaToSXqt8IvuZr5QFOEM="

# Name, Port UDP + TCP, Port TLS, nnnn_status whether it is active.
Server = namedtuple('Server', 'name standard_port tls_port udp_status tcp_status tls_status')
servers = []

# NSD
nsd = ImprovedResolver()
nsd.nameservers = ["192.168.2.236", "fd0e:857:4a1a:1234:e633:373c:17e3:1086"]
nsd.port = 5054
# TLS on default TCP port as changing the value in the config file to anything else doesn't work
servers.append(Server("NSD", nsd, nsd, True, True, True))

# Unbound
unbound = ImprovedResolver()
unbound.nameservers = ["192.168.2.236", "fd0e:857:4a1a:1234:e633:373c:17e3:1086"]
unbound.port = 5055
# TLS on default TCP port as changing the value in the config file to anything else doesn't work
servers.append(Server("Unbound", nsd, nsd, True, True, True))

# Proxy NSD - Don't support IPv6
proxy_nsd = ImprovedResolver()
proxy_nsd.nameservers = ["192.168.2.236"]
proxy_nsd.port = 5056

proxy_nsd_tls = ImprovedResolver()
proxy_nsd_tls.nameservers = ["192.168.2.236"]
proxy_nsd_tls.port = 5856

servers.append(Server("Proxy NSD", proxy_nsd, proxy_nsd_tls, False, True, True))

# Proxy Unbound - Don't support IPv6
proxy_unbound = ImprovedResolver()
proxy_unbound.nameservers = ["192.168.2.236"]
proxy_unbound.port = 5057

proxy_unbound_tls = ImprovedResolver()
proxy_unbound_tls.nameservers = ["192.168.2.236"]
proxy_unbound_tls.port = 5857

# TLS certificate

servers.append(Server("Proxy Unbound", proxy_unbound, proxy_unbound_tls, False, True, True))