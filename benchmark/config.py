import optparse
from optparse import OptionParser, OptionGroup

options = None
args = None
profile = None
analysis = None
graph = None


def parse_command_line(now):
    usage = "\n%prog [options] -f input_file"

    description = "This benchmark tests the performance of DNS over different protocols " \
                  "and allows to tweak protocol specific options"

    parser = OptionParser(description=description)

    parser.add_option("-q", "--quiet", action='store_false', dest="verbose", default=True,
                      help="Show less information.")
    parser.add_option("-d", "--debug", action='store_true', dest="debug", default=False,
                      help=optparse.SUPPRESS_HELP)
    parser.add_option("-e", "--experiment-path", action="store", type="string", dest="path",
                      default="./{}/".format(now.strftime("%Y-%m-%d-%H-%M-%S")),
                      help="The path to the directory where the generated files will be stored.\n"
                           "The path is also used when resuming to retrieve the previous state.\n"
                           "Default: ./year-month-day-hour-minutes-seconds")
    parser.add_option("-p", "--profile", action="store", type="string", dest="profile",
                      default="./profile.yaml",
                      help="Path to the experiment profile. This yaml file describes which experiments will be done."
                           "Default: ./profile.yaml")
    parser.add_option("-l", "--log", action="store", type="string", dest="log_path",
                      default="./benchmark.log",
                      help="Path to the log file."
                           "Default: ./benchmark.log")

    group = OptionGroup(parser, "Behaviour")
    group.add_option("-c", "--capture", action='store_true', dest="capture", default=False,
                      help="Capture the traffic of the experiment.\n"
                           "Default False.\n"
                           "If no 'Behaviour' flags are used, it is equivalent to '-c -a -g'\n")
    group.add_option("-a", "--analyse", action='store_true', dest="analyse", default=False,
                     help="Analyse the traffic of the experiment in the set path.\n"
                          "Default False.\n"
                          "If no 'Behaviour' flags are used, it is equivalent to '-c -a -g'\n")
    group.add_option("-g", "--graph", action='store_true', dest="graph", default=False,
                     help="Generates the graphs for the expirement in the set path.\n"
                          "Default False.\n"
                          "If no 'Behaviour' flags are used, it is equivalent to '-c -a -g'\n")

    return parser
