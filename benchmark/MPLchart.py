import os
import yaml
import matplotlib.pyplot as plt
import numpy as np

import config as cfg
import log

log_level_stream = log.DEBUG
log_level_file = log.DEBUG

logger = log.get_logger(__name__, stream_level=log_level_stream, file_level=log_level_file)


def __boxplot(values, labels, title, y_units, yscale='linear', ylim_min=None, ylim_max=None):
    plt.boxplot(values, labels=labels)
    plt.title = title
    plt.xticks(rotation=90)
    plt.yscale(yscale)
    plt.ylabel('[{}]'.format(y_units))
    if ylim_max is not None:
        if ylim_min is None:
            ylim_min = 0
        plt.ylim(ylim_min, ylim_max)
    plt.tight_layout()


def boxplot(udp_index, tcp_index, title, y_units, udp_mul=1, tcp_mul=1, path='./', mapping=None):
    logger.debug('')
    logger.debug('boxplot {}'.format(title))
    os.makedirs(path, exist_ok=True)
    labels = dict()
    labels_short = dict()
    values = dict()
    values_by_label = dict()

    for protocol in list(cfg.graph['experiment_chart_data'].keys()):
        labels[protocol] = list()
        labels_short[protocol] = list()
        li = 1
        values[protocol] = list()
        values_by_label[protocol] = dict()

        for exp_name in list(cfg.graph['experiment_chart_data'][protocol].keys()):
            label = '{} {}'.format(protocol, exp_name)
            label_short = '{} {}'.format(protocol, li)
            li += 1
            value = list()
            values_by_label[protocol][label] = list()

            if protocol != 'UDP':
                index = tcp_index
                mul = tcp_mul
            else:
                index = udp_index
                mul = udp_mul

            if index:
                for iteration in list(cfg.graph['experiment_chart_data'][protocol][exp_name].keys()):
                    # logger.debug("{} {} {} {}".format(prot, exp_name, iteration, index))
                    value.append(float(cfg.graph['experiment_chart_data'][protocol][exp_name][iteration][index])*mul)
                labels[protocol].append(label)
                labels_short[protocol].append(label_short)
                values[protocol].append(list(value))
                values_by_label[protocol][label] = list(value)

    if mapping:
        for options in mapping:
            logger.debug("    Generating for {} with {} scale".format(" ".join(options['map']), options['yscale']))
            labels_acc = list()
            labels_short_acc = list()
            values_acc = list()
            values_by_label_acc = dict()
            for protocol in [key for key in list(labels.keys()) if key in options['map']]:
                labels_acc = labels_acc + labels[protocol]
                labels_short_acc = labels_short_acc + labels_short[protocol]
                values_acc = values_acc + values[protocol]
                values_by_label_acc[protocol] = dict()
                for label in labels[protocol]:
                    values_by_label_acc[protocol][label] = values_by_label[protocol][label]
            fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
            if len(values_acc) == 0 and len(labels_acc) == 0:
                logger.warning("  No data for {} with {} scale".format(" ".join(options['map']), options['yscale']))
                plt.close(fig)
                continue
            __boxplot(values_acc, labels_acc, title, y_units, yscale=options['yscale'])
            plt.savefig("{}{} {} {}.png".format(path, title, " ".join(options['map']), options['yscale']))
            plt.close(fig)
            fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
            __boxplot(values_acc, labels_short_acc, title, y_units, yscale=options['yscale'])
            plt.savefig("{}{} {} {} short labels.png".format(path, title, " ".join(options['map']), options['yscale']))
            plt.close(fig)
            with open("{}{} {} {}.yaml".format(path, title, " ".join(options['map']), options['yscale']), 'w') as f:
                f.write(yaml.dump({'values': values_acc, 'values_by_label': values_by_label_acc, 'labels': labels_acc,
                                   'title': title, 'y_units': y_units, 'map': options['map'],
                                   'yscale': options['yscale']}))

    else:
        labels_acc = list()
        values_acc = list()
        labels_short_acc = list()
        values_by_label_acc = dict()
        for protocol in list(labels.keys()):
            labels_acc = labels_acc + labels[protocol]
            values_acc = values_acc + values[protocol]
            labels_short_acc = labels_short_acc + labels_short[protocol]
            values_by_label_acc[protocol] = dict()
            for label in labels[protocol]:
                values_by_label_acc[protocol][label] = values_by_label[protocol][label]
        fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
        __boxplot(values_acc, labels_acc, title, y_units, yscale='linear')
        plt.savefig("{}{} {}.png".format(path, title, " ".join(list(labels.keys()))))
        plt.close(fig)
        fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
        __boxplot(values_acc, labels_short_acc, title, y_units, yscale='linear')
        plt.savefig("{}{} {}.png".format(path, title, " ".join(list(labels.keys()))))
        plt.close(fig)
        with open("{}{} {}.yaml".format(path, title, " ".join(list(labels.keys()))), 'w') as f:
            f.write(yaml.dump({'values': values_acc, 'values_by_label': values_by_label_acc, 'labels': labels_acc,
                               'title': title, 'y_units': y_units, 'map': list(labels.keys()),
                               'yscale': 'linear'}))
