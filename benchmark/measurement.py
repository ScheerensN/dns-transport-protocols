import os
import socket
import binascii
import time
from datetime import datetime
import subprocess

import benchmark
from struct_various import Protocol, FastRetransmitType

### Measurement Class ###


class Measurement:
    """ Class used to store measurements made on different protocol.
    """

    def __init__(self, handshake_start=None, transfer_start=None, close_start=None, transfer_times=None,
                 close_end=None, capture_path=(benchmark.options.path_capture if benchmark.options.path_capture is not None else benchmark.options.path),
                 query_time_min=-1.0, query_time_avg=-1.0, query_time_max=-1.0):
        self.handshake_start = handshake_start
        self.transfer_start = transfer_start
        self.close_start = close_start
        self.close_end = close_end
        self.capture_path = capture_path
        self.transfer_times = dict() if transfer_times is None else transfer_times

    def __str__(self):
        return "Measurement{\n" \
               "handshake_start = " + str(self.handshake_start-self.handshake_start) + ",\n" +\
               "transfer_start = " + str(self.transfer_start-self.handshake_start) + ",\n" +\
               "close_start = " + str(self.close_start-self.handshake_start) + ",\n" +\
               "close_end = " + str(self.close_end-self.handshake_start) + ",\n" +\
               "capture_path = " + str(self.capture_path) + ",\n" +\
               "transfer_times = " + str(self.transfer_times) + ",\n" +\
               "}\n"

    @property
    def connection_start(self):
        return self.handshake_start

    @connection_start.setter
    def connection_start(self, value):
        self.handshake_start = value

    @property
    def connection_end(self):
        return self.close_end

    @connection_end.setter
    def connection_end(self, value):
        self.close_end = value

    @property
    def handshake_end(self):
        return self.transfer_start

    @handshake_end.setter
    def handshake_end(self, value):
        self.transfer_start = value

    @property
    def transfer_end(self):
        return self.close_start

    @transfer_end.setter
    def transfer_end(self, value):
        self.close_start = value

    def measure_to_csv(self):
        csv = ", ".join([str(self.handshake_start), str(self.transfer_start), str(self.close_start), str(self.close_end)])
        csv += "\n"
        for transaction_id, timings in self.transfer_times.items():
            # Format dns_id = [query, answer, sent, received, timeout number, [resent_time_after_timeout,]]
            add_list = [str(transaction_id), str(timings[2]), str(timings[3])]
            for e in timings[5]:
                add_list.append(str(e))
            csv += ", ".join(add_list)
            csv += "\n"
        return csv

    def measure_csv_format(self):
        fmt = ", ".join(
            ["handshake_start", "transfer_start", "close_start", "close_end"])
        fmt += "\n"
        return fmt + "[" + ", ".join(["transaction_id, sent_time, receive_time [, retry_time_i]"]) + "]*\n"

    def save_measure(self, path):
        file = open(path, 'w')
        file.write(self.measure_to_csv())
        file.close()

### PCAP Capture ###

# store the command string runned in start_capture on this host and the distant host and the filepath on the distant host.
#  to be be able to find the pid to kill it in stop_capture and transfer back the distant capture file.
_capture_info = [None, None, None]
override = False


def start_capture_filename(filename,af, protocol, port,
                  capture_path=benchmark.options.path_capture if benchmark.options.path_capture is not None else benchmark.options.path):
    global _capture_info
    if (not benchmark.options.capture_client and not benchmark.options.capture_server) or override:
        # if capture is disabled in config return.
        return
    # filename must have two {} to fill

    print("\nStart Capture")

    secret = str(binascii.hexlify(os.urandom(24))).split("'")[1]

    ip = ""
    proto = ""
    if af == socket.AF_INET:
        ip = r"ip"
    elif af == socket.AF_INET6:
        ip = r"ipv6"
    if protocol is Protocol.UDP:
        proto = "udp port " + str(port)
    elif protocol is Protocol.TCP or protocol is Protocol.TLS:
        proto = "tcp port " + str(port)
    capture_filter = ["-f", ip + r" and " + (r"udp" if protocol is Protocol.UDP else r"tcp") + r" port " + str(port)]

    # Capture on this host
    if benchmark.options.capture_client:
        if capture_path != "":
            fname = capture_path + "/" + filename.format("client", socket.gethostname())
        else:
            fname = filename.format("client", socket.gethostname())
        cmd = ["tshark", "-i", benchmark.options.capture_interface_client, "-w", fname]
        cmd += capture_filter
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True)
        #time.sleep(5)

        # String of the runned command:
        _capture_info[0] = ""
        i = 0
        for s in cmd:
            _capture_info[0] += str(s) + (" " if i < (len(cmd) - 1) else "")
            i += 1
        _capture_info[0] = _capture_info[0].replace("screen", "SCREEN")  # appears in caps in ps
        if benchmark.options.verbose:
            print(_capture_info[0])

    # Capture on distant host
    if benchmark.options.capture_server:
        if capture_path != "":
            _capture_info[2] = "\"" + capture_path + "/" + filename.format("server", benchmark.options.capture_ssh_host_server) + "\""
        else:
            _capture_info[2] = "\"" + filename.format("server", benchmark.options.capture_ssh_host_server) + "\""
        cmd = ["ssh", benchmark.options.capture_ssh_host_server, "screen", "-dmS", secret, "tshark",
               "-i", benchmark.options.capture_interface_server,  "-w", _capture_info[2]]
        cmd += capture_filter
        subprocess.Popen(cmd, stdout=subprocess.PIPE)

        # String of the runned command:
        _capture_info[1] = ""
        i = 0
        for s in cmd:
            _capture_info[1] += str(s) + (" " if i < (len(cmd) - 1) else "")
            i += 1
        _capture_info[1] = _capture_info[1].replace("screen", "SCREEN").\
            replace("ssh " + benchmark.options.capture_ssh_host_server + " ", "")  # screen appears in caps in ps
        if benchmark.options.verbose:
            print(" ".join(cmd).replace("screen", "SCREEN"))

    if benchmark.options.verbose:
        print("Waiting 3 seconds for tshark to start")
    if benchmark.options.capture_client or benchmark.options.capture_server:
        time.sleep(3)
    print()


def start_capture(af, protocol, port, resolver, packet_retention=None, early_retransmit=None, tfo=None,
                  capture_path=(benchmark.options.path_capture if benchmark.options.path_capture is not None else benchmark.options.path)):
    date = '{:%Y-%m-%d-%H%M%S}'.format(datetime.today())
    ip = ""
    if af == socket.AF_INET:
        ip = "ip"
    elif af == socket.AF_INET6:
        ip = "ipv6"

    filename = ""
    if protocol == Protocol.UDP:
        filename += protocol.name.lower() + "-" + ip + "-" + date + "-" + resolver + "-{}-{}" + "." + \
                    benchmark.options.capture_extension
    elif protocol == Protocol.TLS:
        filename += protocol.name.lower() + "-" + ip + "-" + \
                    packet_retention.name.lower() + "-" + FastRetransmitType(early_retransmit).name.lower() + "-" + date + "-" + resolver + "-{}-{}" + "." + \
                    benchmark.options.capture_extension
    else:
        if not tfo:
            filename += protocol.name.lower() + "-" + ip + "-" + \
                        packet_retention.name.lower() + "-" + FastRetransmitType(early_retransmit).name.lower() + "-" + date + "-" + resolver + "-{}-{}" + \
                        "." + benchmark.options.capture_extension
        else:
            filename += protocol.name.lower() + "-" + ip + "-tfo-" + \
                        packet_retention.name.lower() + "-" + FastRetransmitType(early_retransmit).name.lower() + "-" + date + "-" + resolver + "-{}-{}" + \
                        "." + benchmark.options.capture_extension
    start_capture_filename(filename, af, protocol, port, capture_path)


def stop_capture():
    global _capture_info
    if (_capture_info[0] is None and _capture_info[1] is None) or override:
        # if there is nothing to stop return.
        return
    time.sleep(2)
    # stop local capture
    print("\nStop Capture")

    if _capture_info[0] is not None and benchmark.options.capture_client:
        pid = kill_cmd(_capture_info[0])
        if benchmark.options.verbose and pid != "0":
            print("Stopped (" + pid + "): " + _capture_info[0])
        elif benchmark.options.verbose:
            print("Already stopped: " + _capture_info[0])

        _capture_info[0] = None

    # stop distant capture
    if _capture_info[1] is not None and benchmark.options.capture_server:
        pid = kill_cmd(_capture_info[1], ssh="ssh " + benchmark.options.capture_ssh_host_server)
        if benchmark.options.verbose and pid != "0":
            print("Stopped (" + pid + "): " + _capture_info[1])
        elif benchmark.options.verbose:
            print("Already stopped: " + _capture_info[1])
        _capture_info[1] = None

        if _capture_info[2].startswith("\"./"):
            _capture_info[2] = _capture_info[2].replace("./", "")

        if _capture_info[2] is not None:
            scp_file = _capture_info[2].strip("'").strip('"')
            scp_cmd = ["scp", benchmark.options.capture_ssh_host_server + ":"
                       + scp_file, scp_file]
            p = subprocess.Popen(scp_cmd, stdout=subprocess.PIPE, universal_newlines=True)
            if benchmark.options.verbose:
                print(" ".join(scp_cmd))
            p.wait()

        if benchmark.options.capture_remove_file and _capture_info[2] is not None:
            rm_cmd = ["ssh", benchmark.options.capture_ssh_host_server, "rm", _capture_info[2]]
            p = subprocess.Popen(rm_cmd, stdout=subprocess.PIPE, universal_newlines=True)
            if benchmark.options.verbose:
                print(" ".join(rm_cmd))
            p.wait()

        # reset to None whether or not the file is removed from distant host.
        _capture_info[2] = None
        print()


def kill_cmd(cmd, ssh=""):
    cmd = "'" + cmd.replace('"', "").replace("'", "") + "'"
    pid_cmd = ssh + " ps aux | grep -v grep | grep " + cmd + " | awk '{print $2}'"
    p = subprocess.Popen(pid_cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
    pid = str(p.communicate()[0]).strip("\n")

    if not str(pid) == "":
        k_cmd = ssh + " kill " + pid
        p = subprocess.Popen(k_cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
        p.wait()
    else:
        pid = str(0)

    return pid

