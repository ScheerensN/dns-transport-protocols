#!/usr/bin/env python3.7
import optparse
from optparse import OptionParser, OptionGroup
import config_benchmark as conf_b
import plot

_usage = "\n%prog [options] -f input_file"

_description = "This benchmark tests the performance of DNS over different protocols " \
               "with various additional protocol options to mess with"

def _set_options():
    global parser, options, args
    (options, args) = parser.parse_args()

def _set_parser():
    global parser
    parser = option_parsing()

def option_parsing():
    parser = OptionParser(usage=_usage, description=_description)

    parser.add_option("-r", "--resolver-path", action='store', type='string', dest='resolver_path', default='resolver.json',
                      help="A JSON file containing your DNS server configuration.\n"
                           "Format is :\n"
                           "[\n"
                           "  [ \"server name\", [ \"IPv4\", \"IPv6\"], udp_support_bool, udp_port, tcp_support_bool, tcp_port, tls_support_bool, tls_port],\n"
                           "]\n"
                           " resolver.json is an exmaple (pretty print json for lisibility)")

    parser.add_option("-q", "--quiet", action='store_false', dest="verbose", default=True,
                      help="Show less information.")

    parser.add_option("--udp-timeout", action='store', type='int', dest="timeout", default=conf_b.retry_timeout_default,
                      help="The number of time before an UDP query will be considered lost. Default is "
                           + str(conf_b.retry_timeout_default) + " [s].")

    parser.add_option("--udp-try", action='store', type='int', dest="tries", default=conf_b.retry_number_default,
                      help="The number of times an UDP query will be retransmitted before giving up. Default is "
                           + str(conf_b.retry_timeout_default) + " times.")

    #file_parser.add_option("-i", "--iterations", action='store', type='int', dest="iterations", default=1,
    #                  help="The number of times the measures are repeated. Default is 1")

    # Query code performance display
    parser.add_option("--stats", action='store_true', dest="stats", default=False,
                      help=optparse.SUPPRESS_HELP)

    group = OptionGroup(parser, "Queries")
    group.add_option("--dnssec", action='store_true', dest="dnssec", default=False,
                      help="Queries use DNSSEC.")
    group.add_option("--edns", action='store_true', dest="edns", default=False,
                      help="Queries use the EDNS Resource Record. Superseeded by --dnssec.")
    group.add_option("--edns-flags", action='store', type="int", dest="edns_flags", default=0,
                      help="What flags to set in the EDNS RR. Default is 0.")
    group.add_option("--edns-payload", action='store', type="int", dest="edns_payload", default=512,
                      help="The value of the requestor edns payload size. Default is 512.")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Protocol")
    group.add_option("--ip6", action='store_true', dest="ip6", default=False,
                     help="IPv6 is used if the resolver has an IPv6 address")
    group.add_option("--udp", action='store_true', dest="udp", default=False,
                     help="Use UDP to transmit queries.")
    group.add_option("--tcp", action='store_true', dest="tcp", default=False,
                     help="Use TCP to transmit queries.")
    group.add_option("--tls", action='store_true', dest="tls", default=False,
                     help="Use TLS to transmit queries.")
    group.add_option("--tfo", action='store_true', dest="tfo", default=False,
                     help="TCP use TCP Fast Open. Not available for TLS because the library doesn't support it.")
    group.add_option("--packet-retention", action='store', type='int', dest="packet_retention", default=2,
                     help="Packet retention Algorithm. Possible values: 0 (nodelay), 1 (nagle), 2(cork). Default is 2")
    group.add_option("--fast-retransmit-level", action='store', type='int', dest="fast_retransmit", default=3,
                     help="Fast Retransmit level for net.ipv4.tcp_early_retrans.\n"
                          " 0 disables Early Retransmit (ER)\n"
                          " 1 enables ER\n"
                          " 2 enables ER but delays fast recovery and fast retransmit by a fourth of RTT\n"
                          " 3 enables delayed ER and TLP\n"
                          " 4 enables TLP only\n"
                          "Previous level is restored after execution of the benchmark")

    group = OptionGroup(parser, "Input")
    group.add_option("-f", "--file", action='store', type='string', dest='input', default=None,
                     help="The path to an input containing the queries to be made and their dependencies.\n"
                          "Three possible format: \n"
                          "    - Native dependence tree (example in dependencetree folder)\n"
                          "    - Timings file generated by the python PhantomJS scripts (example in timings folder (dns.json file))\n"
                          "    - WProf dependency graph generated by the WProf tool (example in wprof folder)")
    #group.add_option("--wprof-qtype", action='store', type='string', dest='wprof_qtype', default='A',
    #                 help="As WProf dependency graph are not dns dependdency tree, they don't specify the query type (A, AAAA, ...). " +\
    #                      "This option is used to set that value for all queries of the WProf dependency graph. Default is A")
    group.add_option("--substitute-tld", action='store', type='string', dest='substitute_tld', default=None,
                     help="The TLD to substitute to the real one in the inputs. "
                          "Used for testing with a custom dns zone to avoid making one for every TLD. "
                          "Default is no substitution")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Output")
    group.add_option("-o", "--output", action='store', type='string', dest='path', default='.',
                     help="Default output folder. Superseeded by the similar output and capture flags")
    group.add_option("-g", "--output-graph", action='store', type='string', dest='path_graph', default=None,
                     help="Default output folder for graphs. Default is the value of the -o flag")
    group.add_option("-m", "--output-measurement", action='store', type='string', dest='path_measurement', default=None,
                     help="Default output folder for measurement. Default is the value of the -o flag")
    group.add_option("--date-string", action='store', type='string', dest='date', default=None,
                     help="A string representing the date. Makes output filenames previsible.")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Capture")
    group.add_option("-c", "--capture-client", action='store_true', dest="capture_client", default=False,
                      help="Capture the queries dns traffic on the client")
    group.add_option("-s", "--capture-server", action='store_true', dest="capture_server", default=False,
                      help="Capture the queries dns traffic on the server")
    group.add_option("--capture-path", action='store', type='string', dest='path_capture', default=None,
                     help="Default output folder for captured traces. Default is the value of the -o flag")
    group.add_option("--capture-extension", action='store', type='string', dest='capture_extension', default='pcap',
                     help=optparse.SUPPRESS_HELP)
    group.add_option("--capture-interface-client", action='store', type='string', dest='capture_interface_client', default='eth0',
                     help="Interface on which to capture the traffic client side. Default is eth0")
    group.add_option("--capture-interface-server", action='store', type='string', dest='capture_interface_server', default='eth0',
                     help="Interface on which to capture the traffic server side. Default is eth0")
    group.add_option("--capture-server-ssh-hostname", action='store', type='string', dest='capture_ssh_host_server', default='pi4',
                     help="The server host on which to perform the capture.")
    group.add_option("--capture-remove-file", action='store_true', dest="capture_remove_file", default=False,
                      help="Removed the capture file from the server after transfering it back on the client")
    parser.add_option_group(group)

    group = OptionGroup(parser, "Connection Test")
    group.add_option("--connection-test", action='store_true', dest="ct_enable", default=False,
                     help="Whether to test the connection to the resolvers")
    group.add_option("--ct-name", action='store', type='string', dest='ct_name', default='test.',
                     help="the name of the record to retrieve")
    group.add_option("--ct-rdtype", action='store', type='string', dest='ct_rdtype', default='DNSKEY',
                     help="the type of query to send")

    group.add_option("--ct-response", action='store', type='string', dest='ct_rrset',
        default="IN DNSKEY 256 3 7 AwEAAdj/+4M1gIJm511v12E1vQxFARvi eECtAX4wtz6GLcbuhQtpiS8OXVtgq/87 " \
                 "amYehNfK6uYh1udpyRK5/Wwe3ytGBUji rxVxwDNVIkbq4NPlw4Dqd3VfdGOYASIK WlsmrlGq3lZ3sJu9njHQsZ/0CyqViC1a " \
                 "08wgcAp8wQNMmPr9LCaknCGGSbr7OOo/ bVcxdWHCZnKx5Bejgk8uK0TqPSUXy0GI bML+N123ilOrOzfor5EgFuVKZI9h9Yp3 " \
                 "+1S4DlyRBi+hRxzHp/BLbCW1V8ptEga0 cAaz42JSFH36RfegB8w598Wky27o21Nc QyOlkqu4olwv2Ct232sBNvNS9Ak=",
        help="the type of query to send")

    parser.add_option_group(group)

    return parser


def parse_error(parser):
    parser.print_help()
    exit()

# Plot stuff


def plot_all(filename, udp, tcp, tfo, tls, rslvr_name):
    if udp is not None:
        plot_start(rslvr_name)
        plot_scatter_3(udp,None,None,rslvr_name)
        plot_stop(filename, rslvr_name,"udp-scatter")
        #plot_start(rslvr_name)
        #plot_cdf_3(udp,None,None,rslvr_name)
        #plot_stop(filename, rslvr_name,"udp-cdp")

    if tcp is not None:
        plot_start(rslvr_name)
        plot_scatter_3(None,tcp,None,rslvr_name)
        plot_stop(filename, rslvr_name,"tcp-scatter")
        #plot_start(rslvr_name)
        #plot_cdf_3(None,tcp,None,rslvr_name)
        #plot_stop(filename, rslvr_name,"tcp-cdf")

    if tfo is not None:
        plot_start(rslvr_name)
        plot_scatter_4(None, None, tfo, None, rslvr_name)
        plot_stop(filename, rslvr_name, "tfo-scatter")
        #plot_start(rslvr_name)
        #plot_cdf_3(None,tcp,None,rslvr_name)
        #plot_stop(filename, rslvr_name,"tcp-cdf")


    if tls is not None:
        plot_start(rslvr_name)
        plot_scatter_3(None, None, tls, rslvr_name)
        plot_stop(filename, rslvr_name, "tls-scatter")
        #plot_start(rslvr_name)
        #plot_cdf_3(None, None, tls, rslvr_name)
        #plot_stop(filename, rslvr_name, "tls-cdf")

    plot_start(rslvr_name)
    plot_scatter_4(udp, tcp, tfo, tls, rslvr_name)
    plot_stop(filename, rslvr_name, "scatter")

    plot_start(rslvr_name)
    plot_scatter_4(udp, tcp, tfo, None, rslvr_name)
    plot_stop(filename, rslvr_name, "scatter-udp-tcp-tfo")

    #plot_start(rslvr_name)
    #plot_cdf_3(udp, tcp, tls, rslvr_name)
    #plot_stop(filename, rslvr_name, "cdf")


def plot_start(rslvr_name):
    plot.close_figure()
    plot.axes_label("Queries and responses " + rslvr_name.replace("_", " "), "time[s]",
                    "Order of the queries.\n Answers and timeout are shown at the level of their query")


def plot_stop(filename, rslvr, txt):
    plot.legend()
    p = (options.path if options.path_graph is None else options.path_graph)
    plot.save_figure(
        ("" if len(p) == 0 else p + "/") + filename + "-" + rslvr + "-" + txt + ".png")

    # cdf plot log x axis
    plot.xscale('log')
    plot.save_figure(
        ("" if len(p) == 0 else p + "/") + filename + "-" + rslvr + "-" + txt + "-log.png")
    plot.close_figure()


def plot_scatter_3(udp, tcp, tls, rslvr):
    plot.axes_label("Queries and responses " + rslvr.replace("_", " "), "time[s]",
                    "Order of the queries.\n Answers and timeout are shown at the level of their query")
    if udp is not None:
        plot.plot_scatter(udp, 'b', 'c', qs_label="UDP Queries", rr_label="UDP response received", timeout_label="UDP timeouts")
    if tcp is not None:
        plot.plot_scatter(tcp, 'g', 'm', qs_label="TCP Queries", rr_label="TCP response received")
    if tls is not None:
        plot.plot_scatter(tls, 'k', 'y', qs_label="TLS Queries", rr_label="TLS response received")


def plot_scatter_4(udp, tcp, tfo, tls, rslvr):
    plot.axes_label("Queries and responses " + rslvr.replace("_", " "), "time[s]",
                    "Order of the queries.\n Answers and timeout are shown at the level of their query")
    if udp is not None:
        plot.plot_scatter(udp, 'b', 'c', qs_label="UDP Queries", rr_label="UDP response received", timeout_label="UDP timeouts")
    if tcp is not None:
        plot.plot_scatter(tcp, 'g', 'm', qs_label="TCP Queries", rr_label="TCP response received")
    if tfo is not None:
        plot.plot_scatter(tfo, '#06E80A', '#E8221C', qs_label="TFO Queries", rr_label="TFO response received")
    if tls is not None:
        plot.plot_scatter(tls, 'k', 'y', qs_label="TLS Queries", rr_label="TLS response received")


def plot_cdf_3(udp, tcp, tls, rslvr):
    plot.axes_label("Queries and responses " + rslvr.replace("_", " "), "time[s]",
                    "Order of the queries.\n Answers and timeout are shown at the level of their query")
    if udp is not None:
        plot.plot_cdf(udp, 'b', 'c', qs_label="UDP Queries", rr_label="UDP response received")
    if tcp is not None:
        plot.plot_cdf(tcp, 'g', 'm', qs_label="TCP Queries", rr_label="TCP response received")
    if tls is not None:
        plot.plot_cdf(tls, 'k', 'y', qs_label="TLS Queries", rr_label="TLS response received")


# Arguments parsing
# setup default\parsed values for every lib.
parser = None
_set_parser()
options = None
args = None
_set_options()

if __name__ == '__main__':
    import os
    import sys
    import json
    import datetime
    import struct_various
    import improved_resolver
    import connection_test
    from file_parser import json_file
    import measurement

    print(" ".join(sys.argv))

    # Arguments check
    if len(sys.argv) < 3:
        # not enough args
        parse_error(parser)

    elif options.input is None:
        # no input file
        parse_error(parser)

    os.makedirs(options.path, exist_ok=True)
    if options.path_measurement is not None:
        os.makedirs(options.path_measurement, exist_ok=True)
    if options.path_graph is not None:
        os.makedirs(options.path_graph, exist_ok=True)
    if options.path_capture is not None:
        os.makedirs(options.path_capture, exist_ok=True)

    # Init resolver
    print("\nSetting up resolvers:")
    f = open(options.resolver_path, 'r')
    resolvers_list = json.load(f)
    f.close()

    resolvers = []
    for r in resolvers_list:
        name = r[0]
        udp_support = None
        if r[2]:
            udp_support = improved_resolver.ImprovedResolver(name=name)
            udp_support.nameservers = r[1]
            udp_support.port = r[3]
        tcp_support = None
        if r[4]:
            tcp_support = improved_resolver.ImprovedResolver(name=name)
            tcp_support.nameservers = r[1]
            tcp_support.port = r[5]
        tls_support = None
        if r[6]:
            tls_support = improved_resolver.ImprovedResolver(name=name)
            tls_support.nameservers = r[1]
            tls_support.port = r[7]
        resolvers.append(struct_various.Server(name, udp_support, tcp_support, tls_support))
        if options.verbose:
            print(name + ": OK")
    print()

    # Connection Test
    if options.ct_enable:
        print("Connection Test for all protocols:")
        measurement.override = True  # Don't capture connecsourcetion test

        connection_test.test_connection(resolvers, options)
        measurement.override = False
        print()

    # Loading Queries
    queries = json_file.load(options.input)

    # Resolving
    print("Resolving Queries")

    if options.date is None:
        date = '{:%Y-%m-%d-%H%M%S}'.format(datetime.datetime.today())
    else:
        date = options.date

    #measurement.override = True
    data = []
    for rslvr in resolvers:
        name = rslvr.name
        m = [None, None, None, None]
        if options.udp:
            if rslvr.udp is not None:
                r = rslvr.udp
                if options.ip6 and r.nameservers[1] is None:
                    print("Warning: No IPv6 support for resolver " + r.name)
                    continue
                if not options.ip6 and r.nameservers[0] is None:
                    print("Warning: No IPv4 support for resolver " + r.name)
                    continue

                dest = r.nameservers[1 if options.ip6 else 0]
                m[0] = r.query(queries, proto=struct_various.Protocol.UDP, dest=dest)
                data.append((name, "udp", m[0].measure_to_csv()))

        if options.tcp:
            if rslvr.tcp is not None:
                r = rslvr.tcp
                if options.ip6 and r.nameservers[1] is None:
                    print("Warning: No IPv6 support for resolver " + r.name)
                    continue
                if not options.ip6 and r.nameservers[0] is None:
                    print("Warning: No IPv4 support for resolver " + r.name)
                    continue

                dest = r.nameservers[1 if options.ip6 else 0]
                m[1] = r.query(queries, proto=struct_various.Protocol.TCP, dest=dest, tfo=False,
                               packet_retention=struct_various.PacketRetention(options.packet_retention),
                               fast_retransmit=options.fast_retransmit if options.fast_retransmit > -1 else None)
                data.append((name, "tcp", m[1].measure_to_csv()))

        if options.tfo:
            if rslvr.tcp is not None:
                r = rslvr.tcp
                if options.ip6 and r.nameservers[1] is None:
                    print("Warning: No IPv6 support for resolver " + r.name)
                    continue
                if not options.ip6 and r.nameservers[0] is None:
                    print("Warning: No IPv4 support for resolver " + r.name)
                    continue

                dest = r.nameservers[1 if options.ip6 else 0]
                m[2] = r.query(queries, proto=struct_various.Protocol.TCP, dest=dest, tfo=True,
                               packet_retention=struct_various.PacketRetention(options.packet_retention),
                               fast_retransmit=options.fast_retransmit if options.fast_retransmit > -1 else None)
                data.append((name, "tfo", m[2].measure_to_csv()))

        if options.tls:
            if rslvr.tls is not None:
                r = rslvr.tls
                if options.ip6 and r.nameservers[1] is None:
                    print("Warning: No IPv6 support for resolver " + r.name)
                    continue
                if not options.ip6 and r.nameservers[0] is None:
                    print("Warning: No IPv4 support for resolver " + r.name)
                    continue

                dest = r.nameservers[1 if options.ip6 else 0]
                m[3] = r.query(queries, proto=struct_various.Protocol.TLS, dest=dest, tfo=options.tfo,
                               packet_retention=struct_various.PacketRetention(options.packet_retention),
                               fast_retransmit=options.fast_retransmit if options.fast_retransmit > -1 else None)
                data.append((name, "tls", m[3].measure_to_csv()))

        # Write plots
        plot_all(date, m[0], m[1], m[2], m[3], name)

    filename = (options.path if options.path_measurement is None or len(options.path_measurement) == 0 else options.path_measurement) + "/" + date + "-measurement.json"
    f = open(filename, 'w')
    json.dump(data, f, indent=4, sort_keys=True)
    f.close()

    filename = (options.path if options.path_measurement is None or len(options.path_measurement) == 0 else options.path_measurement) + "/" + date + "-options.json"
    f = open(filename, 'w')
    json.dump(options.__dict__, f, indent=4, sort_keys=True)
    f.close()


    """
    name_m = []
    udp_m = []
    tcp_m = []
    tls_m = []
    for rslvr in resolvers:
        name_m.append(None)
        udp_m.append(None)
        tcp_m.append(None)
        tls_m.append(None)
    agg_m = []
    agg_m_p = []
    measurement.override = True
    for i in range(0, options.iterations):
        agg_m.append([list(name_m), list(udp_m), list(tcp_m), list(tls_m)])
        agg_m_p.append([list(name_m), list(udp_m), list(tcp_m), list(tls_m)])
        for j, rslvr in enumerate(resolvers):
            agg_m[i][0][j] = rslvr.name
            agg_m_p[i][0][j] = rslvr.name
            m = [None, None, None]
            if options.udp:
                if rslvr.udp is not None:
                    r = rslvr.udp
                    if options.ip6 and r.nameservers[1] is None:
                        print("Warning: No IPv6 support for resolver " + r.name)
                        continue
                    if not options.ip6 and r.nameservers[0] is None:
                        print("Warning: No IPv4 support for resolver " + r.name)
                        continue

                    dest = r.nameservers[1 if options.ip6 else 0]
                    m[0] = r.query(queries, proto=struct_various.Protocol.UDP, dest=dest)
                    agg_m[i][1][j] = m[0]
                    agg_m_p[i][1][j] = m[0].measure_to_csv()

            if options.tcp:
                if rslvr.tcp is not None:
                    r = rslvr.tcp
                    if options.ip6 and r.nameservers[1] is None:
                        print("Warning: No IPv6 support for resolver " + r.name)
                        continue
                    if not options.ip6 and r.nameservers[0] is None:
                        print("Warning: No IPv4 support for resolver " + r.name)
                        continue

                    dest = r.nameservers[1 if options.ip6 else 0]
                    m[1] = r.query(queries, proto=struct_various.Protocol.TCP, dest=dest, tfo=options.tfo,
                                packet_retention=struct_various.PacketRetention(options.packet_retention),
                                fast_retransmit=options.fast_retransmit if options.fast_retransmit > -1 else None)
                    agg_m[i][2][j] = m[1]
                    agg_m_p[i][2][j] = m[1].measure_to_csv()

            if options.tls:
                if rslvr.tls is not None:
                    r = rslvr.tls
                    if options.ip6 and r.nameservers[1] is None:
                        print("Warning: No IPv6 support for resolver " + r.name)
                        continue
                    if not options.ip6 and r.nameservers[0] is None:
                        print("Warning: No IPv4 support for resolver " + r.name)
                        continue

                    dest = r.nameservers[1 if options.ip6 else 0]
                    m[2] = r.query(queries, proto=struct_various.Protocol.TLS, dest=dest, tfo=options.tfo,
                                packet_retention=struct_various.PacketRetention(options.packet_retention),
                                fast_retransmit=options.fast_retransmit if options.fast_retransmit > -1 else None)
                    agg_m[i][3][j] = m[2]
                    agg_m_p[i][3][j] = m[2].measure_to_csv()

            # Write measurements



            # Write plots

            #Scatter
            plot.close_figure()
            plot.axes_label("Queries and responses " + rslvr.name.replace("_", " "), "time[s]",
                            "Order of the queries.\n Answers and timeout are shown at the level of their query")
            plot.legend()

            if rslvr.udp is not None:
                plot.plot_scatter(m[0], 'b', 'c', 'r', qs_label="UDP Queries", rr_label="UDP response received",
                                  timeout_label="UDP Timeout")
            if rslvr.tcp is not None:
                plot.plot_scatter(m[1], 'g', 'm', 'r', qs_label="TCP Queries", rr_label="TCP response received",
                                  timeout_label="???")
            if rslvr.tls is not None:
                plot.plot_scatter(m[2], 'k', 'y', 'r', qs_label="TLS Queries", rr_label="TLS response received",
                                  timeout_label="???")
            p = (options.path if options.path_graph is None else options.path_graph)
            plot.save_figure(("" if len(p) == 0 else p + "/") + filename.format(rslvr.name + " " + str(i) + "-scatter_plot.png"))
            plot.xscale('log')
            plot.save_figure(("" if len(p) == 0 else p + "/") + filename.format(rslvr.name + " " + str(i) + "-scatter_plot_log.png"))
            plot.close_figure()
            # CDF
            plot.axes_label("Queries and responses " + rslvr.name.replace("_", " "), "time[s]",
                            "Order of the queries.\n Answers and timeout are shown at the level of their query")
            plot.legend()

            if m[0] is not None:
                plot.plot_scatter(m[0], 'b', 'c', 'r', qs_label="UDP Queries", rr_label="UDP response received",
                                  timeout_label="UDP Timeout")
            if m[1] is not None:
                plot.plot_scatter(m[1], 'g', 'm', 'r', qs_label="TCP Queries", rr_label="TCP response received",
                                  timeout_label="???")
            if m[2] is not None:
                plot.plot_scatter(m[2], 'k', 'y', 'r', qs_label="TLS Queries", rr_label="TLS response received",
                                  timeout_label="???")

    p = (options.path if options.path_measurement is None else options.path_measurement)
    f = open(("" if len(p) == 0 else p + "/") + filename.format("", "agg_measure.dat"), 'w')
    json.dump(agg_m_p, f)
    f.close()
    """




