#!/usr/bin/env python3.8
import os
import gc
import dns.message
import binascii
import socket
import yaml

import pyshark
import pprint
import time
import datetime
import math
import matplotlib.pyplot as plt

eth_type_ipv4 = '0x00000800'
eth_type_ipv6 = '0x000086DD'


def initDictionaryField(dictionary, key, value):
    """
    If the key isn't in dictionary, add value with that key
    :param dictionary: a dictionary
    :param key: a possible key for the dictionary
    :param value: a value
    :return: The value is added to dictionary if the key wasn't in the dictionary
    """
    if key not in list(dictionary.keys()):
        dictionary[key] = value


def dnsToString(packet):
    """
    Returns A tuple with the pertinent information about the DNS packet
    :param packet: a DNS packet
    :return: A tuple with the pertinent information about the packet
    """
    return (float(packet.frame_info.time_relative),  # Time from the the first captured frame.
            str(packet.dns.id),  # Transaction id

            packet.dns.qry_name if int(packet.dns.flags_response) == 0 else  # Query or response.
            packet.dns.resp_name if int(packet.dns.count_answers) > 0 else None,  # Handle responses without answers

            packet.dns.qry_type.showname_value.split(" ")[0],  # String representing the record type
            str(packet[1].layer_name)  # ip or ipv6
            )


def dissectDnsNetwork(filtered, data, pcap, dst_port):
    dst_port = [str(p) for p in dst_port]
    i = 1
    initDictionaryField(filtered, 'TCP', [])
    initDictionaryField(filtered, 'TLS', [])
    initDictionaryField(filtered, 'HTTPS', [])

    initDictionaryField(data, 'UDP', {})
    initDictionaryField(data, 'TCP', {})

    start_time = 0
    for packet in pcap:
        if i == 1:
            start_time = packet.sniff_time
        if not i % 3000:
            print("Parsed {} packets".format(i))
            gc.collect()
        i += 1
        if hasattr(packet, 'eth'):
            if packet.eth.type == eth_type_ipv6:
                layer2 = packet[1]  # Handle both IPv4 and IPv6 as layer 2

            if packet.eth.type in [eth_type_ipv4, eth_type_ipv6]:
                capture_time = packet.sniff_time - start_time
                #s = "{} ".format(capture_time)
                #s += "IPv4 " if packet.eth.type == eth_type_ipv4 else "IPv6 "
                if hasattr(packet, 'udp'):
                    c_port = packet.udp.srcport if packet.udp.srcport not in dst_port else packet.udp.dstport
                    c_port = int(c_port)
                    initDictionaryField(data['UDP'], c_port, [])
                    data['UDP'][c_port].append(packet)
                    #s += "UDP ({}, {},{}) ".format(packet.udp.stream, packet.udp.srcport, packet.udp.dstport)

                    #if hasattr(packet, 'dns'):
                    #    s += str_pcap_dns(packet)

                if hasattr(packet, 'tcp'):
                    c_port = packet.tcp.srcport if packet.tcp.srcport not in dst_port else packet.tcp.dstport
                    c_port = int(c_port)
                    if c_port not in filtered['TCP']:
                        filtered['TCP'].append(str(c_port))
                    if c_port not in data['TCP']:
                        data['TCP'][c_port] = []
                    data['TCP'][c_port].append(packet)

                    #s += "TCP "
                    #if int(packet.tcp.flags_syn):
                    #    s += "SYN "
                    #elif int(packet.tcp.flags_fin):
                    #    s += "FIN "
                    #elif int(packet.tcp.flags_reset):
                    #    s += "RST "
                    #s += "ACK " if int(packet.tcp.flags_ack) else ""
                    #s += "({}, {}, {}) ".format(packet.tcp.stream, packet.tcp.srcport, packet.tcp.dstport)

                    if hasattr(packet, 'tls'):
                        if c_port not in filtered['TLS']:
                            filtered['TLS'].append(str(c_port))
                        # DNS over TCP
                        #s += "TLS "

                    #if hasattr(packet, 'dns'):
                        # DNS over TCP
                        #s += str_pcap_dns(packet)

                    if hasattr(packet, 'http2'):
                        if c_port not in filtered['HTTPS']:
                            filtered['HTTPS'].append(c_port)
                        #s += "HTTP2 ()"
                        #l = [layer for layer in packet.layers if layer.layer_name == 'http2']
                        #l = [layer for layer in l if hasattr(layer, 'data_data')]
                        #if len(l) > 0:
                            #data_b = binascii.unhexlify("".join(str(l[0].data_data).split(':')))
                            #s += 'DATA '
                            #ans = dns.message.from_wire(data_b)
                            #temp = socket.htons(ans.flags) >> 15
                            #s += "DNS {} {} {} {}".format('R' if temp else 'Q', ans.id, ans.question, ans.answer)

    filtered['TCP'] += [l for l in filtered['TCP'] if l not in filtered['TLS']]
    filtered['TLS'] += [l for l in filtered['TLS'] if l not in filtered['HTTPS']]


def str_pcap_dns(packet):
    return "DNS {} {} {} {}".format('Query' if int(packet.dns.flags_response) == 0 else 'Response',
                                        packet.dns.id,
                                        packet.dns.qry_type.showname_value.split(" ")[0],
                                        packet.dns.qry_name)


def analysis(pcapPath):
    # Split because pcaps are big
    print("Open and analyse pcap file")
    gc.enable()
    print('UDP')
    pcapFile = pyshark.FileCapture(pcapPath, keep_packets=False, display_filter='udp')
    filtered, packets, pcap, dst_port = {}, {}, pcapFile, [53, 443, 853]
    dissectDnsNetwork(filtered, packets, pcap, dst_port)
    pcapFile.close()
    gc.collect()
    print('TCP')
    pcap = pyshark.FileCapture(pcapPath, keep_packets=False, display_filter='tcp.port == 53')  # DNS over TCP
    dissectDnsNetwork(filtered, packets, pcap, dst_port)
    pcap.close()
    gc.collect()
    print('TLS')
    pcap = pyshark.FileCapture(pcapPath, keep_packets=False, display_filter='tcp.port == 853')  # DNS over TLS
    dissectDnsNetwork(filtered, packets, pcap, dst_port)
    pcap.close()
    gc.collect()
    print('HTTPS')
    pcap = pyshark.FileCapture(pcapPath, keep_packets=False, display_filter='tcp.port == 443')  # DNS over HTTPS
    dissectDnsNetwork(filtered, packets, pcap, dst_port)
    pcap.close()
    gc.collect()


    print("Parsing pcap")
    data_g = {}
    print("Parsing UDP")
    data_g['UDP'] = {}
    for c_port in list(packets['UDP'].keys()):
        flow = packets['UDP'][c_port]
        start_time = flow[0].sniff_time
        for packet in flow:
            capture_time = packet.sniff_time - start_time
            if hasattr(packet, 'dns'):
                name = packet.dns.qry_name
                dns_t = 'Response' if int(packet.dns.flags_response) else 'Query'
                c_port_i = int(c_port)
                initDictionaryField(data_g['UDP'], c_port_i, {})
                initDictionaryField(data_g['UDP'][c_port_i], name, {})
                initDictionaryField(data_g['UDP'][c_port_i][name], dns_t, [])
                data_g['UDP'][c_port_i][name][dns_t].append(capture_time)

    print("Parsing TCP")
    data_g['TCP'] = {}
    initDictionaryField(data_g['TCP'], "TCP", {})
    initDictionaryField(data_g['TCP'], "TLS", {})
    initDictionaryField(data_g['TCP'], "HTTPS", {})
    initDictionaryField(data_g['TCP'], "Unknown", {})
    for c_port in list(packets['TCP'].keys()):
        flow = packets['TCP'][c_port]
        start_time = flow[0].sniff_time
        s_port = int(flow[0]['tcp'].dstport)
        for packet in flow:
            connection_type = "TCP" if int(s_port) == 53 else "TLS" if int(s_port) == 853 else "HTTPS" if int(s_port) == 443 else "Unknown"
            capture_time = packet.sniff_time - start_time
            packet_tcp = None
            packet_tls = None
            packet_http2 = None
            packet_dns = None
            if hasattr(packet, 'tcp'):
                packet_tcp = {'SYN': int(packet.tcp.flags_syn),
                              'FIN': int(packet.tcp.flags_fin),
                              'RST': int(packet.tcp.flags_reset),
                              'ACK': int(packet.tcp.flags_ack)}

                if hasattr(packet, 'tls'):
                    if hasattr(packet.tls, 'handshake'):
                        packet_tls = {'Client Hello': str(packet.tls.handshake) == 'Client Hello',
                                      'Server Hello': str(packet.tls.handshake) == 'Handshake Protocol: Server Hello',
                                      'Handshake Finished': str() == 'Handshake Protocol: Finished'}
                    else:
                        packet_tls = {'Client Hello': False,
                                      'Server Hello': False,
                                      'Handshake Finished': False}

                if hasattr(packet, 'http2'):
                    packet_http2 = True
                    l = [layer for layer in packet.layers if layer.layer_name == 'http2']
                    l = [layer for layer in l if hasattr(layer, 'data_data')]
                    if len(l) > 0:
                        data_b = binascii.unhexlify("".join(str(l[0].data_data).split(':')))
                        ans = dns.message.from_wire(data_b)
                        packet_dns = {'name': ans.question[0].name,
                                      'type': 'Response' if socket.htons(ans.flags) >> 15 else 'Query'}

                elif hasattr(packet, 'dns'):
                    # DNS over TCP
                    packet_dns = {'name': packet.dns.qry_name,
                                  'type': 'Response' if int(packet.dns.flags_response) else 'Query'}
            c_port_i = int(c_port)
            initDictionaryField(data_g['TCP'][connection_type], c_port_i, [])
            data_g['TCP'][connection_type][c_port_i].append({'offset': capture_time, 'tcp': packet_tcp, 'tls': packet_tls,
                                                  'http2': packet_http2, 'dns': packet_dns})
    # data_g:
    #   UDP:
    #       c_port_i:
    #           q_name:
    #               dns_t:
    #                   [
    #                       offset
    #                   ]
    #   TCP:
    #       TCP:
    #           c_port_i:
    #                   [
    #                       offset,
    #                       tcp:
    #                           {
    #                               SYN,
    #                               FIN,
    #                               RST,
    #                               ACK
    #                           }
    #                       tls:
    #                           None
    #                       http2:
    #                           None
    #                       dns:
    #                           {
    #                               name,
    #                               type
    #                           }
    #                   ]
    #       TLS:
    #           c_port_i:
    #                   [
    #                       offset,
    #                       tcp:
    #                           {
    #                               SYN,
    #                               FIN,
    #                               RST,
    #                               ACK
    #                           }
    #                       tls:
    #                           {
    #                               Client Hello
    #                               Server Hello
    #                               Handshake Finished
    #                           }
    #                       http2:
    #                           None
    #                       dns:
    #                           {
    #                               name,
    #                               type
    #                           }
    #                   ]
    #       HTTPS:
    #           c_port_i:
    #                   [
    #                       offset,
    #                       tcp:
    #                           {
    #                               SYN,
    #                               FIN,
    #                               RST,
    #                               ACK
    #                           }
    #                       tls:
    #                           {
    #                               Client Hello
    #                               Server Hello
    #                               Handshake Finished
    #                           }
    #                       http2:
    #                           boolean
    #                       dns:
    #                           {
    #                               name,
    #                               type
    #                           }
    #                   ]
    #       Unknown:
    #           <Should be empty => Unknown dst_port>
    #           c_port_i:
    #                   [
    #                       offset,
    #                       tcp:
    #                           {
    #                               SYN,
    #                               FIN,
    #                               RST,
    #                               ACK
    #                           }
    #                       tls:
    #                           {
    #                               Client Hello
    #                               Server Hello
    #                               Handshake Finished
    #                           }
    #                       http2:
    #                           boolean
    #                       dns:
    #                           {
    #                               name,
    #                               type
    #                           }
    #                   ]
    #
    return data_g


def set_chart_data(analysis_d, graph_d, protocol):
    print(protocol)
    for c_port_i in analysis_d['TCP'][protocol]:
        for flow in analysis_d['TCP'][protocol][c_port_i]:
            try:
                experiment = graph_d['experiment_map']['TCP'][c_port_i]['experiment']
            except KeyError:
                continue
            iteration = graph_d['experiment_map']['TCP'][c_port_i]['iteration']

            offset = flow['offset']
            tcp = flow['tcp']
            dns = flow['dns']
            tcp_t = 'SYN' if tcp['SYN'] else 'FIN' if tcp['FIN'] else 'RST' if tcp['RST'] else 'DATA'
            if tcp_t != 'DATA' and tcp['ACK'] == 1:
                tcp_t += ' ACK'

            if tcp_t != 'DATA':
                initDictionaryField(graph_d['experiment_chart_pcap_data'][protocol], experiment, {})
                initDictionaryField(graph_d['experiment_chart_pcap_data'][protocol][experiment], tcp_t, {})
                if iteration in list(graph_d['experiment_chart_pcap_data'][protocol][experiment][tcp_t].keys()):
                    offset_orig = graph_d['experiment_chart_pcap_data'][protocol][experiment][tcp_t][iteration]
                    graph_d['experiment_chart_pcap_data'][protocol][experiment][tcp_t][iteration] = \
                        min(offset/datetime.timedelta(microseconds=1000), offset_orig)
                else:
                    graph_d['experiment_chart_pcap_data'][protocol][experiment][tcp_t][iteration] = \
                        offset/datetime.timedelta(microseconds=1000)

            if dns is not None:
                initDictionaryField(graph_d['experiment_chart_pcap_data'][protocol], experiment, {})
                initDictionaryField(graph_d['experiment_chart_pcap_data'][protocol][experiment], dns['name'], {})
                initDictionaryField(graph_d['experiment_chart_pcap_data'][protocol][experiment][dns['name']], dns['type'], {})
                if iteration in list(graph_d['experiment_chart_pcap_data'][protocol][experiment][dns['name']].keys()):
                    offset_orig = graph_d['experiment_chart_pcap_data'][protocol][experiment][dns['name']][iteration]
                    graph_d['experiment_chart_pcap_data'][protocol][experiment][dns['name']][dns['type']][iteration] = \
                        min(offset_orig, offset/datetime.timedelta(microseconds=1000))
                else:
                    graph_d['experiment_chart_pcap_data'][protocol][experiment][dns['name']][dns['type']][iteration] = \
                        offset/datetime.timedelta(microseconds=1000)


def __boxplot(values, labels, title, y_units, yscale='linear', xlim=None, ylim_min=None, ylim_max=None):
    #print(len(values) == len(labels))
    plt.boxplot(values, labels=labels, vert=False)
    plt.title = title
    #plt.xticks(rotation=15)
    #plt.yscale(yscale)
    #plt.ylabel('[{}]'.format(y_units))
    plt.xlabel('[{}]'.format(y_units))
    if xlim is not None:
        plt.xlim(0, xlim)
    if ylim_max is not None:
        if ylim_min is None:
            ylim_min = 0
        plt.ylim(ylim_min, ylim_max)
    plt.tight_layout()


def boxplot(chart_data, title, y_units, path='./', mapping=None, xlim=None):
    print('boxplot {}'.format(title))
    os.makedirs(path, exist_ok=True)
    labels = dict()
    values = dict()

    for protocol in list(chart_data.keys()):
        labels[protocol] = dict()
        values[protocol] = dict()
        if protocol == 'UDP' and len(chart_data['UDP']) > 0:
            experiment = protocol
            initDictionaryField(labels[protocol], experiment, [])
            initDictionaryField(values[protocol], experiment, [])
            # graph_d['experiment_chart_pcap_data']['UDP'][q_name][dns_t][iteration]
            for name in list(chart_data[protocol].keys()):
                for dns_t in list(chart_data[protocol][name].keys()):
                    labels[protocol][experiment].append("{} {}".format(name, dns_t))
                    values[protocol][experiment].append([offset for offset in list(chart_data[protocol][name][dns_t].values())])

            fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
            __boxplot(values[protocol][experiment], labels[protocol][experiment], experiment, y_units, yscale='linear', xlim=xlim)
            plt.savefig("{}{} {}.png".format(path, title, " ".join(list(labels.keys()))))
            plt.close(fig)
            with open("{}{} {}.yaml".format(path, title, "{}".format(protocol)), 'w') as f:
                f.write(yaml.dump(
                    {'values': values[protocol][experiment], 'labels': labels[protocol][experiment], 'title': title,
                     'y_units': y_units,
                     'yscale': 'linear'}))
        else:
            for experiment in list(chart_data[protocol].keys()):
                initDictionaryField(labels[protocol], experiment, [])
                initDictionaryField(values[protocol], experiment, [])
                for name in list(chart_data[protocol][experiment].keys()):
                    keys = list(chart_data[protocol][experiment][name].keys())
                    if 'Response' not in keys and 'Query' not in keys:
                        # SYN, FIN or RST Packet
                        labels[protocol][experiment].append(name)
                        values[protocol][experiment].append([offset for offset in list(chart_data[protocol][experiment][name].values())])
                    else:
                        for dns_t in keys:
                            labels[protocol][experiment].append("{} {}".format(name, dns_t))
                            values[protocol][experiment].append([offset for offset in list(chart_data[protocol][experiment][name][dns_t].values())])

                    fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
                    __boxplot(values[protocol][experiment], labels[protocol][experiment], experiment, y_units, yscale='linear', xlim=xlim)
                    plt.savefig("{}{} {}.png".format(path, title, "{} {}".format(protocol, experiment)))
                    plt.close(fig)
                    with open("{}{} {}.yaml".format(path, title, "{} {}".format(protocol, experiment)), 'w') as f:
                        f.write(yaml.dump({'values': values[protocol][experiment], 'labels': labels[protocol][experiment], 'title': title, 'y_units': y_units,
                                           'yscale': 'linear'}))
    print()
    # for protocol in list(chart_data.keys()):
    #     labels[protocol] = list()
    #     values[protocol] = list()
    #
    #     for exp_name in list(chart_data[protocol].keys()):
    #         label = '{} {}'.format(protocol, exp_name)
    #         value = list()
    #
    #         if index:
    #             for iteration in list(chart_data[protocol][exp_name].keys()):
    #                 # logger.debug("{} {} {} {}".format(prot, exp_name, iteration, index))
    #                 value.append(float(chart_data[protocol][exp_name][iteration][index])*mul)
    #             labels[protocol].append(label)
    #             values[protocol].append(value)
    #
    # if mapping:
    #     for options in mapping:
    #         logger.debug("    Generating for {} with {} scale".format(" ".join(options['map']), options['yscale']))
    #         labels_acc = list()
    #         values_acc = list()
    #         for protocol in [key for key in list(labels.keys()) if key in options['map']]:
    #             labels_acc = labels_acc + labels[protocol]
    #             values_acc = values_acc + values[protocol]
    #         fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
    #         if len(values_acc) == 0 and len(labels_acc) == 0:
    #             logger.warning("  No data for {} with {} scale".format(" ".join(options['map']), options['yscale']))
    #             plt.close(fig)
    #             continue
    #         __boxplot(values_acc, labels_acc, title, y_units, yscale=options['yscale'])
    #         plt.savefig("{}{} {} {}.png".format(path, title, " ".join(options['map']), options['yscale']))
    #         plt.close(fig)
    #         with open("{}{} {} {}.yaml".format(path, title, " ".join(options['map']), options['yscale']), 'w') as f:
    #             f.write(yaml.dump({'values': values_acc, 'labels': labels_acc, 'title': title, 'y_units': y_units,
    #                                'map': options['map'], 'yscale': options['yscale']}))
    #
    # else:
    #     labels_acc = list()
    #     values_acc = list()
    #     for protocol in list(labels.keys()):
    #         labels_acc = labels_acc + labels[protocol]
    #         values_acc = values_acc + values[protocol]
    #     fig = plt.figure(figsize=(12.8, 7.2), dpi=100)
    #     __boxplot(values_acc, labels_acc, title, y_units, yscale='linear')
    #     plt.savefig("{}{} {}.png".format(path, title, " ".join(list(labels.keys()))))
    #     plt.close(fig)
    #     with open("{}{} {}.yaml".format(path, title, " ".join(list(labels.keys()))), 'w') as f:
    #         f.write(yaml.dump({'values': values_acc, 'labels': labels_acc, 'title': title, 'y_units': y_units,
    #                            'map': list(labels.keys()), 'yscale': 'linear'}))


def graph(analysis_d, analysis_yaml_path, graphDstPath):
    unknown_l = len(analysis_d['TCP']['Unknown'])
    if unknown_l > 0:
        print("Warning: analysis_d['TCP']['Unknown'] should be empty. len: {}".format(unknown_l))
    # Partially copied from main.graph (needed to know each experiments characteristic):
    print("Reading analysis data")
    try:
        with open(analysis_yaml_path, 'r') as f:
            analysis_yaml = yaml.load(f, Loader=yaml.SafeLoader)
    except FileNotFoundError as e:
        print("analysis.yaml not found at: {}".format('{}'.format(analysis_yaml_path)))
        exit()

    # map src port to experiment
    print("Setting up 'src_port => experiment' mapping")
    keys = list(analysis_yaml['capture_info'].keys())
    if len(keys) != 2:
        print("analysis_yaml['capture_info'] has {} keys instead of two".format(len(keys)))
        exit()
    key = keys[0] if keys[0] != 'pcap' else keys[1]

    graph_d = dict()
    graph_d['experiment_map'] = dict()
    graph_d['experiment_map']['UDP'] = dict()
    graph_d['experiment_map']['TCP'] = dict()

    for prot in list(analysis_yaml['capture_info'][key].keys()):
        if prot == 'UDP':
            print("    Mapping UDP")
            for iteration in list(analysis_yaml['capture_info'][key][prot].keys()):
                src_port = analysis_yaml['capture_info'][key][prot][iteration]['src_port']
                graph_d['experiment_map']['UDP'][src_port] = dict()
                graph_d['experiment_map']['UDP'][src_port]['experiment'] = ""
                graph_d['experiment_map']['UDP'][src_port]['iteration'] = iteration
        elif prot == 'TCP':
            print("    Mapping TCP")
            for retention in list(analysis_yaml['capture_info'][key][prot].keys()):
                for retransmit_type in list(analysis_yaml['capture_info'][key][prot][retention].keys()):
                    for tfo in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type].keys()):
                        for iteration in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type][tfo].keys()):
                            src_port = analysis_yaml['capture_info'][key][prot][retention][retransmit_type][tfo][iteration]['src_port']
                            graph_d['experiment_map']['TCP'][src_port] = dict()
                            graph_d['experiment_map']['TCP'][src_port]['protocol'] = prot
                            graph_d['experiment_map']['TCP'][src_port]['experiment'] = "{} {} {}".format(retention, retransmit_type, tfo)
                            graph_d['experiment_map']['TCP'][src_port]['iteration'] = iteration
        elif prot == 'TLS':
            print("    Mapping TLS")
            for retention in list(analysis_yaml['capture_info'][key][prot].keys()):
                for retransmit_type in list(analysis_yaml['capture_info'][key][prot][retention].keys()):
                    for tfo in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type].keys()):
                        for ssl_version in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type][tfo].keys()):
                            for ssl_resumption in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type][tfo][ssl_version].keys()):
                                for iteration in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type][tfo][ssl_version][ssl_resumption].keys()):
                                    src_port = analysis_yaml['capture_info'][key][prot][retention][retransmit_type][tfo][ssl_version][ssl_resumption][iteration]['src_port']
                                    graph_d['experiment_map']['TCP'][src_port] = dict()
                                    graph_d['experiment_map']['TCP'][src_port]['protocol'] = prot
                                    graph_d['experiment_map']['TCP'][src_port]['experiment'] = "{} {} {} {} {}".format(retention, retransmit_type, tfo, ssl_version, ssl_resumption)
                                    graph_d['experiment_map']['TCP'][src_port]['iteration'] = iteration
        elif prot == 'HTTPS':
            print("    Mapping HTTPS")
            for retention in list(analysis_yaml['capture_info'][key][prot].keys()):
                for retransmit_type in list(analysis_yaml['capture_info'][key][prot][retention].keys()):
                    for iteration in list(analysis_yaml['capture_info'][key][prot][retention][retransmit_type].keys()):
                        src_port = analysis_yaml['capture_info'][key][prot][retention][retransmit_type][iteration]['src_port']
                        graph_d['experiment_map']['TCP'][src_port] = dict()
                        graph_d['experiment_map']['TCP'][src_port]['protocol'] = prot
                        graph_d['experiment_map']['TCP'][src_port]['experiment'] = "{} {}".format(retention, retransmit_type)
                        graph_d['experiment_map']['TCP'][src_port]['iteration'] = iteration
        else:
            print("Unrecognized protocol: '{}'".format(prot))

    # end of copy paste

    # Set up data
    print('Calculating values for graphs')
    graph_d['experiment_chart_pcap_data'] = dict()
    graph_d['experiment_chart_pcap_data']['UDP'] = dict()
    graph_d['experiment_chart_pcap_data']['TCP'] = dict()
    graph_d['experiment_chart_pcap_data']['TLS'] = dict()
    graph_d['experiment_chart_pcap_data']['HTTPS'] = dict()
    print('UDP')
    for c_port_i in analysis_d['UDP']:
        for q_name in analysis_d['UDP'][c_port_i]:
            for dns_t in analysis_d['UDP'][c_port_i][q_name]:
                for offset in analysis_d['UDP'][c_port_i][q_name][dns_t]:
                    if c_port_i in list(graph_d['experiment_map']['UDP'].keys()):
                        initDictionaryField(graph_d['experiment_chart_pcap_data']['UDP'], q_name, {})
                        initDictionaryField(graph_d['experiment_chart_pcap_data']['UDP'][q_name], dns_t, {})

                        iteration = graph_d['experiment_map']['UDP'][c_port_i]['iteration']
                        if iteration in list(graph_d['experiment_chart_pcap_data']['UDP'][q_name][dns_t].keys()):
                            graph_d['experiment_chart_pcap_data']['UDP'][q_name][dns_t][iteration] = \
                                min(graph_d['experiment_chart_pcap_data']['UDP'][q_name][dns_t][iteration]/datetime.timedelta(microseconds=1000),
                                    offset/datetime.timedelta(microseconds=1000))
                        else:
                            graph_d['experiment_chart_pcap_data']['UDP'][q_name][dns_t][iteration] = \
                                offset/datetime.timedelta(microseconds=1000)

    set_chart_data(analysis_d, graph_d, 'TCP')
    set_chart_data(analysis_d, graph_d, 'TLS')
    set_chart_data(analysis_d, graph_d, 'HTTPS')

    os.makedirs(graphDstPath + "10/", exist_ok=True)
    os.makedirs(graphDstPath + "60/", exist_ok=True)
    os.makedirs(graphDstPath + "3500/", exist_ok=True)
    boxplot(graph_d['experiment_chart_pcap_data'], 'timeline', 'ms', graphDstPath + "all/")
    boxplot(graph_d['experiment_chart_pcap_data'], 'timeline', 'ms', graphDstPath + "3500/", xlim=3500)
    boxplot(graph_d['experiment_chart_pcap_data'], 'timeline', 'ms', graphDstPath + "10/", xlim=10)
    boxplot(graph_d['experiment_chart_pcap_data'], 'timeline', 'ms', graphDstPath + "60/", xlim=60)

    return graph_d['experiment_chart_pcap_data']


if __name__ == '__main__':
    stime = time.time()
    import subprocess
    import sys

    if len(sys.argv) == 3:
        pass
        pcapPath = sys.argv[1]
        pcapPathSrc = pcapPath + sys.argv[2]
        pcapPathDst = pcapPath + "decrypted.pcap"
    else:
        pcapPath = "/home/badsanta/cloud/dns tranport protocols/git/benchmark/Measure/test/"
        pcapPathSrc = pcapPath + "B-Muspelheim-XPS13U_client.pcap"
        pcapPathDst = pcapPath + "decrypted.pcap"

    cmd = ["editcap", "--inject-secrets", "tls," + pcapPath + "SSLKEYLOGFILE", pcapPathSrc, pcapPathDst]
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, text=True)
    process.wait()
    outs, errs = process.communicate(timeout=15)
    print(outs)
    print(errs)

    data = analysis(pcapPathDst)

    a = graph(data, pcapPath + "analysis.yaml", pcapPath + "charts/timeline/")

    execution_time = time.time() - stime
    hours, remainder = divmod(execution_time, 60*60)
    minutes, seconds = divmod(remainder, 60)
    seconds = math.floor(seconds)
    print("Finished in {}h{}m{}s\n\n".format(int(hours), int(minutes), int(seconds)))





