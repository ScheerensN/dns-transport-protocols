import sys
import logging
import coloredlogs

DEBUG = logging.DEBUG
INFO = logging.INFO
WARNING = logging.WARNING
ERROR = logging.ERROR
CRITICAL = logging.CRITICAL


def get_log_level_str(var):
    if var == DEBUG:
        return 'DEBUG'
    elif var == INFO:
        return 'INFO'
    elif var == WARNING:
        return 'WARNING'
    elif var == ERROR:
        return 'ERROR'
    elif var == CRITICAL:
        return 'CRITICAL'
    return None

__level_style = {'debug': {'color': 'blue'},
                   'info': {'color': 'black'},
                   'warn': {'color': 'yellow'},
                   'error': {'color': 'red'},
                   'critical': {'color': 'black', 'background': 'red'}
                   }
__field_style = {'asctime': {'color': 'green'},
                 'hostname': {'color': 'magenta'},
                 'levelname': {'color': 'black', 'bold': True},
                 'programname': {'color': 'cyan'},
                 'name': {'color': 'blue'}}


def get_logger(name, stream_level=INFO, stream_format='%(asctime)s - %(name)s:%(lineno)d - %(levelname)s - %(message)s',
               log_path=None, file_level=INFO, file_format='%(asctime)s;%(name)s:%(lineno)d;%(levelname)s;%(message)s'):

    logger = logging.getLogger(name)

    coloredlogs.install(stream_level, logger=logger, milliseconds=False, fmt=stream_format, datefmt="%Y-%m-%d %H:%M:%S",
                        level_styles=__level_style,
                        field_styles=__field_style,
                        stream=sys.stdout, isatty=True)

    if log_path is not None:
        add_file_handler(logger, log_path, level=file_level, file_format=file_format)
    else:
        logger.warning("No file_handler for {}'s logger because log_path is None".format(name))

    return logger


def add_file_handler(logger, log_path, level=INFO, file_format='%(asctime)s;%(name)s:%(lineno)d;%(levelname)s;%(message)s'):
    if log_path:
        file_handler = logging.FileHandler(log_path)
        file_handler.setLevel(level)

        file_formatter = logging.Formatter(file_format)
        file_handler.setFormatter(file_formatter)
        logger.addHandler(file_handler)
        logger.debug("Added filehandler - Path:'{}' - Level:'{}'".format(log_path, get_log_level_str(level)))
    else:
        logger.error("log_path is None")
