import json
import heapq
from collections import namedtuple

# python3.7 was not happy with just dns
import dns.rdataclass
import dns.name
import dns.rdatatype
import dns.resolver
import dns.message

import config as cfg
import log
import wprof

logger = None

MessageNode = namedtuple('MessageNode', 'offset name id qtype next')  # used for debug printing


def start_logger(stream_level=log.INFO, file_level=log.INFO):
    global logger
    logger = log.get_logger(__name__, stream_level=stream_level, log_path=cfg.options.log_path, file_level=file_level)


def query_to_message_node(message_list, query_list):
    # used for debug printing
    for q in query_list:
        offset = q.offset
        name = q.query.question[0].name.to_text()
        id = q.query.id
        qtype = dns.rdatatype.to_text(q.query.question[0].rdtype)
        next_m = []
        query_to_message_node(next_m, q.next)
        message_list.append(MessageNode(offset=offset, name=name, id=id, qtype=qtype, next=next_m))
    # Finished


class QueryNode:

    def __init__(self, offset=0, query=None, next=[]):
        self.offset = offset
        self.query = query
        self.next = next

    # To allow heapq to order it as the query field is unorderable
    def __lt__(self, other):
        if self.offset != other.offset:
            return self.offset < other.offset
        else:
            return self.query.id < other.query.id

    def __gt__(self, other):
        if self.offset != other.offset:
            return self.offset > other.offset
        else:
            return self.query.id > other.query.id

    def __str__(self):
        self.__repr__()

    def __repr__(self):
        return "<QueryNode " + str(self.offset) + " " + str(self.query.id) + " " + str(self.query.question) + \
               " child_query:" + str(self.next) + ">"


def append_querynode(l, query, verbose=True, use_edns=None, want_dnssec=False, ednsflags=None, payload=None,
                     request_payload=None, options=None):
    (offset, name, qtype, next_list) = query
    q = make_query_str(name, qtype, use_edns=use_edns, want_dnssec=want_dnssec, ednsflags=ednsflags, payload=payload,
                       request_payload=request_payload, options=options)
    next_list_query = []
    for qq in next_list:
        append_querynode(next_list_query, qq)

    # ordered by offset to simplify sending
    heapq.heapify(next_list_query)
    l.append(QueryNode(offset=offset, query=q, next=next_list_query))


def make_query_str(qname, rdtype, rdclass=dns.rdataclass.IN, use_edns=None, want_dnssec=False,
                   ednsflags=None, payload=None, request_payload=None, options=None):
    if isinstance(qname, str):
        qname = dns.name.from_text(qname, None)
    if isinstance(rdtype, str):
        rdtype = dns.rdatatype.from_text(rdtype)
    if dns.rdatatype.is_metatype(rdtype):
        raise dns.resolver.NoMetaqueries
    if isinstance(rdclass, str):
        rdclass = dns.rdataclass.from_text(rdclass)
    if dns.rdataclass.is_metaclass(rdclass):
        raise dns.resolver.NoMetaqueries
    return dns.message.make_query(qname, rdtype, rdclass, use_edns, want_dnssec, ednsflags, payload, request_payload, options)


def _load_timings_phantomjs(queries, data, verbose=True, use_edns=None, want_dnssec=False, ednsflags=None, payload=None,
                            request_payload=None, options=None):
    for q in data["query"]:
        #if verbose: print("Adding Query for " + q[3] + " " + q[2])
        request = make_query_str(q[2], q[3], use_edns=use_edns, want_dnssec=want_dnssec,
                                 ednsflags=ednsflags, payload=payload, request_payload=request_payload, options=options)
        queries.append(QueryNode(offset=q[0], query=request, next=[]))
    return queries


def _load_dependency_tree(queries, data, verbose=True, use_edns=None, want_dnssec=False, ednsflags=None,
                          payload=None, request_payload=None, options=None):
    for q in data:
        append_querynode(queries, q, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                         ednsflags=ednsflags, payload=payload, request_payload=request_payload, options=options)
    return queries


def _load_wprof(queries, data, rr_type, verbose=True, use_edns=None, want_dnssec=False, ednsflags=None, payload=None,
                request_payload=None, options=None):
    sparse_tree = wprof.load(data, rr_type)

    for q in sparse_tree:
        append_querynode(queries, q, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                         ednsflags=ednsflags, payload=payload, request_payload=request_payload, options=options)
    return queries


# def replace_tld(domain):
#     if benchmark.options.substitute_tld is not None:
#         d = domain.split(".")
#         if len(d) == 1 and (d == ""):
#             return "."
#         else:
#             d[-2 if d[-1] == "" else -1] = benchmark.options.substitute_tld
#             return ".".join(d)
#     return domain


def parse(file_path, rr_type="A", verbose=True, use_edns=None, want_dnssec=False, ednsflags=None, payload=None,
          request_payload=None, options=None, generate_packets=True):
    """
    Check which type of data we loaded based on the JSON file content
    Support Dependency tree, PhantomJS timings & WProf data

    @param use_edns: The EDNS level to use; the default is None (no EDNS).
    See the description of dns.message.Message. see dns.message.use_edns() for the possible
    values for use_edns and their meanings.
    @type use_edns: int or bool or None
    @param want_dnssec: Should the query indicate that DNSSEC is desired?
    @type want_dnssec: bool
    @param ednsflags: EDNS flag values.
    @type ednsflags: int
    @param payload: The EDNS sender's payload field, which is the maximum
    size of UDP datagram the sender can handle.
    @type payload: int
    @param request_payload: The EDNS payload size to use when sending
    this message.  If not specified, defaults to the value of payload.
    @type request_payload: int or None
    @param options: The EDNS options
    @type options: None or list of dns.edns.Option objects
    @see: RFC 2671
    """

    with open(file_path, 'r') as f:
        content = json.load(f)

    queries = []

    # Switch for the different kind of files
    if isinstance(content, list):
        # If this is a list, a dependency tree is in the file
        logger.info("Importing Dependency Tree from '{}'".format(file_path))
        if not generate_packets:
            return content
        return _load_dependency_tree(queries, content, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                                     ednsflags=ednsflags, payload=payload, request_payload=request_payload,
                                     options=options)

    elif isinstance(content, dict) and "query" in content.keys():
        # If this is a dictionary and it contains the key "query", PhantomJS timings are in the file
        logger.info("Importing PhantomJS timings from '{}'".format(file_path))
        if not generate_packets:
            return content
        return _load_timings_phantomjs(queries, content,  verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                                       ednsflags=ednsflags, payload=payload, request_payload=request_payload,
                                       options=options)

    else:
        # Otherwise the file contains a WProf data set
        logger.info("Importing WProf Dependency Tree from '{}'".format(file_path))
        if not generate_packets:
            return content
        return _load_wprof(queries, content, rr_type, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                           ednsflags=ednsflags, payload=payload, request_payload=request_payload,
                           options=options)


def generate_packets(content, rr_type="A", verbose=True, use_edns=None, want_dnssec=False, ednsflags=None, payload=None,
          request_payload=None, options=None):
    """
    Check which type of data we loaded based on the JSON file content
    Support Dependency tree, PhantomJS timings & WProf data

    @param use_edns: The EDNS level to use; the default is None (no EDNS).
    See the description of dns.message.Message. see dns.message.use_edns() for the possible
    values for use_edns and their meanings.
    @type use_edns: int or bool or None
    @param want_dnssec: Should the query indicate that DNSSEC is desired?
    @type want_dnssec: bool
    @param ednsflags: EDNS flag values.
    @type ednsflags: int
    @param payload: The EDNS sender's payload field, which is the maximum
    size of UDP datagram the sender can handle.
    @type payload: int
    @param request_payload: The EDNS payload size to use when sending
    this message.  If not specified, defaults to the value of payload.
    @type request_payload: int or None
    @param options: The EDNS options
    @type options: None or list of dns.edns.Option objects
    @see: RFC 2671
    """

    queries = []
    # Switch for the different kind of files
    if isinstance(content, list):
        # If this is a list, a dependency tree is in the file
        return _load_dependency_tree(queries, content, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                                     ednsflags=ednsflags, payload=payload, request_payload=request_payload,
                                     options=options)

    elif isinstance(content, dict) and "query" in content.keys():
        return _load_timings_phantomjs(queries, content, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                                       ednsflags=ednsflags, payload=payload, request_payload=request_payload,
                                       options=options)

    else:
        # Otherwise the file contains a WProf data set
        return _load_wprof(queries, content, rr_type, verbose=verbose, use_edns=use_edns, want_dnssec=want_dnssec,
                           ednsflags=ednsflags, payload=payload, request_payload=request_payload,
                           options=options)


if __name__ == '__main__':

    paths = ["../input/timings/amazon.com/dns.json",
            "../input/dependencytree/amazon.com.json",
            "../input/wprof/www.amazon.com_/www.amazon.com_.json"]

    for path in paths:
        print(path)
        message = list()
        query = parse(path)
        query_to_message_node(message, query)
        print(json.dumps(message, sort_keys=True, indent=4))
        print()






