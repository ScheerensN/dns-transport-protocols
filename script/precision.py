#!/usr/bin/env python3.5
import time

iteration = 1000000
i = 0

precision_min = 1000000000
precision_max = 0
precision_sum = 0
while i < iteration:
	a = time.time()
	b = time.time()
	while(b == a):
		b = time.time()
		
	precision_min = precision_min if (b-a) > precision_min else (b-a)
	precision_max = precision_max if precision_max > (b-a) else (b-a)
	precision_sum += b-a
	i+=1
average = precision_sum / iteration

print("Time resolution min/avg/max on " + str(iteration) + " tries: " + str(precision_min) + " " + str(average) + " " + str(precision_max))
