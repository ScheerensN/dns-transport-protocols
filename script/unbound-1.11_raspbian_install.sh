#! /bin/bash

echo ""
echo "##### UNBOUND INSTALL  #####"
echo ""
echo "Installing dependency"
echo ""
# unbound compilation dependency
sudo apt-get update
sudo apt-get build-dep unbound -y
sudo apt-get install autoconf libssl-dev libexpat1-dev libevent-dev binutils-dev bison -y
echo ""
echo "Downloading Unbound 1.11"
echo ""
if [ -f "unbound-1.11.0.tar.gz" ]; then
  echo "skiping download. the file already exists"
else
  wget https://nlnetlabs.nl/downloads/unbound/unbound-1.11.0.tar.gz
fi

mkdir -p unbound-1.11.0

if find "unbound-1.11.0" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
    :
else
    tar -xzf unbound-1.11.0.tar.gz -C .
fi

cd unbound-1.11.0

echo ""
echo "Configure"
echo ""

./configure --with-ssl --with-libevent --disable-flto > ../unbound-1.11.0_configure.log
if [ $? -ne 0 ]; then
   echo "./configure FAILED: aborting. See ../unbound-1.11.0_configure.log for details"
   cd ..
   exit
fi

echo ""
echo "Make"
echo ""

make > ../unbound-1.11.0_make.log
if [ $? -ne 0 ]; then
   echo "make FAILED:. See ../unbound-1.11.0_make.log for details"
   cd ..
   exit
fi

echo ""
echo "Make Install"
echo ""

sudo make install > ../unbound-1.11.0_make_install.log
if [ $? -ne 0 ]; then
   echo "make install FAILED:. See ../unbound-1.11.0_make_install.log for details"
   cd ..
   exit
fi

echo ""
echo "Creating user and groups for Unbound"
echo ""

sudo addgroup unbound
sudo useradd -d /var/unbound -m -g unbound -s /bin/false unbound
sudo unbound-control-setup -d /var/unbound
sudo chown -R unbound:unbound /var/unbound

cd ..

echo ""
echo "Installion Finished"
echo ""