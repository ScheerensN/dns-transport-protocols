#!/usr/bin/env python3
import subprocess
log = True
debug = False
logf = ""
maxDomain = 100000

def logOpen():
    global logf
    if log:
        logf = open("log.txt",'w')

def logWrite(txt):
    global logf
    if log:
        logf.write(txt)

def logClose():
    global logf
    if log:
        logf.close()


if __name__ == '__main__':
    file = open("../phantomJS/top-1m.csv")
    lines_nb = str(len(file.readlines()))
    file.seek(0)
    supported_domains = ""
    iteration = 0
    logOpen()
    fileSupport = open("domain_support_dnnsec.txt", 'w')
    fileNoSupport = open("domain_nosupport_dnnsec.txt", 'w')

    for line in file:
        iteration += 1
        print("Domain " + str(iteration) + " of " + lines_nb, end="\r")
        domain = line.split(",")[1]
        cmd = ["dig", "DNSKEY", domain]

        try:
            code = subprocess.run(cmd, check=True, stdout=subprocess.PIPE)
        except subprocess.CalledProcessError as cpe:
            logWrite(str(iteration) + "Error with "+domain+": return code is " + str(cpe.returnCode) + "\n")
            print("Error with "+domain+": return code is " + str(cpe.returnCode))
            continue
        except KeyboardInterrupt:
            logWrite(str(iteration) + " KeyboardInterrupt at " + domain + "\n")
            print(str(iteration) + " KeyboardInterrupt at " + domain + "\n")
            break;
        except Exception as e:
            logWrite(str(iteration) + "Error with " + domain + ": "+str(e)+ "\n")
            print("Error with " + domain + ": "+str(e))
            continue
        output = str(code.stdout).replace("\\n", "\n").replace("\\t", "\t").split("'")[1]
        dnskey_count = output.count("DNSKEY")

        if debug:
            print(output)
            print(dnskey_count)
        logWrite(str(iteration) +"\n"+ output)

        if dnskey_count > 2:
            supported_domains += domain + "\n"
            fileSupport.write(str(iteration) + " " + domain + "\n")
            print("\n" + domain + " support DNSSEC")
        else:
            fileNoSupport.write(str(iteration) + "\n")
        
        if iteration >= maxDomain:
            break

    file.close()
    logClose()
    fileSupport.close()
    fileNoSupport.close()

    print(supported_domains)

