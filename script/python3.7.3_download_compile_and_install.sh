echo "##### Python 3.7.3 compilation #####"

echo "## Installing Depencies ##"
sudo apt-get install libxml2-dev libxslt-dev python3-tk libbz2-dev libncurses5-dev libgdbm-dev liblzma-dev sqlite3 libsqlite3-dev openssl libssl-dev tcl8.6-dev tk8.6-dev libreadline-dev zlib1g-dev autoconf openssl libffi-dev uuid-dev libgdbm-compat-dev -y

echo "## Downloading Python 3.7.3 ##"
wget https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tar.xz
tar -xvf Python-3.7.3.tar.xz

echo "## configure ##"
cd Python-3.7.3
./configure --prefix=/opt/python3.7.3 --enable-optimizations --with-ensurepip=install
echo "## make ##"
make > make.log

echo "## make altinstall and package creation ##"
# checkinstall to make it into a package
sudo make altinstall

echo "## Setting up symlinks ##"
sudo ln -s /opt/python3.7.3/bin/python3.7 /usr/bin/python3.7
sudo ln -s /opt/python3.7.3/bin/python3.7m /usr/bin/python3.7m
sudo ln -s /opt/python3.7.3/bin/python3.7m-config /usr/bin/python3.7m-config
sudo ln -s /opt/python3.7.3/bin/pip3.7 /usr/bin/pip3.7
sudo ln -s /opt/python3.7.3/bin/pyvenv3.7 /usr/bin/pyvenv3.7

echo "Upgrading pip3.7 and installing packages"
sudo pip3.7 install --upgrade pip
sudo pip3.7 install pyshark numpy matplotlib pyyaml
cd ..
echo "Compilation and installation complete"
