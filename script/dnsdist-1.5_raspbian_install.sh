#! /bin/bash

echo "##### DNSDIST 1.5 INSTALL FOR RASPBIAN BUSTER #####"

echo "Adding repository to apt sources"
echo ""

echo "deb http://repo.powerdns.com/raspbian buster-dnsdist-15 main" | sudo tee "/etc/apt/sources.list.d/pdns.list"
echo "Package: dnsdist*" | sudo tee "/etc/apt/preferences.d/dnsdist"
echo "Pin: origin repo.powerdns.com" | sudo tee -a "/etc/apt/preferences.d/dnsdist"
echo "Pin-Priority: 600" | sudo tee -a "/etc/apt/preferences.d/dnsdist"

curl https://repo.powerdns.com/FD380FBB-pub.asc | sudo apt-key add -

echo ""
echo "Installing"
sudo apt-get update
sudo apt-get install dnsdist -y
echo ""
echo "Installed"
