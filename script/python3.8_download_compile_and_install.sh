PYTHON_VERSION='3.8.0'
PYTHON_VERSION_SHORT='3.8'
PYTHON_VERSION_FULL='3.8.0b3'
CURRENT_USER=`whoami`
CURRENT_USER_PRIMARY_GROUP=`id -gn $CURRENT_USER`
PREFIX="/opt/python${PYTHON_VERSION}"
echo "##### Python ${PYTHON_VERSION} compilation #####"

echo "## Installing Depencies ##"
sudo apt-get install libxml2-dev libxslt-dev python3-tk libbz2-dev libncurses5-dev libgdbm-dev liblzma-dev sqlite3 libsqlite3-dev tcl8.6-dev tk8.6-dev libreadline-dev zlib1g-dev autoconf openssl libffi-dev uuid-dev libssl-dev -y # openssl libssl-dev

echo "## Downloading Python ${PYTHON_VERSION} ##"
wget https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION_FULL}.tar.xz
tar -xvf Python-${PYTHON_VERSION_FULL}.tar.xz

echo "## configure ##"
cd Python-${PYTHON_VERSION_FULL}
./configure --enable-shared --prefix=$PREFIX --enable-optimizations --with-ensurepip=install > ../python${PYTHON_VERSION_SHORT}_config.log 2>&1
cat python${PYTHON_VERSION_SHORT}_config.log | tail -n 10

echo "## make ##"
make > ../python${PYTHON_VERSION_SHORT}_make.log 2>&1
cat python${PYTHON_VERSION_SHORT}_make.log | tail -n 30

echo "## make altinstall ##"
sudo make altinstall > ../python${PYTHON_VERSION_SHORT}_altinstall.log 2>&1
cat python${PYTHON_VERSION_SHORT}_altinstall.log | tail -n 24
sudo chown -R $CURRENT_USER:$CURRENT_USER_PRIMARY_GROUP $PREFIX

echo "## Setting up symlinks ##"
sudo ln -s $PREFIX/bin/python${PYTHON_VERSION_SHORT} /usr/bin/python${PYTHON_VERSION_SHORT}
sudo ln -s $PREFIX/bin/python${PYTHON_VERSION_SHORT}m /usr/bin/python${PYTHON_VERSION_SHORT}m
sudo ln -s $PREFIX/bin/python${PYTHON_VERSION_SHORT}m-config /usr/bin/python${PYTHON_VERSION_SHORT}m-config
sudo ln -s $PREFIX/bin/pip${PYTHON_VERSION_SHORT} /usr/bin/pip${PYTHON_VERSION_SHORT}
sudo ln -s $PREFIX/bin/pyvenv${PYTHON_VERSION_SHORT} /usr/bin/pyvenv${PYTHON_VERSION_SHORT}
sudo ln -s $PREFIX/lib/libpython${PYTHON_VERSION_SHORT}.so.1.0 /usr/local/lib/libpython${PYTHON_VERSION_SHORT}.so.1.0

echo "Upgrading pip${PYTHON_VERSION} and installing packages"
sudo pip${PYTHON_VERSION_SHORT} install --upgrade pip
sudo pip${PYTHON_VERSION_SHORT} install pyshark numpy matplotlib pyyaml
cd ..
echo "Compilation and installation complete"
