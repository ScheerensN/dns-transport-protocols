#!/usr/bin/env python3.5
import sys
import subprocess
from optparse import OptionParser

proxyListen = [("nsd", "tcp", "5056"), ("nsd", "tls", "5856"), ("unbound", "tcp", "5057"), ("unbound", "tls", "5857")]
nsdListenUDP = "5054"
unboundListenUDP = "5055"

def usage(program):
    print("usage: ./" + program + " nsd|unbound [options] control_cmd\n"
          "usage: ./" + program + " proxy start|stop\n"
          "usage: ./" + program + " start|stop|reload|sign|status")
    exit(0)

def run(args, sh=False):
    try:
        return subprocess.run(args, check=True, shell=sh, stdout=subprocess.PIPE)
    except subprocess.CalledProcessError as cpe:
        print(str(cpe))
        print("Error: return code is " + str(cpe.returncode))
        exit()
    except Exception as e:
        print("Error: " + str(e))


def Popen(args, sh=False):
    return subprocess.Popen(args, shell=sh, stdout=subprocess.PIPE)


def printStdOut(arg):
    output = str(arg.stdout).split("b'")[1].split("'")[0].replace("\\n","\n")
    print(output, end="")


def controlNSDInstance(args, config):
    args.pop(0)
    args.insert(0, "sudo")
    args.insert(1, "nsd-control")
    args.insert(2, "-c")
    args.insert(3, config)
    return run(args)


def controlNSD(args):
    status = controlNSDInstance(list(args), "/etc/nsd/nsd_tcp_5054.conf")
    printStdOut(status)
    status = controlNSDInstance(list(args), "/etc/nsd/nsd_tls_5854.conf")
    printStdOut(status)


def controlUnboundInstance(args, config):
    args.pop(0)
    args.insert(0, "sudo")
    args.insert(1, "unbound-control")
    args.insert(2, "-c")
    args.insert(3, config)
    return run(args)


def controlUnbound(args):
    status = controlUnboundInstance(list(args), "/var/unbound/unbound-1.5.10_tcp_5055.conf")
    printStdOut(status)
    status = controlUnboundInstance(list(args), "/var/unbound/unbound-1.5.10_tls_5855.conf")
    printStdOut(status)


def controlProxy(args):
    global port
    if str(args[1]).lower() == "start":
        for port in proxyListen:
            return run(["./script/proxyUp"])

    elif str(args[1]).lower() == "stop":
        for port in proxyListen:
            return run(["./script/proxyDown"])

    elif str(args[1]).lower() == "status":
        return run(["./script/proxyStatus"])

    else:
        usage(program)
        return 0


if __name__ == '__main__':
    program = str(sys.argv[0])
    parser = OptionParser(usage="usage: ./%prog nsd [options] control_cmd\nusage: ./%prog unbound [options] control_cmd",
                          description="Convenient way to call unbound-control and nsd-control with default parameters")
    (options, args) = parser.parse_args()
    if len(args) < 1:
        usage(program)

    run(["sudo", "sysctl", "-w", "net.ipv4.tcp_fastopen=3"])

    if str(args[0]).lower() == "nsd":
        controlNSD(args)
        exit()

    elif str(args[0]).lower() == "unbound":
        controlUnbound(args)
        exit()

    elif str(args[0]).lower() == "proxy":
        status = controlProxy(args)
        if status != 0:
            printStdOut(status)
            exit(status.returncode)
        else:
            exit(status)

    elif str(args[0]).lower() == "start":
        controlNSD(["nsd", "start"])
        controlUnbound(["unbound", "start"])

        status = controlProxy(["proxy", "start"])
        printStdOut(status)

    elif str(args[0]).lower() == "stop":
        controlNSD(["nsd", "stop"])
        controlUnbound(["unbound", "stop"])

        status = controlProxy(["proxy", "stop"])
        printStdOut(status)

    elif str(args[0]).lower() == "reload":
        controlNSD(["nsd", "reload"])
        controlUnbound(["unbound", "reload"])

    elif str(args[0]).lower() == "sign":
        status = run(["./script/dnssecSignZone"])
        printStdOut(status)

    elif str(args[0]).lower() == "status":
        p1 = subprocess.Popen(["sudo", "ps", "aux"], stdout=subprocess.PIPE)
        p2 = subprocess.Popen(["grep", "-e", "unbound", "-e", "nsd", "-e", "tdns"],
                              stdin=p1.stdout, stdout=subprocess.PIPE)
        p3 = subprocess.Popen(["grep", "-v", "-e", "grep", "-e", "SCREEN"],
                              stdin=p2.stdout, stdout=subprocess.PIPE)
        print(str(p3.communicate()[0]).split("b'")[1].split("'")[0].replace("\\n", "\n"))
        print()

        p4 = subprocess.Popen(["sudo", "netstat", "-tulpvn"], stdout=subprocess.PIPE)
        p5 = subprocess.Popen(["grep", "-e", "unbound", "-e", "nsd", "-e", "tdns"],
                              stdin=p4.stdout, stdout=subprocess.PIPE)
        print(str(p5.communicate()[0]).split("b'")[1].split("'")[0].replace("\\n", "\n"))

    else:
        usage(program)
